\input{tex/configuration}

\usepackage{semantic}


\newcommand{\HRule}[1]{\rule{\linewidth}{#1}}

\begin{document}

%\include{tex/titlepage}
%------------------------------ Title ------------------------------
\thispagestyle {empty}
\vspace*{\stretch{1}}
\begin{center}
	\noindent \HRule{1.5mm} \\[5mm]
	\noindent \Large \textbf{Network Code} \\
	\noindent \normalsize \textbf{Case Studies Setup}\\[3mm]
	\noindent \HRule{1.5mm}
	\vspace*{2cm}
	{\small Version: 0.9.0} \\
	\vspace*{\stretch{1}}
\end{center}

%------------------------------ Authors ------------------------------
\newpage
\hspace{2cm}
\vspace{\stretch{1}}
\begin{center}
	Corresponding authors:\\[2mm]
	Gonzalo Carvajal \\
	Department of Electrical and Computer Engineering \\
	University of Waterloo \\
	\url{gcarvaja@uwaterloo.ca} \\[5mm]
	%
	Sebastian Fischmeister \\
	Department of Electrical and Computer Engineering \\
	University of Waterloo \\
	\url{sfischme@uwaterloo.ca} \\[5mm]
	%
	Luis Araneda \\
	Department of Electrical Engineering \\
	Universidad de Concepcion, Chile \\
	\url{luaraneda@udec.cl} \\[5mm]
\end{center}
%
\vspace{\stretch{1}}
Additional collaborators:
\begin{itemize}
	\item Robert Trausmuth (\url{trausmuth@fhwn.ac.at})
	\item Madhukar Anand (\url{anandm@cis.upenn.edu}),
	\item Gregor K\"onig (\url{gregor@gregorkoenig.info}),
	\item Insup Lee (\url{lee@cis.upenn.edu}),
	\item Ben (Ching-Pei) Lin (\url{c7lin@engmail.uwaterloo.ca}),
	\item Oleg Sokolsky (\url{sokolsky@cis.upenn.edu}).
\end{itemize}
%
\vspace{\stretch{1}}
Associated web page(s) are:
\begin{itemize}
	\item \url{https://esg.uwaterloo.ca}
\end{itemize}

\vspace{\stretch{1}}


%------------------------------ TOC ------------------------------
\cleardoublepage
\pdfbookmark{\contentsname}{toc}
\tableofcontents

\cleardoublepage

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Equipment details}

\begin{figure}[h]
	\centering
	\begin{subfigure}[b]{9cm}
		\centering
		\includegraphics[width=\textwidth]{netfpga_box}
		\caption{NetFPGAs box}
		\label{fig:equipment:netfpga_box}
	\end{subfigure}
	~
	\begin{subfigure}[b]{7cm}
		\centering
		\includegraphics[width=\textwidth]{atlys_board}
		\caption{Atlys board}
		\label{fig:equipment:atlys}
	\end{subfigure}
	\caption{Functional diagram of case study 1}
	\label{fig:used_equipment}
\end{figure}

Fig.~\ref{fig:used_equipment} depicts the used equipment to run the experiments.
Fig.~\ref{fig:equipment:netfpga_box} shows one of the two boxes available that contain modified network switches.
Box 1 contains four NetFPGA boards and box 2 contains three.
The boxes run GNU/Linux as their operative system, from which the boards can be controlled and programmed to act as a normal Ethernet switch, a real-time capable switch, or as a traffic injector.
Finally, every NetFPGA board contains four Gigabit Ethernet switches.

Fig.~\ref{fig:equipment:atlys} shows an Atlys FPGA development board from Digilent.
This board is used to implement network nodes with custom NICs.
They have capabilities that let them receive and transmit real-time and best-effort traffic using an Ethernet network.


\cleardoublepage
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Video technical details}

The experiments described in this guide demonstrate a video transmission through an Ethernet network using real-time and best-effort traffic.
Table~\ref{table:reference-stream} summarizes the timing and bandwidth requirements for streaming a video of 640x480 pixels, 8-bit gray-scale encoding, with a refresh rate of 60 frames-per-second.

\begin{table}[h]
	\centering
	\caption{Timing and bandwidth requirements for the reference video stream}
	\label{table:reference-stream}
	\begin{tabular}{@{}lcc@{}}
		~ & Ref. Video &  Sync. \\
		\hline
		Line period [$\mu s$]                         &  33   & 33    \\
		Ethernet frame length (with overhead) [Bytes] &  752  & 72    \\
		Ethernet frames per second                    & 30300 & 30300 \\
		Required bandwidth [Mbit/s]                   &  174  &  16   \\
	\end{tabular}
\end{table}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Case 1}

%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Description}

\begin{figure}[h]
	\centering
	\includegraphics[width=10cm]{setup-experiment-1}
	\caption{Functional diagram of case study 1}
	\label{fig:experiment-1}
\end{figure}

Fig.~\ref{fig:experiment-1} depicts the multi-hop setup interconnecting a set of stations through four cascaded real-time capable switches. Stations tagged as C use COTS NICs, and the rest are real-time capable. In this case, all real-time frames use a $\textrm{TTL} \geq 4$, defining a single domain. Different stations are added to the network in different steps to represent different congestion scenarios. 

%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Testing procedure}

Table~\ref{table:case1_roles} summarizes the used equipment, its role, the Fig.~\ref{fig:experiment-1} corresponding naming, and description.

\begin{table}[tb]
	\centering
	\caption{Used equipment and its role for case study 1}
	\label{table:case1_roles}
	\renewcommand{\arraystretch}{1.2}
	\begin{tabular}{r|c|c|p{7cm}}
		Item & Network role & Fig.~\ref{fig:experiment-1} corr. & Description \\
		\hline
		NetFPGA box 1      & Infrastructure & --                & 4 Real-Time capable switches \\
		NetFPGA box 2 (B0) & Infrastructure & $\mathrm{C^{TX}}$ & 1 Best-Effort traffic injector \\
		Node 1 (N1)        & Master         & $\mathrm{RT_1^S}$ & Captures a video signal from HDMI and sends it as RT and BE traffic \\
		Node 2 (N2)        & Slave          & $\mathrm{RT_2^S}$ & Sends RT traffic to enhance the interference in BE traffic \\
		Node 3 (N3)        & Slave          & $\mathrm{RT_1^D}$ & Receives and displays RT traffic \\
		Node 4 (N4)        & Slave          & $\mathrm{BE_1^D}$ & Receives and displays BE traffic \\
		Node 5 (N5)        & --             & $\mathrm{C^{RX}}$ & COTS station for measurements \\
	\end{tabular}
\end{table}


\begin{figure}[tb]
	\centering
	\begin{subfigure}[b]{12cm}
		\centering
		\includegraphics[width=\textwidth]{experiment-1-connentions-init}
		\caption{Initial state}
		\label{fig:experiment-1-connentions-init}
	\end{subfigure}
	\\
	\begin{subfigure}[b]{12cm}
		\centering
		\includegraphics[width=\textwidth]{experiment-1-connentions-exp}
		\caption{Experiments}
		\label{fig:experiment-1-connentions-steps}
	\end{subfigure}
	\caption{Connections for case study 1}
\end{figure}

\begin{minipage}{\linewidth}
Initially, everything should be connected as illustrated by Fig.~\ref{fig:experiment-1-connentions-init}:
~
\begin{itemize}
	\item The switches from box 1 are interconnected using patch cables represented with color blue.
	\item Nodes N1, N3, N4 and N5 should be connected to the NetFGPA box 1.
	\item Connect only one end of cable B0 to the illustrated port of NetFPGA box 2.
	\item The HMDI input from node N1 should be connected to a video source, like a laptop or video-player.
	\item The HDMI output of nodes N3 and N4, which receive real-time and best-effort video respectively, should be connected to monitors.
	Both should be producing a clear video without artifacts.
\end{itemize}
~
\end{minipage}

To replicate our results follow these steps and connections illustrated in Fig~\ref{fig:experiment-1-connentions-steps}:
\begin{enumerate}
	\item Connect the BE traffic injector (B0) from box 2 to box 1 as indicated in the figure.
	The best-effort video should display a choppy video and the real-time video should stay stable.
	\item Connect the second real-time source video (N2).
	This represents more traffic injected.
	The best-effort video should be cut on the right border and the real-time video should remain stable.
\end{enumerate}

%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Schedules}

\noindent\begin{minipage}{\linewidth}
\noindent\begin{minipage}[b]{0.46\linewidth}\begin{lstlisting} [caption = Schedule of node 1]
L0: sync(master,7);
    future(552,L1);
    halt();
L1: future(872,L2)
    create(1);
    send(0,RT,7);
    halt();
L2: future(2506,L0);
    create(1);
    send(0,BE,7);
    halt();
\end{lstlisting}
\end{minipage}
~
\noindent\begin{minipage}[b]{0.46\linewidth}\begin{lstlisting} [caption = Schedule of node 2]
L0: sync(slave,3960);
    if(StatusTest,SyncTimeout,L0);
    future(2107,L1);
    halt();
L1: create(1);
    send(0,RT,7);
    if(AlwaysTrue,L0);
    nop();
\end{lstlisting}
\end{minipage}
\end{minipage}

\noindent\begin{minipage}{\linewidth}
\noindent\begin{minipage}{0.46\linewidth}\begin{lstlisting} [caption = Schedule of node 3]
L0: sync(slave,3960);
    if(StatusTest,SyncTimeout,L0);
    future(2363,L1);
    halt();
L1: receive(0,0);
    if(AlwaysTrue,L0);
\end{lstlisting}
\end{minipage}
~
\noindent\begin{minipage}{0.46\linewidth}\begin{lstlisting} [caption = Schedule of node 4]
L0: sync(slave,3960);
    if(StatusTest,SyncTimeout,L0);
    future(2363,L1);
    halt();
L1: receive(0,0);
    if(AlwaysTrue,L0);
\end{lstlisting}
\end{minipage}
\end{minipage}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\cleardoublepage
\section{Case 2}

%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Description}

\begin{figure}[h]
	\centering
	\includegraphics[width=17cm]{setup-experiment-2}
	\caption{Functional diagram of case study 2}
	\label{fig:experiment-2}
\end{figure}

Fig.~\ref{fig:experiment-2} illustrates the segmentation and dynamic bandwidth allocation capabilities of the devices.
In this case, $\mathrm{RT_O^S}$ transmits a single real-time reference stream.
Stations $\mathrm{RT_{F1}}$ and $\mathrm{RT_{F2}}$ are coordinated to receive interleaved pieces of the original video, apply a Sobel filter for border detection on their corresponding pieces of video, and then transmit the processed lines.
Station $\mathrm{RT_F^D}$ receives and displays the processed lines using a buffer-less configuration.
Correct assembling of the video using lines from distributed sources puts an additional level of stress to the timing properties compared to the previous setup.
Finally, a simple best-effort stream is introduced, flowing from $\mathrm{BE_1^S}$ to $\mathrm{BE_1^D}$, in the domain subject to the bandwidth limitations imposed by the scheduled frames from the video filters.

The schedules use the TTL to prevent unnecessarily broadcasting scheduled frames outside of their intended scope.
$\mathrm{RT_O^S}$ is the synchronization master for the entire network and generates synchronization frames with TTL=5 to cover all switches.
Frames containing original video lines have a TTL=4, just enough to reach the distributed real-time video filters.
Likewise, since the remote display is located only two switches away, the real-time filters emit frames with TTL=2.
Additionally, the schedules in real-time video filters skip transmission of processed video lines containing only black pixels.
When writing the processed lines in the corresponding real-time buffer for transmission, the filtering task running in the host asserts a flag to indicate whether the line contains only black pixels.
The schedule in the NIC then checks this flag, and use the \textbf{if} instruction to decide whether to transmit the line or leave the slot free until the next line period.
The remote display will automatically show a black line whenever it detects that a scheduled line did not arrive.

%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Testing procedure}

Table~\ref{table:case2_roles} summarizes the used equipment, its role, the Fig.~\ref{fig:experiment-2} corresponding naming, and description.

\begin{table}[tb]
	\centering
	\caption{Used equipment and its role for case study 2}
	\label{table:case2_roles}
	\renewcommand{\arraystretch}{1.2}
	\begin{tabular}{r|c|c|p{7cm}}
		Item & Network role & Fig.~\ref{fig:experiment-2} corr. & Description \\
		\hline
		NetFPGA box 1 & Infrastructure & --                 & 4 Real-Time capable switches \\
		NetFPGA box 2 & Infrastructure & --                 & 3 Real-Time capable switches \\
		Node 1 (N1)   & Master         & $\mathrm{RT_O^S}$  & Captures a video signal from HDMI and sends it as RT traffic. \\
		Node 2 (N2)   & Slave          & $\mathrm{RT_{F1}}$ & Receives RT traffic, applies a sobel filter to data, and sends it as RT traffic. \\
		Node 3 (N3)   & Slave          & $\mathrm{RT_{F2}}$ & Receives RT traffic, applies an invert filter to data, and sends it as RT traffic. \\
		Node 4 (N4)   & Slave          & $\mathrm{RT_F^D}$  & Receives and displays an RT filtered video. \\
		Node 5 (N5)   & Slave          & $\mathrm{BE_1^S}$  & Captures a video signal from HDMI and sends it as BE traffic. \\
		Node 6 (N6)   & Slave          & $\mathrm{BE_1^D}$  & Receives and displays BE traffic from node 5. \\
	\end{tabular}
\end{table}

\begin{figure}[tb]
	\centering
	\begin{subfigure}[b]{12cm}
		\centering
		\includegraphics[width=\textwidth]{experiment-2-connentions-init}
		\caption{Initial state}
		\label{fig:experiment-2-connentions-init}
	\end{subfigure}
	\\
	\begin{subfigure}[b]{12cm}
		\centering
		\includegraphics[width=\textwidth]{experiment-2-connentions-exp}
		\caption{Experiments}
		\label{fig:experiment-2-connentions-steps}
	\end{subfigure}
	\caption{Connections for case study 2}
\end{figure}

\begin{minipage}{\linewidth}
	Initially, everything should be connected as illustrated by Fig.~\ref{fig:experiment-2-connentions-init}:
	~
	\begin{itemize}
		\item The switches from box 1 and 2 are interconnected using patch cables represented with color blue.
		\item The slide switches from the Atlys boards on nodes N2 and N3 should be in deactivated position (bottom).
		\item Node N1 should be connected to the NetFGPA box 1, and nodes N4, N5 and N6 to NetFGPA box 2.
		\item The HMDI input from nodes N1 and N5 should be connected to some video sources, like a laptop or video-player.
		Ideally, the sources should have different characteristics to differentiate them visually.
		\item The HDMI output of nodes N4 and N6, which receive real-time and best-effort video respectively, should be connected to monitors.
		Only the monitor corresponding to node N6 should be producing a clear video without artifacts, and node N4 should be displaying a still image.
	\end{itemize}
	~
\end{minipage}

To replicate our results follow these steps and connections illustrated in Fig~\ref{fig:experiment-2-connentions-steps}:
\begin{enumerate}
	\item Connect N2.
	Two interleaved pieces of filtered real-time video, covering half screen, should appear on node N4.
	On node N6, the video should be distorted by the same amount of pieces, but located at random positions on the screen.
	\item Connect N3.
	The other half of filtered real-time video should appear on node N4, covering the whole screen now.
	On node N6, the video should now be almost totally distorted, with only a minor area displaying fine.
	\item Activate dynamic transmission of filtered video by moving the first (switch[0]) slide switch on nodes N2 and N3.
	Real-time video (node N4) shouldn't change, and best-effort video (node N6) should improve depending on the quantity of black lines present in filtered video.
	\item Make some changes on the video corresponding to HDMI input from node N1, to produce different results when applying the Sobel filter.
	This should change the amount of black lines sent by the filtering nodes, which in turn will improve or deteriorate the best-effort video stream.
\end{enumerate}

%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Schedules}

\noindent\begin{minipage}{\linewidth}
\noindent\begin{minipage}[b]{0.48\linewidth}\begin{lstlisting} [caption = Schedule of node 1]
SYNC_M: future(500,SLOT_0_S);
    sync(master,5);
    halt();
SLOT_0_S: future(1000,SLOT_0_R);
    create(1);
    send(0,RT,4);
    halt();
SLOT_0_R: future(200,SLOT_1_S);
    halt();
SLOT_1_S: future(1100,SLOT_1_R);
    halt();
SLOT_1_R: future(100,SLOT_2_S);
    halt();
SLOT_2_S: future(850,SLOT_2_R);
    count(not);
    halt();
SLOT_2_R: future(100,SLOTS_END);
    halt();
SLOTS_END: future(46,SYNC_M);
    halt();
\end{lstlisting}
\end{minipage}
~
\noindent\begin{minipage}[b]{0.48\linewidth}\begin{lstlisting} [caption = Schedule of node 2]
SYNC_S: sync(slave,15);
 if(StatusTest,SyncTimeout,SYNC_S);
SLOT_SYNC: future(250,SLOT_0_S);
    halt();
SLOT_0_S: future(1000,SLOT_0_R);
    receive(0,0);
    halt();
SLOT_0_R: future(200,SLOT_1_S);
    receive(0,1);
    halt();
SLOT_1_S: future(1100,SLOT_1_R);
    count(not);
    if(TestVar,2,z,SLOT_1_SN);
        create(3);
        send(0,RT,2);
SLOT_1_SN: halt();
SLOT_1_R: future(100,SLOT_2_S);
    halt();
SLOT_2_S: future(1100,SLOT_2_R);
    halt();
SLOT_2_R: future(100,SLOTS_END);
    halt();
SLOTS_END: future(25,SYNC_S);
    halt();
\end{lstlisting}
\end{minipage}
\end{minipage}

\noindent\begin{minipage}{\linewidth}
\noindent\begin{minipage}[b]{0.49\linewidth}\begin{lstlisting} [caption = Schedule of node 3]
SYNC_S: sync(slave,15);
 if(StatusTest,SyncTimeout,SYNC_S);
SLOT_SYNC: future(250,SLOT_0_S);
    halt();
SLOT_0_S: future(1000,SLOT_0_R);
    receive(0,0);
    halt();
SLOT_0_R: future(200,SLOT_1_S);
    receive(0,1);
    halt();
SLOT_1_S: future(1100,SLOT_1_R);
    count(not);
    if(TestVar,2,z,SLOT_1_SN);
        create(3);
        send(0,RT,2);
SLOT_1_SN: halt();
SLOT_1_R: future(100,SLOT_2_S);
    halt();
SLOT_2_S: future(1100,SLOT_2_R);
    halt();
SLOT_2_R: future(100,SLOTS_END);
    halt();
SLOTS_END: future(25,SYNC_S);
    halt();
\end{lstlisting}
\end{minipage}
~
\noindent\begin{minipage}[b]{0.49\linewidth}\begin{lstlisting} [caption = Schedule of node 4]
SYNC_S: sync(slave,15);
 if(StatusTest,SyncTimeout,SYNC_S);
SLOT_SYNC: future(250,SLOT_0_S);
    halt();
SLOT_0_S: future(1000,SLOT_0_R);
    receive(0,0);
    halt();
SLOT_0_R: future(200,SLOT_1_S);
    halt();
SLOT_1_S: future(1100,SLOT_1_R);
    receive(0,0);
    halt();
SLOT_1_R: future(100,SLOT_2_S);
    receive(0,1);
    halt();
SLOT_2_S: future(1100,SLOT_2_R);
    receive(0,0);
    halt();
SLOT_2_R: future(100,SLOTS_END);
    halt();
SLOTS_END: future(25,SYNC_S);
    count(not);
    receive(0,0);
    halt();
\end{lstlisting}
\end{minipage}
\end{minipage}

\noindent\begin{minipage}{\linewidth}
\noindent\begin{minipage}[b]{0.48\linewidth}\begin{lstlisting} [caption = Schedule of node 5]
SYNC_S: sync(slave,15);
 if(StatusTest,SyncTimeout,SYNC_S);
SLOT_SYNC: future(250,SLOT_0_S);
    halt();
SLOT_0_S: future(1000,SLOT_0_R);
    count(not);
    create(1);
    send(0,BE,2);
    halt();
SLOT_0_R: future(200,SLOT_1_S);
    halt();
SLOT_1_S: future(1100,SLOT_1_R);
    halt();
SLOT_1_R: future(100,SLOT_2_S);
    halt();
SLOT_2_S: future(1100,SLOT_2_R);
    halt();
SLOT_2_R: future(100,SLOTS_END);
    halt();
SLOTS_END: future(25,SYNC_S);
    halt();
\end{lstlisting}
\end{minipage}
~
\noindent\begin{minipage}[b]{0.48\linewidth}\begin{lstlisting} [caption = Schedule of node 6]
SYNC_S: sync(slave,15);
 if(StatusTest,SyncTimeout,SYNC_S);
SLOT_SYNC: future(250,SLOT_0_S);
    halt();
SLOT_0_S: future(1000,SLOT_0_R);
    halt();
SLOT_0_R: future(200,SLOT_1_S);
    halt();
SLOT_1_S: future(1100,SLOT_1_R);
    halt();
SLOT_1_R: future(100,SLOT_2_S);
    receive(0,1);
    halt();
SLOT_2_S: future(1100,SLOT_2_R);
    halt();
SLOT_2_R: future(100,SLOTS_END);
    halt();
SLOTS_END: future(25,SYNC_S);
    count(not);
    halt();
\end{lstlisting}
\end{minipage}
\end{minipage}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\section{Something else?}

\end{document}
