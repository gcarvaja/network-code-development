\acro{ABS}{Anti-lock Braking System}
\acro{ASIC}{Application-Specific Integrated Circuit}
\acro{ASIP}{Application Specific Instruction-Set Processor}
\acro{AVB}{Audio/Video Bridging}
\acro{CAN}{Controller Area Network}
\acro{COTS}{Commercial Off-The-Shelf}
\acro{CRC}{Cyclic Redundancy Check}
\acro{CSMA/CD}{Carrier Sense Multiple/Access Collision Detection}
\acro{DSP}{Digital Signal Processor}
\acro{ECU}{Electronic Control Unit}
\acro{EEL}{End-to-End Latency}
\acro{EPA}{Ethernet for Plant Automation}
\acro{EPL}{Ethernet Powerlink}
\acro{ESP}{Electronic Stability Program}
\acro{FCS}{Frame Check Sum}
\acro{FIFO}{First-In First-Out}
\acro{FPGA}{Field-Programmable Gate Array}
\acro{FpS}{Frames-per-Second}
\acro{FTT-E}{Flexible Time-Triggered Ethernet}
\acro{FTT-SE}{Flexible Time-Triggered Switched Ethernet}
\acro{HDL}{Hardware Description Language}
\acro{IEEE}{Institute of Electrical and Electronics Engineers}
\acro{IFG}{Inter-Frame Gap}
\acro{IP}{Intellectual Property}
\acro{IRT}{Isochronous Real-Time}
\acro{IT}{Information Technology}
\acro{LAN}{Local Area Network}
\acro{LIN}{Local Interconnect Network}
\acro{LTL}{Linear Temporal Logic}
\acro{LUT}{Look-Up Table}
\acro{MAC}{Medium Access Controller}
\acro{MIPS}{Million Instructions Per Second}
\acro{MOST}{Media Oriented System Transport}
\acro{NC-ASIP}{Network Code ASIP}
\acro{NCC}{Network Code Core}
\acro{NCP}{Network Code Processor}
\acro{NIC}{Network Interface Controller}
\acro{PCI}{Peripheral Communication Interconnect}
\acro{PHY}{Physical transceiver}
\acro{r-NCP}{reduced-NCP}
\acro{RBS}{Reference Broadcast Synchronization}
\acro{RTE}{Real-Time Ethernet}
\acro{SOF}{Start-Of-Frame}
\acro{TDMA}{Time Division Multiple Access}
\acro{TMR}{Triple Module Redundancy}
\acro{TTE}{Time-Triggered Ethernet}
\acro{TTL}{Time-To-Live}
\acro{TTP}{Time-Triggered Protocol}
\acro{VLAN}{Virtual Local Area Network}
\acro{WCPL}{Worst-Case Propagation Latency}