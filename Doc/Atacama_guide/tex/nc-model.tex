\part {The Atacama Framework}
%\section{The Network Code Framework} \label{chapter:nc-model}

Atacama is a comprehensive framework for hard real-time communication on top of Ethernet infrastructure. Atacama is based on the Network Code communication model, which introduced the concept of state-based scheduling. State-based schedules allow developers to describe multiple operational modes (states) using a single executable abstraction, and encode guarded guarded transitions from one state to another in subsequent time slots based on conditions that change at runtime. This property increases the flexibility and improve the bandwidth utilization in relation to traditional static \ac{TDMA} configurations, while keeping the system analyzable and verifiable. Multiple case studies illustrate the advantages of this approach in domains such as control theory~\cite{Weiss2009}, hybrid systems~\cite{Anand2010}, hierarchical scheduling~\cite{Easwaran2007}, medical devices~\cite{Fischmeister2010a}, and in general bursty demand models~\cite{Phan2008}. %

Formal concepts and definitions of Network Code have been widely described in previous work~\cite{Fischmeister2007, Fischmeister2009a}. This chapter summarizes the main concepts, focusing on particular aspects that are relevant for the design and implementation of the hardware components.

\section{Communication Model and Definitions}\label{sec:NC-model}

Network Code defines the layered model depicted in Figure~\ref{fig:NC-model-overview} for each station requiring real-time communication in a distributed system. The model splits the functionality in two parts: the \emph{computation layer} and the \emph{communication layer}. Tasks running in the computation layer produce and consume application data to generate results for the distributed application. The communication layer provides services that enable the exchange of messages among tasks running in different stations. Tasks operate with two types of messages: \emph{real-time} and \emph{best-effort}.

The exchange of real-time messages containing time-critical data follows repetitive temporal patterns that are known in advance and remain static during the operation of the system. Real-time data stays valid for a defined time period, and it is mapped to predefined and fixed memory locations, or buffers, on each station. In every period, tasks performs read/write operations on specific buffers, replacing old data in case there was a local update for a specific variable, and using updated data from the other stations for the local computations. The data stays in the buffer until being locally or remotely updated, and is not destroyed by reading accesses. 

The communication layer provides coordination and offers a guaranteed service for the exchange of real-time messages across the shared medium. An execution unit runs pre-programmed \ac{TDMA} schedules that define anisochronous communication rounds. The schedules divide time into non-overlapped slots, which are allotted to specific messages according to a given strategy. During its allotted slot, a message receives exclusive access to the communication medium. For transmission, the corresponding schedule will read the application data from a specific buffer, encapsulating it in a frame, and sending it to the physical layer that interfaces the medium. The medium provides an atomic broadcast service, so either all stations receive a message or none of them do. This is a common assumption for embedded systems and an intrinsic property of legacy fieldbuses. Schedules implicitly encode sender and receiver information for each message, and then interested consumers of a message are ready to perform the matching operations to retrieve the required data from incoming frames and store it into a local buffer. The communication layer operates independently from the application layer, and they only interact by exchanging data through data buffers. Tasks in the application layer see the communication layer as a shared memory, and they just need to perform periodical reading and writing to specific memory locations.

%%%%%%%%%%%%%%%%
\begin{figure}[t] %\centering %
 \centering
 \centering
 \includegraphics[scale=0.55]{figures/NC-overview}
  \caption{Layered Model for the Network Code Framework}
  \label{fig:NC-model-overview}
\end{figure}
%%%%%%%%%%%%%%%%

To achieve coordination, all schedules must share a global view of time. Network Code follows a \ac{RBS} scheme~\cite{Elson:RBS}, designating a master station in charge of emitting periodical synchronization frames or beacons. A beacon does not contain an explicit timestamp, and slave stations use the arrival time of these beacons as a point of reference to adjust their time base and coordinate the actions. This approach is a simple, effective, and low-cost alternative to traditional distributed clock mechanisms used in legacy fieldbuses~\cite{Decotignie2005, Elson:RBS}. Even though it is also possible to use more complex distributed clock mechanisms, such as IEEE 1588~\cite{IEEE:1588}, these solutions come at an additional cost in specialized hardware (high-precision reference clocks and timestamping units), communication overhead (additional messages for synchronization frames and handshaking mechanisms), and computation overhead (clock adjustment operations in stations).

In the practical implementation, the designated master broadcast beacons usually to signal the start of a communication round. All other stations are slaves which start executing their local actions for the corresponding round only after receiving and correctly decoding a beacon. The synchronization accuracy then relates to variations in the propagation and processing latency in the slave stations. The schedules must consider a worst-case value for these latencies to guarantee conflict-free communication at run time.

Being a time-triggered approach, the model assumes that actions within each round follow well-defined temporal patterns, and all necessary message exchanges and data structures are known in advance and computed offline. Under these conditions, the user can apply static verification~\cite{Fischmeister2007} and analysis~\cite{Anand2006} to evaluate system correctness and detect potential conflicts before they occur at run time (overlapped slots, sender/receiver pairing, memory mapping, etc.). This is a fundamental property for safety-critical applications requiring evidence-based certification.

Best-effort messages are for non--time-critical data. They operate independently from real-time messages, can follow arbitrary patterns, and use additional upper layers and stacks. Since both messages must share a single \ac{MAC} to interface the medium, stations must include some kind of arbitration for accessing this resource from both real-time and best-effort queues. In the original model, the scheduling unit was in charge of controlling all traffic flows, and the framework provided a specific instruction (\code{mode}) to switch between both queues before transmitting or receiving data. Therefore, designers needed to plan in advance operations over best-effort frames, and merge them with the real-time operations in a single schedule. This approach added a new level of complexity to the design, analysis, and verification of the schedules. In this thesis, we modified the model by including a hardware based classifier/arbiter module for accessing the MAC. For incoming frames, the module automatically separates real-time from best-effort frames and sends them to the corresponding queues. For transmission, the module enforces strict priority access to the output resources for real-time frames. Best-effort frames receive access to the medium whenever there are no real-time messages in the queue, and the arbiter interrupts on-going best-effort transmissions in case the scheduler triggers transmission of a real-time frame. This approach allows us to physically isolate real-time and best-effort queues, making the \code{mode} instruction unnecessary.


\section{Network Code Instruction Set} \label{sec:instruction-set}

The Network Code framework provides a domain-specific assembly-like instruction set to describe dynamic \ac{TDMA} communication schedules. We can classify the instructions according to its functionality in data flow control, timing control, and execution flow control. The following paragraphs describe a simplified instruction set adapted from the original language definition~\cite{Fischmeister2007}. The adapted instruction set omits some of the original instructions, and defines new parameters specifically targeted for operation in switched Ethernet networks. For detailed formalities about the semantics and parameters of the complete language, and the differences with the adapted version for this work, see~\cite{Fischmeister2007, NC-spec}.

\subsection{Data Flow Control}

Transmission of real-time messages is driven by \code{create-send} sequences. The instruction \code{create(varID)} moves application data from the data buffer identified by \code{varID}, to an internal buffer. The instruction \code{send(ch, TTL)} encapsulates the data from the internal buffer into an Ethernet frame and sends it to the \ac{MAC} for transmission through the specified logical channel \code{ch}. Channels have logical identifiers to separate independent data flows at the receivers, and are mapped to the same communication medium. The \code{TTL} parameter specifies the maximum number of switches in the path to the destination. 

For the reception of real-time data, the instruction \code{receive(varID, ch)} moves application from the specified channel in the input real-time queue (there is one input buffer per channel) to the local data buffer specified in \code{varID}.

\subsection{Timing Control} \label{sec:timing-instructions}

The instructions for timing control provide the necessary constructs to achieve synchronization, and define and assign the time slots to coordinate the actions across the distributed stations.

The instruction \code{future(L, dl)} starts a countdown timer with initial value \code{dl}, generating an interruption when it expires. The instruction \code{halt()} stalls the execution of subsequent instructions waiting for an interruption from a previous \code{future} instruction, and then resumes execution of the program at label \code{L} when it occurs. A sequence \code{future-halt} is equivalent to a \code{wait} statement, but having them separate gives the flexibility to the designer to perform additional tasks before the timer expires for efficiency and tighter timing control.

The instruction \code{sync(mode, dl)} instruction implements the \ac{RBS} synchronization mechanism to coordinate the actions. In master mode, it broadcasts a synchronization beacon with a specific format without application data. In slave mode, it stalls the execution of the next instruction until either the arrival of a synchronization frame, or the expiration of a countdown timer with initial value \code{dl}, whatever happens first. Once finishing the execution, the instruction sets a status flag to report whether it ended by the arrival of a beacon or by the expiration of the deadline. The schedule can check this flag to decide the following actions.

\subsection{Conditional Execution} \label{sec:conditional-inst}

The instruction \code{branch(guard, L)} enables the implementation of dynamic schedules that make on-the-fly decisions based on guards to assign the time slots. Guards check for specific conditions based on values of the data buffers, execution history, flags, etc. If the evaluated guard returns \code{TRUE}, the schedule will continue execution at the specified label \code{L}. If the guard returns \code{FALSE}, the schedule will continue with the next instruction. For example, the system can stop transmitting data when a certain variable lies below a predefined threshold. 

The branching capability is a unique property of the Network Code language, which provides more flexibility and better resource utilization than strictly static configurations, while still remaining analyzable and verifiable. 

\section{Examples of Network Code Schedules}

The following examples illustrate the concepts of Network Code schedules and how to encode \ac{TDMA} policies using the provided instruction set.

\subsection{Transmitter-Receiver Pairing}

Figure~\ref{fig:example-tx-rx} shows an example of using the data flow and timing control instructions to enable coordinated message exchange between a transmitter and receiver station. For simplicity, we assume that both stations start execution at the same time and remain synchronized while the programs run. 

Figure~\ref{fig:example-tx-rx-sched} shows the programmed schedules in the transmitter and receiver stations. The schedule at the transmitter station $\textrm{S}_{\textrm{tx}}$ starts by setting a countdown timer with initial value $T$, and then transmitting a message containing application data from the application buffer $A$. After sending the data, the schedule stays in a waiting stage until the expiration of the timer to restart execution at label L0 and repeat the process. At the other end, the receiver must wait for the \ac{WCPL} to account for the necessary time to propagate the message through the network, and then perform the operation to retrieve the data and store it in the local buffer $A$. Similarly to the receiver, the process repeats every $T$ time units.

Figure~\ref{fig:example-tx-rx-diagram} shows a graphical representation on how the instructions are mapped to the division and assignment of time slots, and the data flow in the output port of the transmitter and input port of the receiver. Since the communication is broadcast, all stations will receive a copy of a transmitted messages. The system specification available in advance must contain the list of interest receivers for each message. Only interested receivers must perform the receive instruction to retrieve the data. Since reception does not require putting data into the medium, multiple stations can perform the operation during the same time slot. The slot must be long enough to ensure that all receivers process and retrieve the data. If a station does not perform a receive instruction, the data stays in the input buffer until being replaced by a new message arriving from the same channel.

\begin{figure}
  \centering
  \subfloat[Example schedules]{
	\label{fig:example-tx-rx-sched}
    \begin{minipage}{3.8cm}
  \centering   \footnotesize{S$_{tx}$}   \vspace{-4mm}
	\lstinputlisting[language=ncode,stepnumber=2,frame=lines]{figures/example1-tx.ncode} \end{minipage}
  \hspace{2cm}
	\begin{minipage}{3.8cm}
  \centering   \footnotesize{S$_{rx}$}   \vspace{-4mm}
  \lstinputlisting[language=ncode,stepnumber=2,frame=lines]{figures/example1-rx.ncode}\end{minipage}}
  \\
  \centering
  \subfloat[Execution flow and buffer status on each station]{
	\label{fig:example-tx-rx-diagram}
  \includegraphics[scale=0.85]{figures/example-tx-rx-pair}}
  \caption{Coordinated Communication Using Network Code}
  \label{fig:example-tx-rx}
\end{figure}

\subsection{Time Synchronization}

The key requirement to achieve coordination across the distributed stations is keeping a common time-base. Figure~\ref{fig:example-tx-rx-sync} illustrates the \ac{RBS} process to coordinate the tasks between a transmitter-receiver pair.

In the example, the transmitter also acts as the master station in charge of transmitting a synchronization beacon at the start of the round. The block L0 in the transmitter generates and transmit the beacon, including extra waiting time to account for the propagation of this beacon across the network before executing the block L1 to transmit the message with application data. At the other end, the receiver is a slave waiting for the arrival of the beacon. The time base for all actions in the slaves is relative to the arrival time of the beacon. The \code{branch()} instruction in block L0 in the slave checks whether the previous \code{sync(slave)} instruction ended by the arrival of a beacon or by a timeout. In the former case, it continues with the following instructions to receive the application data. In the case of a Timeout, the schedule will stay in the loop until correctly receiving a beacon within the expected time.

The synchronization accuracy relates to variations in the propagation latency and processing time of the frames on each station. To guarantee conflict-free communication at run time, these parameters must be correctly modeled considering a worst-case scenario and considered in the design of the schedules. Assuming that all stations are potential consumers, the slots must provide enough time to propagate the message between the most distant stations in the network, including any source of jitter. Correct characterization of these parameters is particularly complex in Ethernet networks using multiple switches. As the schedules must always consider the worst-case value, reducing the sources of jitter in the devices is as important as reducing the mean value of the latency.

\begin{figure}
  \centering
  \subfloat[Example schedules]{
    \begin{minipage}{4.2cm}
  \centering   \footnotesize{S$_{tx}$}   \vspace{-4mm}
	\lstinputlisting[language=ncode,stepnumber=2,frame=lines]{figures/example2-sync-tx.ncode} \end{minipage}
  \hspace{2cm}
	\begin{minipage}{5.4cm}
  \centering   \footnotesize{S$_{rx}$}   \vspace{-4mm}
  \lstinputlisting[language=ncode,stepnumber=2,frame=lines]{figures/example2-sync-rx.ncode}\end{minipage}}
  \\
  \centering
  \subfloat[Execution flow on each station]{
  \includegraphics[scale=0.85]{figures/example-tx-rx-pair-sync}}
  \caption{Synchronization Between Distributed Stations using Network Code Schedules}
  \label{fig:example-tx-rx-sync}
\end{figure}


\subsection{Conditional branching}

To illustrate the concepts of the conditional branching capabilities, let us consider an example based on a \ac{TMR} application. In a traditional setup for \ac{TMR}, three different sensors transmit independent samples of the same variable in consecutive messages $A$, $B$, and $C$. A voting controller receives these messages and performs a majority vote to determine the final value. On the one hand, in a traditional static \ac{TDMA} configuration, the sequence of transmitted messages is determined only by
the progression of time, and thus stations will always transmit the three messages, even if $A$ and $B$ are already decisive for the voting. On the other hand, schedules based on Network Code can perform a preliminary voting in the third sensor after receiving the first two samples, and if the voting is already decisive, then the schedule can skip the transmission of the third value, leaving the associated slot available for other purposes such as communication of background traffic. 

Figure~\ref{fig:example-TMR} illustrates the associated schedules that describe the previous behavior, and a scheme of the utilization of the medium during different time slots. For simplicity, we omit the synchronization block by assuming that all stations start execution at the same time. The time slots for each message account for the total time required to transmit and receive messages, and $\epsilon$ represents that the medium is free.
%%%%%%%%%%%%%%%%%%%%%
\begin{figure}
  \centering
  \subfloat[Example schedules]{
    \begin{minipage}{3.6cm}
  \centering   \footnotesize{S$_{tx}$}   \vspace{-4mm}
	\lstinputlisting[language=ncode,stepnumber=2,frame=lines]{figures/TMR-example1.ncode} \vspace{3cm} \end{minipage}
  \hspace{1cm}
	\begin{minipage}{3.6cm}
  \centering   \footnotesize{S$_{rx}$}   \vspace{-4mm}
  \lstinputlisting[language=ncode,stepnumber=2,frame=lines]{figures/TMR-example2.ncode} \vspace{2.3cm} \end{minipage}
  \hspace{1cm}
	\begin{minipage}{3.6cm}
  \centering   \footnotesize{S$_{rx}$}   \vspace{-4mm}
  \lstinputlisting[language=ncode,stepnumber=2,frame=lines]{figures/TMR-example3.ncode}\end{minipage}}
	\\
  \centering
  \subfloat[Assignment of time slots based on conditional execution]{
  \includegraphics[scale=0.9]{figures/example-TMR}}
  \caption{TMR example application using Network Code}
  \label{fig:example-TMR}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%\section{The Network Code Processor}
%
%The Network Code Processor is the unit in charge of executing the pre-designed and verified schedules at run time. Previous experimental work on the implementation of the framework considered a software prototype residing in the network driver of a real-time Linux system using \ac{COTS} Ethernet NICs~\cite{Fischmeister2007}. This approach provided good flexibility, because the software can easily be changed and extended to accommodate new features. However, although the code sits as close to the hardware as possible, the overhead of the operating system (memory allocation, preemption, clock granularity, etc.) generates high jitter, affecting the achieved periodicity and limiting operation at high data rates.
%
%For example, results taken from a software implementation and reported in~\cite{Fischmeister2007, Fischmeister2009a}, showed that the statistical mode for the execution time of the \code{send()} instruction is 372~[ns]. Considering the 99th percentile of the sample set, then the execution time lies between 371 and 733~[ns]. Increasing the percentile on small amounts above 99 percent, will lead to significant increases in the execution time. For instance, the 99.9999th percentile reports an upper bound of 19090~[ns]. Since correct operation requires considering the worst-case scenario to size up the time slots, jitter becomes a critical limitation for the efficient utilization of the bandwidth. Results based on the software implementation of the Network Code Processor lead to similar conclusions to a different study reported in~\cite{grillinger:2006-88} for a different network driver based on time-triggered communication.
%
%To address the limitation of the software implementations, a second version of the processor considered a custom hardware implementation~\cite{Fischmeister2009a}. The first prototype reported significant improvement in terms of predictability and efficiency of the bandwidth utilization with respect to the software version, while theoretically enabling operation at line rate over 100[Mbit/s] links. The previous work emphasized the importance of efficient hardware implementations for increased performance; however, the original study restricted the analysis to the implementation and evaluation of the network interfaces, without considering the additional effects of propagation across switched networks.