%Two ports of the IXIA emmiting BE frames of 1000 bytes at 470 Mbps
%One port emiting RT and BE frames every 50 microseconds with frame_size
close all;
clear all;

frame_size= [64 250 400 550 700 850 1000 1250];
total_bit_rate = [10.123 38.414  60.078 80.787 100.604 120.006 138.06 167.045]/2; %Mbps

minRT = [5.10 5.10 5.10 5.12 5.10 5.10 5.12 5.10 ];
maxRT = [5.18 5.18 5.20 5.20 5.20 5.20 5.18 5.18 ];
avgRT = [5.14 5.14 5.16 5.16 5.14 5.14 5.16 5.16 ];

minBE = [19.40 22.90 26.88 28.44 32.70 ];
maxBE = [34.94 36.52 39.50 41.38 50.60 ];
avgBE = [27.18 29.69 32.71 36.37 40.70 ];

for i=1:length(avgRT)
    RT_L(i) = abs(avgRT(i) - minRT(i));
    RT_U(i) = abs(avgRT(i) - maxRT(i));
end

for i=1:length(avgBE)
    BE_L(i) = abs(avgBE(i) - minBE(i));
    BE_U(i) = abs(avgBE(i) - maxBE(i));
end

f=figure(1);
errorbar(total_bit_rate(1:length(avgBE)), avgBE, BE_L, BE_U, 'Marker', 'o');
hold on
errorbar(total_bit_rate, avgRT, RT_L, RT_U, 'Marker', 's');


%text(total_bit_rate(5), avgBE(5),'\leftarrow start with frame losses',...
%     'HorizontalAlignment','left')

xlabel('Tx rate on reference streams [Mbps]');
ylabel('Latency [\mus]');
legend('BE frames', 'RT frames', 'Location','East');
grid on;
MyFigStyle_IEEE_journal(f, 1, 'robustness.eps', 'East');