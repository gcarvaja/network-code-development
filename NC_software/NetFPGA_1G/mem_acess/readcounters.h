#ifndef __READCOUNTERS_H__
#define __READCOUNTERS_H__
#include "memmap.h"
#include "../common/nf2.h"
#include "../common/nf2util.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <limits.h>
#include <signal.h>
#include <time.h>
#include <sys/time.h>

#define NC0_IFACE "nf2c0"
#define NC1_IFACE "nf2c1"
#define NC2_IFACE "nf2c2"
#define NC3_IFACE "nf2c3"
#define NUM_IFACE 4

#define READ_PERIOD 10000       /* default counter sampling rate (in ns) */

/* Global Variables */
static struct nf2device nf2[4];
static char enabled[4] = {0, 0, 0, 0};
static FILE *fp;
static long sec, usec;
sigset_t mask;
static char readonce = 0;

/* Function Prototypes */
void timerhandler(int);
void starttimer(long sec, long usec);
void processArgs(int argc, char ** argv);
void readCounters(void);
void usage(void);

#endif /* __READCOUNTERS_H__ */
