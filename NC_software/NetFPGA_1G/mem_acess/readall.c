/* ****************************************************************************
 * $Id: regread.c 2267 2007-09-18 00:09:14Z grg $
 *
 * Module: readall.c
 * Project: NetFPGA 2 Register Access
 * Description: Reads some registers
 *
 * Change history:
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <net/if.h>

#include "regdefs.h"
//#include "../../../driver/common/nf2.h"
//#include "../../../driver/common/nf2util.h"

#define PATHLEN		80

#define DEFAULT_IFACE	"nf2c0"

/* Global vars */
static struct nf2device nf2;
static int verbose = 0;
static int force_cnet = 0;
static int alias = 0;
static int limitset = 0;
static int base_offset = 0;
static int read_limit =  512; // limits register reads
/* Function declarations */
void readRegisters (int , char **);
void processArgs (int , char **);
void usage (void);

int main(int argc, char *argv[])
{
	unsigned val;
	nf2.device_name = DEFAULT_IFACE;


	processArgs(argc, argv);

	// Open the interface if possible
	if (check_iface(&nf2))
	{
		exit(1);
	}
	if (openDescriptor(&nf2))
	{
		exit(1);
	}

	// Increment the argument pointer
	argc -= optind;
	argv += optind;

	// Read the registers
	readRegisters(argc, argv);
	closeDescriptor(&nf2);

	return 0;
}

/*
 * Read the register(s)
 */
void readRegisters(int argc, char** argv)
{
	int i,k;
	unsigned addr;
	unsigned value;
	int limit_address = PROG_ROM_SIZE;

	// Verify that we actually have some registers to display
	if (argc == 0) {
		usage();
		exit(1);
	}

	// Process the registers one by one
	for (i = 0; i < argc; i++)
	{
		// Work out if we're dealing with decimal or hexadecimal
		if(!alias)
    if (strncmp(argv[i], "0x", 2) == 0 || strncmp(argv[i], "0X", 2) == 0)
		{
			sscanf(argv[i] + 2, "%x", &addr);
		}
    else
		{
			sscanf(argv[i], "%u", &addr);
		} // Else if the alias flag is set...
    else{
      if(strcmp(argv[i],"CONTROL_REG")==0)
      {
        addr = CONTROL_REG_BASE;
        if(!limitset) 
          read_limit = CONTROL_REG_SIZE;
		      limit_address = CONTROL_REG_SIZE;
      }
      else if(strcmp(argv[i],"DATA_MEMORY")==0)
      {
        addr = DATA_MEMORY_BASE;
        if(!limitset) 
          read_limit = DATA_MEMORY_SIZE;
		      limit_address = DATA_MEMORY_SIZE;
      }
      else if(strcmp(argv[i],"PROG_ROM_0")==0)
        addr = PROG_ROM_0_BASE;
      else if(strcmp(argv[i],"CFG_ROM_0")==0)
        addr = CFG_ROM_0_BASE;
      else if(strcmp(argv[i],"CFG_ROM_QUEUE_0")==0)
        addr = CFG_ROM_QUEUE_0_BASE;
      else if(strcmp(argv[i],"PROG_ROM_1")==0)
        addr = PROG_ROM_1_BASE;
      else if(strcmp(argv[i],"CFG_ROM_1")==0)
        addr = CFG_ROM_1_BASE;
      else if(strcmp(argv[i],"CFG_ROM_QUEUE_1")==0)
        addr = CFG_ROM_QUEUE_1_BASE;
      else if(strcmp(argv[i],"PROG_ROM_2")==0)
        addr = PROG_ROM_2_BASE;
      else if(strcmp(argv[i],"CFG_ROM_2")==0)
        addr = CFG_ROM_2_BASE;
      else if(strcmp(argv[i],"CFG_ROM_QUEUE_2")==0)
        addr = CFG_ROM_QUEUE_2_BASE;
      else if(strcmp(argv[i],"PROG_ROM_3")==0)
        addr = PROG_ROM_3_BASE;
      else if(strcmp(argv[i],"CFG_ROM_3")==0)
        addr = CFG_ROM_3_BASE;
      else if(strcmp(argv[i],"CFG_ROM_QUEUE_3")==0)
        addr = CFG_ROM_QUEUE_3_BASE;
      else {
        printf("Wrong alias, valid options include:\n");
        printf("\t CONTROL_REG\n");
        printf("\t DATA_MEMORY\n");
        printf("\t PROG_ROM_N\n");
        printf("\t CFG_ROM_N\n");
        printf("\t CFG_ROM_QUEUE_N\n");
        return ;
      }
    }
    
    // Perform the actual register read
    for(k=0 + base_offset;k < read_limit + base_offset;k++){
	if(k==limit_address)
	{ 
	  printf("Limit address reached\n\n");
	  break;
	}
	readReg(&nf2, addr+4*k, &value);
	printf("%8x: Reg 0x%08x (%u):   0x%08x (%u)\n",k, addr+4*k, addr+4*k, value, value);
    }
 }
}

/* 
 *  Process the arguments.
 */
void processArgs (int argc, char **argv )
{
	char c;

	/* don't want getopt to moan - I can do that just fine thanks! */
	opterr = 0;
	  
	while ((c = getopt (argc, argv, "i:ahn:o:")) != -1)
	{
		switch (c)
	 	{
	 		case 'i':	/* interface name */
		 		nf2.device_name = optarg;
		 		break;
			case 'a':
				alias = 1;
				break;
			case 'n':
				limitset = 1;
				read_limit = atoi(optarg);
				break;
			case 'o':
				base_offset= atoi(optarg);
				break;
	 		case '?':
		 		if (isprint (optopt))
		         		fprintf (stderr, "Unknown option `-%c'.\n", optopt);
		 		else
		         		fprintf (stderr,
		                  		"Unknown option character `\\x%x'.\n",
		                  		optopt);
      case 'h':
	 		default:
		 		usage();
		 		exit(1);
	 	}
	}
}

/*
 *  Describe usage of this program.
 */
void usage (void)
{
	printf("Usage: ./readall <options> [addr...] \n\n");
	printf("Options  -h : Print this message and exit.\n");
	printf("         -o : Defines an offset in 32 bits words over the base address to start reading (default 0).\n");
	printf("         -n : Inputs the number of register to be read starting from addr + o.\n");
	printf("         -a : Use an alias instead of an address. Valid aliases:\n");
  printf("\t\t CONTROL_REG\n");
  printf("\t\t DATA_MEMORY\n");
  printf("\t\t PROG_ROM_N\n");
  printf("\t\t CFG_ROM_N\n");
  printf("\t\t CFG_ROM_QUEUE_N\n");
	printf("         [addr...] is a list of one or more base addresses to read from\n");
}
