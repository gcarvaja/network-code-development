#include "readcounters.h"

    char const *foo[] = { COL_01_LABEL, COL_02_LABEL, COL_03_LABEL, COL_04_LABEL, COL_05_LABEL, COL_06_LABEL, COL_07_LABEL, COL_08_LABEL, COL_09_LABEL, COL_10_LABEL,                   COL_11_LABEL, COL_12_LABEL, COL_13_LABEL, COL_14_LABEL, COL_15_LABEL, COL_16_LABEL, COL_17_LABEL, COL_18_LABEL, COL_19_LABEL, COL_20_LABEL,
                          COL_21_LABEL, COL_22_LABEL, COL_23_LABEL, COL_24_LABEL, COL_25_LABEL, COL_26_LABEL, COL_27_LABEL};

float counters[27][4];

int main(int argc, char *argv[]) {
    struct sigaction act;
    int i;
    
    /* initialization of parameters */
    sec = 0;
    usec = READ_PERIOD;

    fp = stdout;

    nf2[0].device_name = NC0_IFACE;
    nf2[1].device_name = NC1_IFACE;
    nf2[2].device_name = NC2_IFACE;
    nf2[3].device_name = NC3_IFACE;
    
    /* initantiating timer */
    sigemptyset(&mask);
    sigaddset(&mask,SIGALRM);
    act.sa_handler = timerhandler;
    act.sa_flags = 0;
    sigemptyset(&act.sa_mask);

    sigaction(SIGALRM, &act, NULL);
    
    /* process arguments if there are any */
    processArgs(argc, argv);

    /* open port for all interfaces that were specified in the arguments (or open all if none are specified) */
    #ifndef DEBUG
        for ( i = 0; i < NUM_IFACE; i++ ) {
            if ( enabled[i] && (check_iface(&nf2[i]) || openDescriptor(&nf2[i]))) {
                printf("ERROR: Failed to detect/connect to nf2c%d\n", i);
                exit(1);
            }
        }
    #endif
    
    /* if readonce flag is set, just read the counters once and terminate */
    if (readonce) {
        readCounters();
        return 0;
    }
    
    /* start the timer */
    starttimer(sec,usec);
    
    /* do nothing and let the timer interrupt fire and read the counters until user enters 'q' to stdin */
    do {} while (getchar() != 'q');
    
    /* close connections to all enabled interfaces */
    #ifndef DEBUG
        for ( i = 0; i < NUM_IFACE; i++ ) {
            if(enabled[i]) {
                closeDescriptor(&nf2[i]);
            }
        }
    #endif
    
    /* close file output stream unless the stream is the default stdout */
    if(fp != stdout) {
        fclose(fp);
    }

    return 0;
}

/********************************
 * Process the input arguments
 ********************************/
void processArgs(int argc, char **argv) {
    char c;
    char iflag = 0;
    
    opterr = 0;
      
    while ((c = getopt (argc, argv, "hi:p:f:o")) != -1) {
        switch (c) {
             case 'i':    /* enable interface */
                 iflag = 1;
                 enabled[atoi(optarg)] = 1;
                 break;
            case 'p':   /* sample period */
                usec = atol(optarg);
                if( usec == 0 || usec == LONG_MAX || usec == LONG_MIN ) {
                    printf("WARNING: Could not read the period length, defaulting to %d usec\n", READ_PERIOD);
                    usec = READ_PERIOD;
                } else {
                    printf("Read period: %ld usec\n", usec);
                }
                break;
            case 'f':  /* redirect output */
                fp = NULL;
                fp = fopen(optarg,"w");
                if ( fp == NULL ) {
                    printf("WARNING: Could not enabledopen the file %s for writing, using stdout instead\n",optarg);
                    fp = stdout;
                } else {
                    printf("Output file: %s\n",optarg);
                }
                break;
            case 'o': /* read counters only once */
                readonce = 1;
                break;
             case '?':
                 if (isprint (optopt))
                         fprintf (stderr, "Unknown option `-%c'.\n", optopt);
                 else
                         fprintf (stderr,
                                  "Unknown option character `\\x%x'.\n",
                                  optopt);
            case 'h':
             default:
                 usage();
                 exit(1);
         }
    }
    if ( !iflag ) {
        enabled[0] = 1; enabled[1] = 1; enabled[2] = 1; enabled[3] = 1;
    }
}

/********************************
 * Timer interrupt handler
 ********************************/
void timerhandler(int signo) {
    readCounters();
}

/********************************
 * Start the timer used to invoke
 * calls to read the counters
 ********************************/
void starttimer(long sec, long usec){
    struct itimerval tv;
    tv.it_value.tv_sec = sec;
    tv.it_value.tv_usec = usec;
    tv.it_interval.tv_sec = sec;
    tv.it_interval.tv_usec = usec;
    if(setitimer(ITIMER_REAL, &tv, NULL) < 0) {
        printf("ERROR: failed to set timer\n");
    }
}

/********************************
 * Invoked to read the counters
 ********************************/
void readCounters(void) {
    struct timespec currenttime;
    int i, baseaddr, offsetaddr; 
    unsigned value = 0;
        
    //clock_gettime(CLOCK_REALTIME, &currenttime);
        i=0;
        for( offsetaddr = COUNTERS_BASE_BEGIN; offsetaddr <= COUNTERS_BASE_END; offsetaddr += COUNTER_SIZE) {
        fprintf(fp, "%s,", foo[i]);
        
        if( enabled[0] ) {
            readReg( &nf2[0], COUNTERS_0_BASE + offsetaddr, &value );
            counters[i][0] = value;
            fprintf(fp,"%u,",value);
        }
        
        if( enabled[1] ) {
            readReg( &nf2[1], COUNTERS_1_BASE + offsetaddr, &value );
            counters[i][1] = value;
            fprintf(fp,"%u,",value);
        }
        
        if( enabled[2] ) {
            readReg( &nf2[2], COUNTERS_2_BASE + offsetaddr, &value );
            counters[i][2] = value;
            fprintf(fp,"%u,",value);
        }
        
        if( enabled[3] ) {
            readReg( &nf2[3], COUNTERS_3_BASE + offsetaddr, &value );
            counters[i][3] = value;
            fprintf(fp,"%u,",value);
        }
            fprintf(fp,"\n");
            i++;    
    }

    for(i=0; i<4; i++) {
        if( enabled[i] ) {
            printf("Tx throughput %i = %f [Mbps]\n", i, counters[14][i]*8/(counters[15][i]*8e-3));
            printf("Rx throughput %i = %f [Mbps]\n", i, counters[16][i]*8/(counters[17][i]*8e-3));        
        }
    }
}

/********************************
 * Prints the usage statement
 ********************************/
void usage(void) {
    printf("Usage:   ./readcounters <options> \n");
    printf("           to exit the readcounters program, type 'q'\n");
    printf("Options  -h               : Prints this message and exit.\n");
    printf("         -i <iface>       : Include interface <iface>, where <iface> = 0,1,2 or 3. If not specified, all interfaces logged\n");
    printf("         -p <period>      : Set the period (usec) of reading the counters. By default, the period is (approx.) %d usec\n", READ_PERIOD);
    printf("         -f <output file> : Redirect output of application to file\n");
    printf("         -o               : Read current counter values only. The application terminates once all of the counters are read\n");
}
