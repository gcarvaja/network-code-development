# NetFPGA-1G software #

This folder contains the driver and utilities to program and debug the NetFPGA-1G under GNU/Linux.
The GNU/Linux driver is included for the NetFPGA-1G board, forked from the official repository (located at https://github.com/NetFPGA/netfpga ) with some patches added to support newer Kernels.


### Tested environments ###

The following table shows a list of tested OSs:

  Name  |  Version  |  Arch.  |      Kernel
:------:|:---------:|:-------:|:------------------:
 Fedora |     14    | x86_64  | 2.6.35.6-45
 Ubuntu | 12.04 LTS | x86_64  | 3.13.0-32-generic
 Ubuntu | 14.04 LTS | x86_64  | 3.16.0-31-generic

In general, any Linux distribution using systemd will have problems, because the interface names depends of driver's probe order, and with systemd this is no longer predictable.
Version v197 and newer of systemd incorporate a feature called "Predictable Network Interface Names", but even on those systems the current driver doesn't work properly.


### Compilation and installation ###

Before start (software dependencies):

- gcc compiler (gcc)
- kernel headers (kernel-devel)


To compile and install the software, follow these steps:

1. Copy the "NC_software" folder to its definitive location.
2. Make sure that all scripts files have the "execute" permission. One script is at "NC_software/init_NC_system.sh" and the others at "NC_software/NetFPGA_1G/scripts/".
3. Open a terminal and obtain super user (root) privileges.
4. Navigate to "NC_software/NetFPGA_1G/" folder.
5. Run the command `make`. The driver should compile following by the utilities.
6. Review the output from the previous step and make sure that everything was compiled ok.
7. Test the driver:
    - Navigate to the folder "NC_software/NetFPGA_1G/kernel/" and run the command `insmod nf2.ko`. This should load the driver into the Kernel.
    - Run the command `ifconfig -a`. This command prints a list of network interfaces present in the system. Each NetFPGA-1G is integrated by 4 interfaces, named from "nf2c0" to "nf2c3". In the case that more than one NetFPGA-1G is present, its interfaces will be named from "nf2c4" to "nf2c7", and so on.
    - If everything is working, navigate back to the "NC_software/NetFPGA_1G/" folder.
8. Run the command `make install`. This will install the driver in the appropriate folder and the utilities inside "/usr/local/bin/".
9. You should be able to operate the NetFPGA-1G from the workstation.
10. Optionally follow the installation of the startup script inside the parent folder to automate somethings.


### Utilities ###

The official repository provides some utilities:

- nf_download: Used to program the FPGA inside the NetFPGA board. It must be done on every reboot because the NetFPGA-1G doesn't have a flash memory.
- regread: Read a register using the memory map from the NetFPGA.
- regwrite: write a register using the memory map from the NetFPGA.


On top of the base ones, we provide some additional utilities:

- memfill: Write a memory area in the FPGA memory space. The data is read from a file.
- readall: Read a memory area in the FPGA memory space. The data is written to a file.
- readcounters: Read and display the counters of the Network Processor if it's present in the NetFPGA.


### Scripts ###

This and the "scripts" folder contain a set of scripts to automate the driver loading and programing of the NetFPGAs at system startup.
The scripts assume that the driver and the utilities are placed in their originals locations.
The main script is "init_NetFPGA_1G.sh" which loads the driver and program the NetFPGAs. Other scripts are provided for a more fine-grained control over the NetFPGAs.

NOTE: These scripts were designed for a platform with 2 NetFPGAs, one acting as a real-time switch and the other as a traffic injector.
