#!/bin/bash

## --- Configuration ---
# Kernel folder name
kernel_fname=`uname -r`
# Driver file name
driver_filename="nf2.ko"
# Final driver path
driver_path=/lib/modules/$kernel_fname/kernel/drivers/$driver_filename


## --- Execution ---

# Get the location of this script (file)
BASE_PATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd $BASE_PATH

# Load driver if needed
./driver__is_loaded.sh
if [ $? -eq 0 ]; then
    echo "Loading driver"
    # Check for driver file existence
    if ! test -f "$driver_path" ; then
        echo "Driver file not found. Possible causes are:"
        echo "- The driver is not installed/compiled. Run 'make install'"
        echo "- A different Kernel version was recently installed and the driver must be recompiled (make install)"
        exit 1
    fi
    insmod "$driver_path"
    # Time to init the driver
    sleep 1s
else
    echo "INFO: Driver already loaded"
fi

