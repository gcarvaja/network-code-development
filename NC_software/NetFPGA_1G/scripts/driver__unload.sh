#!/bin/bash

## --- Configuration ---
# Relative path to the driver
driver_name="nf2"


## --- Execution ---
# Unload driver
rmmod "$driver_name"

