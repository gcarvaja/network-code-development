#!/bin/bash


## --- Configuration ---
# Info utility
nf_info_bin=/usr/local/bin/nf_info
# Program utility
nf_download_bin="/usr/local/bin/nf_download"

# (default) First interface of the NetFPGA
iface="nf2c0"
# File to program
bit_file="../FPGA_firmware/switch__RT_no_ncp.bit"

## Command line configuration
# Interface
if [[ $1 ]]; then
    iface=$1
fi
# bitfile
if [[ $2 ]]; then
    bit_file=$2
fi


## --- Execution ---

# Check for binary existence
if ! test -f "$nf_info_bin" ; then
    echo "Info binary not found, please make sure this is the path: $nf_info_bin"
    exit 1
elif ! test -f "$nf_download_bin" ; then
    echo "Download binary not found, please make sure this is the path: $nf_download_bin"
    exit 1
fi

# Request board info. Added as a fix to communications errors
$nf_info_bin -i "$iface"

# Program the NetFPGA
$nf_download_bin -i "$iface" "$bit_file" -n

