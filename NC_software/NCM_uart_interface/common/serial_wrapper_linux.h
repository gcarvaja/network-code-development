#ifndef SERIAL_WRAPPER_LINUX_H_INCLUDED
#define SERIAL_WRAPPER_LINUX_H_INCLUDED

#include <string.h>
#include <termios.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>


static speed_t baudrate_to_termios( int baudrate_int ) {
    // Translation table
    // You can add or remove values as needed
    switch( baudrate_int ) {
        case 0:      return B0;
        case 1200:   return B1200;
        case 9600:   return B9600;
        case 19200:  return B19200;
        case 38400:  return B38400;
        case 57600:  return B57600;
        case 115200: return B115200;
        case 230400: return B230400;
        case 460800: return B460800;
        case 500000: return B500000;
        case 576000: return B576000;
        case 921600: return B921600;
        default: return -1;
    }
}


static int port_handle = -1;

int SerialWrapper_OpenPort( char *port_name, int baud_rate, int read_timeout_ms, char *error_str_ptr ) {

    port_handle = open( port_name, O_RDWR | O_NOCTTY | O_SYNC );
    if( port_handle == -1 ) {
        strcpy( error_str_ptr, strerror(errno) );
        return -1;
    }

    // --- Port configuration ---
    struct termios tty_cfg;
    memset( &tty_cfg, 0, sizeof(tty_cfg) );

    // Input and output baudrates
    int res = 0;
    res += cfsetispeed( &tty_cfg, baudrate_to_termios(baud_rate) );
    res += cfsetospeed( &tty_cfg, baudrate_to_termios(baud_rate) );
    if( res != 0 ) {
        sprintf( error_str_ptr, "Unsupported baudrate: %i", baud_rate );
        return -1;
    }

    // 8-bit chars (mask and select 8 bits)
    tty_cfg.c_cflag = (tty_cfg.c_cflag & ~CSIZE) | CS8;
    // disable IGNBRK for mismatched speed tests; otherwise receive break
    // as \000 chars
    tty_cfg.c_iflag &= ~IGNBRK;  // disable break processing
    tty_cfg.c_lflag = 0;         // no signaling chars, no echo,
                                 // no canonical processing
    tty_cfg.c_oflag     = 0;     // no remapping, no delays
    tty_cfg.c_cc[VMIN]  = 1;     // read blocks execution
    tty_cfg.c_cc[VTIME] = 5;     // 0.5 seconds read timeout // TODO: read_timeout_ms

    tty_cfg.c_iflag &= ~(IXON | IXOFF | IXANY); // shut off xon/xoff ctrl

    tty_cfg.c_cflag |= (CLOCAL | CREAD);    // ignore modem controls,
                                            // enable reading
    tty_cfg.c_cflag &= ~(PARENB | PARODD);  // shut off parity
    tty_cfg.c_cflag |= 0;
    tty_cfg.c_cflag &= ~CSTOPB;
    tty_cfg.c_cflag &= ~CRTSCTS;

    // Try to apply the configuration to the port
    if( tcsetattr( port_handle, TCSANOW, &tty_cfg ) != 0 ) {
        sprintf( error_str_ptr, "Failed on configuring port: %s", strerror(errno) );
        close( port_handle );
        port_handle = -1;
        return -1;
    }

    return 0;
}


void SerialWrapper_ClosePort( void ) {

    if( port_handle != -1 )
        close( port_handle );

    port_handle = -1;
    return;
}


int SerialWrapper_SendData( uint8_t *data_buffer, uint16_t data_len ) {

    if( port_handle == -1 )
        return -1;

    int tx_words = write( port_handle, data_buffer, data_len );
    if( tx_words < 0 ) {
        SerialWrapper_ClosePort();
        return -1;
    }
    // Wait for transmission to complete
    tcdrain( port_handle );
    return 0;
}


int SerialWrapper_RecvData( uint8_t *data_buffer, uint16_t max_data_len ) {

    if( port_handle == -1 )
        return -1;

    int rx_words = read( port_handle, data_buffer, max_data_len );
    if( rx_words == -1 ) {
        SerialWrapper_ClosePort();
        return -1;
    }

    return 0;
}

void SerialWrapper_FlushRxBuff( void ) {

    if( port_handle == -1 )
        return;

    tcflush( port_handle, TCIFLUSH );
    return;
}

#endif // SERIAL_WRAPPER_LINUX_H_INCLUDED
