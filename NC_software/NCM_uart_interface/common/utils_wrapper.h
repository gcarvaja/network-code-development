#ifndef UTILS_WRAPPER_H_INCLUDED
#define UTILS_WRAPPER_H_INCLUDED



#ifdef platform_windows
  #include "utils_wrapper_win.h"
#elif defined(platform_linux)
  #include "utils_wrapper_linux.h"
#else
  #error "You must define a platform first"
#endif


#endif // UTILS_WRAPPER_H_INCLUDED
