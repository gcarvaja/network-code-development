#ifndef SERIAL_WRAPPER_H_INCLUDED
#define SERIAL_WRAPPER_H_INCLUDED



#ifdef platform_windows
  #include "serial_wrapper_win.h"
#elif defined(platform_linux)
  #include "serial_wrapper_linux.h"
#else
  #error "You must define a platform first"
#endif


#endif // SERIAL_WRAPPER_H_INCLUDED
