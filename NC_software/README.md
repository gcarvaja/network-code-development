# Network Code software #

This folder contains all the software developed for the different Network Code boards.


### File organization ###

Due to the scripts executed at startup to program the boards, it's recommended to place this folder into a common folder so they can be accessed by everyone.
From this point onwards we assume that the main software folder is located at "/home/some_user/NC_software/".


### Startup script ###

The script "init_NC_system.sh" executes on GNU/Linux platforms at system startup and initializes the different boards.
Because we can't find an unified way of registering the script to run at startup, please refer to the documentation of your current GNU/Linux distribution.
Usual locations of the startup file are:

- /etc/rc.d/rc.local (for Fedora)
- /etc/rc.local (for Ubuntu)

NOTE: The file might not be present on distributions using the `systemd` daemon, but it should work if you create it.

An example of the file-content is:

    #!/bin/sh
    
    /home/some_user/NC_software/init_NC_system.sh
    
