
/* A Bison parser, made by GNU Bison 2.4.1.  */

/* Skeleton interface for Bison's Yacc-like parsers in C
   
      Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002, 2003, 2004, 2005, 2006
   Free Software Foundation, Inc.
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.
   
   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */


/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     TKN_NUM = 258,
     TKN_ID = 259,
     TKN_ARG = 260,
     TKN_ASIGN = 261,
     TKN_PTOCOMA = 262,
     TKN_COMA = 263,
     END = 264,
     TKN_MULT = 265,
     TKN_DIV = 266,
     TKN_MAS = 267,
     TKN_MENOS = 268,
     TKN_PAA = 269,
     TKN_PAC = 270,
     TKN_TAG = 271,
     TKN_FUTURE = 272,
     TKN_HALT = 273,
     TKN_CREATE = 274,
     TKN_SEND = 275,
     TKN_RECEIVE = 276,
     TKN_NOP = 277,
     TKN_SIGNAL = 278,
     TKN_SYNC = 279,
     TKN_COUNT = 280,
     TKN_MODE = 281,
     TKN_IF = 282
   };
#endif
/* Tokens.  */
#define TKN_NUM 258
#define TKN_ID 259
#define TKN_ARG 260
#define TKN_ASIGN 261
#define TKN_PTOCOMA 262
#define TKN_COMA 263
#define END 264
#define TKN_MULT 265
#define TKN_DIV 266
#define TKN_MAS 267
#define TKN_MENOS 268
#define TKN_PAA 269
#define TKN_PAC 270
#define TKN_TAG 271
#define TKN_FUTURE 272
#define TKN_HALT 273
#define TKN_CREATE 274
#define TKN_SEND 275
#define TKN_RECEIVE 276
#define TKN_NOP 277
#define TKN_SIGNAL 278
#define TKN_SYNC 279
#define TKN_COUNT 280
#define TKN_MODE 281
#define TKN_IF 282




#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef union YYSTYPE
{

/* Line 1676 of yacc.c  */
#line 37 "bison_06.y"

	float real;
	int ival;
	char *sval;



/* Line 1676 of yacc.c  */
#line 114 "y.tab.h"
} YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
#endif

extern YYSTYPE yylval;


