`timescale 1ns / 1ps

module user_app (
		output wire        DDR2CLK_P,
		output wire        DDR2CLK_N,
		output wire        DDR2CKE,
		output wire        DDR2RASN,
		output wire        DDR2CASN,
		output wire        DDR2WEN,
		inout  wire        DDR2RZQ,
		inout  wire        DDR2ZIO,
		output wire [2:0]  DDR2BA,

		output wire [12:0] DDR2A,
		inout  wire [15:0] DDR2DQ,

		inout  wire        DDR2UDQS_P,
		inout  wire        DDR2UDQS_N,
		inout  wire        DDR2LDQS_P,
		inout  wire        DDR2LDQS_N,
		output wire        DDR2LDM,
		output wire        DDR2UDM,
		output wire        DDR2ODT,
		
        // Clocks an timer ticks
        input  wire        app_clk,
        input  wire        app_timer_tick,
        input  wire        mem_clk,
        // NCM interface
        output reg  [15:0] ncm_varram_addr,
        output reg         ncm_varram_wen,
        output reg  [31:0] ncm_varram_din,
        input  wire [31:0] ncm_varram_dout,
        // Video display (read video line from RAM)
        input  wire                    vid_clk,
        input  wire                    vid_preload_line,
		input  wire [H_POSIT_BITS-1:0] vid_hpos,
		input  wire [V_POSIT_BITS-1:0] vid_vpos,
        output reg  [23:0]             vid_data_out,
        // Receive command status
        input wire   receive_fault_empty,
        // Status
        output reg   app_status,
        // Control signals
        input wire dynamic_TDMA__en
    );


// ---------- INCLUDES ----------
`include "verilog_utils.vh"


// ---------- PARAMETERS ----------
    // Display video size
    parameter DISP_H_RES_PIX = 640;
    parameter DISP_V_RES_PIX = 480;
    // Optional parameters. Included for compatibility.
    // Default value (0) will automatically calculate the ports width. 
    parameter H_POS_BITS = 0;
    parameter V_POS_BITS = 0;
    //
    parameter SYNT_NODE_2 = 1;


// ---------- LOCAL PARAMETERS ----------
    localparam [0:0] SEND_EVEN_LINES = SYNT_NODE_2;
    //
    localparam VARRAM_VAR_1_ADDR = 'h0;
    localparam VARRAM_VAR_1_SIZE = 160; // 150 = 600 pix; 160 = 640 pix
    //
    localparam VARRAM_VAR_2_ADDR = 'hC8;
    localparam VARRAM_VAR_2_SIZE = 1;
    //
    localparam VARRAM_VAR_3_ADDR = 'hD0;
    localparam VARRAM_VAR_3_SIZE = 160;
    //
    localparam VARRAM_VAR_4_ADDR = 'h198;
    localparam VARRAM_VAR_4_SIZE = 1;
    ////////////////////////////////////////////////////////////
    localparam PIXELS_PER_WORD   = 4;   // 8 bits gray-scale
	localparam H_IMG_RES         = PIXELS_PER_WORD*VARRAM_VAR_1_SIZE;
    localparam V_IMG_RES         = 480;
    //---------------------
    localparam H_POSIT_BITS = (H_POS_BITS!=0)? (H_POS_BITS) : (ceil_log2(DISP_H_RES_PIX-1));
    localparam V_POSIT_BITS = (V_POS_BITS!=0)? (V_POS_BITS) : (ceil_log2(DISP_V_RES_PIX-1));
    localparam [12:0] DISP_H_RES_PIX_FIX = DISP_H_RES_PIX*4;


    // ---- MODULE ----
    
    // DDR2 interface
    wire c3_calib_done;
    wire reset;
	wire c3_clk0;
    
	wire        c3_p0_cmd_en,        c3_p1_cmd_en,        c3_p2_cmd_en,        c3_p3_cmd_en;
    wire [2:0]  c3_p0_cmd_instr,     c3_p1_cmd_instr,     c3_p2_cmd_instr,     c3_p3_cmd_instr;
    wire [5:0]  c3_p0_cmd_bl,        c3_p1_cmd_bl,        c3_p2_cmd_bl,        c3_p3_cmd_bl;
    wire [29:0] c3_p0_cmd_byte_addr, c3_p1_cmd_byte_addr, c3_p2_cmd_byte_addr, c3_p3_cmd_byte_addr;

    wire [7:0]  c3_p0_wr_mask,       c3_p1_wr_mask,       c3_p2_wr_mask,       c3_p3_wr_mask;
    wire [31:0] c3_p0_wr_data,       c3_p1_wr_data,       c3_p2_wr_data,       c3_p3_wr_data;
    wire        c3_p0_wr_full,       c3_p1_wr_full,       c3_p2_wr_full,       c3_p3_wr_full;
    wire        c3_p0_wr_empty,      c3_p1_wr_empty,      c3_p2_wr_empty,      c3_p3_wr_empty;
    wire [6:0]  c3_p0_wr_count,      c3_p1_wr_count,      c3_p2_wr_count,      c3_p3_wr_count;
    wire        c3_p0_wr_en,         c3_p1_wr_en,         c3_p2_wr_en,         c3_p3_wr_en;

    wire [31:0] c3_p0_rd_data,       c3_p1_rd_data,       c3_p2_rd_data,       c3_p3_rd_data;
    wire [6:0]  c3_p0_rd_count,      c3_p1_rd_count,      c3_p2_rd_count,      c3_p3_rd_count;
	wire        c3_p0_rd_en,         c3_p1_rd_en,         c3_p2_rd_en,         c3_p3_rd_en;
	wire        c3_p0_rd_empty,      c3_p1_rd_empty,      c3_p2_rd_empty,      c3_p3_rd_empty;

    // Config
    assign reset = 0;
    
	assign c3_p0_wr_mask = 0;
	assign c3_p1_wr_mask = 0;
	assign c3_p2_wr_mask = 0;
	assign c3_p3_wr_mask = 0;
	
    // Ports utilization
    // Port 0: Read
        //assign c3_p0_rd_en = 0;
        assign c3_p0_wr_en = 0;
    // Port 1: Not used
        assign c3_p1_rd_en = 0;
        assign c3_p1_wr_en = 0;
    // Port 2: Write
        assign c3_p2_rd_en = 0;
        //assign c3_p2_wr_en = 0;
    // Port 3: Not used
        assign c3_p3_rd_en = 0;
        assign c3_p3_wr_en = 0;
    
    ///////////////
    ddr2_user_interface
    DDR2_MCB_1 (
        // Physical interface (PINs)
		.DDR2CLK_P           (DDR2CLK_P),
		.DDR2CLK_N           (DDR2CLK_N),
		.DDR2CKE             (DDR2CKE),
		.DDR2RASN            (DDR2RASN),
		.DDR2CASN            (DDR2CASN),
		.DDR2WEN             (DDR2WEN),
		.DDR2RZQ             (DDR2RZQ),
		.DDR2ZIO             (DDR2ZIO),
		.DDR2BA              (DDR2BA),
		.DDR2A               (DDR2A),
		.DDR2DQ              (DDR2DQ),
		.DDR2UDQS_P          (DDR2UDQS_P),
		.DDR2UDQS_N          (DDR2UDQS_N),
		.DDR2LDQS_P          (DDR2LDQS_P),
		.DDR2LDQS_N          (DDR2LDQS_N),
		.DDR2LDM             (DDR2LDM),
		.DDR2UDM             (DDR2UDM),
		.DDR2ODT             (DDR2ODT),
        // Clock
		.clk                 (mem_clk),
        // Status and control
		.c3_calib_done       (c3_calib_done),
		.reset               (reset),
		.c3_clk0             (c3_clk0),
        // Port 0: Bidirectional
		.c3_p0_cmd_en        (c3_p0_cmd_en),
		.c3_p0_cmd_instr     (c3_p0_cmd_instr),
		.c3_p0_cmd_bl        (c3_p0_cmd_bl),
		.c3_p0_cmd_byte_addr (c3_p0_cmd_byte_addr),
		.c3_p0_wr_mask       (c3_p0_wr_mask),
		.c3_p0_wr_data       (c3_p0_wr_data),
		.c3_p0_wr_full       (c3_p0_wr_full),
		.c3_p0_wr_empty      (c3_p0_wr_empty),
		.c3_p0_wr_count      (c3_p0_wr_count), 
		.c3_p0_rd_data       (c3_p0_rd_data),
		.c3_p0_rd_count      (c3_p0_rd_count),
		.c3_p0_rd_en         (c3_p0_rd_en),
		.c3_p0_rd_empty      (c3_p0_rd_empty),
		.c3_p0_wr_en         (c3_p0_wr_en),
        // Port 1: Bidirectional
		.c3_p1_cmd_en        (c3_p1_cmd_en),
		.c3_p1_cmd_instr     (c3_p1_cmd_instr), 
		.c3_p1_cmd_bl        (c3_p1_cmd_bl),
		.c3_p1_cmd_byte_addr (c3_p1_cmd_byte_addr),
		.c3_p1_wr_mask       (c3_p1_wr_mask),
		.c3_p1_wr_full       (c3_p1_wr_full),
		.c3_p1_wr_empty      (c3_p1_wr_empty),
		.c3_p1_wr_count      (c3_p1_wr_count),
		.c3_p1_wr_data       (c3_p1_wr_data),
		.c3_p1_rd_data       (c3_p1_rd_data),
		.c3_p1_rd_count      (c3_p1_rd_count),
		.c3_p1_rd_en         (c3_p1_rd_en),
		.c3_p1_rd_empty      (c3_p1_rd_empty),
		.c3_p1_wr_en         (c3_p1_wr_en),
        // Port 2: Bidirectional
		.c3_p2_cmd_en        (c3_p2_cmd_en),
		.c3_p2_cmd_instr     (c3_p2_cmd_instr),
		.c3_p2_cmd_bl        (c3_p2_cmd_bl),
		.c3_p2_cmd_byte_addr (c3_p2_cmd_byte_addr),
		.c3_p2_wr_mask       (c3_p2_wr_mask),
		.c3_p2_wr_full       (c3_p2_wr_full),
		.c3_p2_wr_empty      (c3_p2_wr_empty),
		.c3_p2_wr_count      (c3_p2_wr_count),
		.c3_p2_wr_data       (c3_p2_wr_data),
		.c3_p2_rd_data       (c3_p2_rd_data),
		.c3_p2_rd_count      (c3_p2_rd_count),
		.c3_p2_rd_en         (c3_p2_rd_en),
		.c3_p2_rd_empty      (c3_p2_rd_empty),
		.c3_p2_wr_en         (c3_p2_wr_en),
        // Port 3: Bidirectional
		.c3_p3_cmd_en        (c3_p3_cmd_en),
		.c3_p3_cmd_instr     (c3_p3_cmd_instr),
		.c3_p3_cmd_bl        (c3_p3_cmd_bl),
		.c3_p3_cmd_byte_addr (c3_p3_cmd_byte_addr),
		.c3_p3_wr_mask       (c3_p3_wr_mask),
		.c3_p3_wr_full       (c3_p3_wr_full),
		.c3_p3_wr_empty      (c3_p3_wr_empty),
		.c3_p3_wr_count      (c3_p3_wr_count),
		.c3_p3_wr_data       (c3_p3_wr_data),
		.c3_p3_rd_data       (c3_p3_rd_data),
		.c3_p3_rd_count      (c3_p3_rd_count),
		.c3_p3_rd_en         (c3_p3_rd_en),
		.c3_p3_rd_empty      (c3_p3_rd_empty),
		.c3_p3_wr_en         (c3_p3_wr_en)
	);


    // -- Video line buffer --
    wire [7:0]              line_buff_dout;
    wire                    line_buff_we;
    wire [H_POSIT_BITS-1:0] line_buff_wr_addr;
    wire [31:0]             dispatcher_data_out;
    
    RAM__indep_rd_port__wr_port #(
        .WORDS_COUNT (DISP_H_RES_PIX),
        .WORDS_BITS  (8)
    )
    RAM_read_line_buffer_1 (
        // Write port (Read dispatcher)
        .wr_port_clk  (c3_clk0),
        .wr_port_addr (line_buff_wr_addr),
        .wr_port_we   (line_buff_we),
        .wr_port_din  (dispatcher_data_out[7:0]),
        // Read port (vid)
        .rd_port_clk  (vid_clk),
        .rd_port_addr (vid_hpos),
        .rd_port_dout (line_buff_dout)
    );
    
    always @( posedge vid_clk ) begin
        vid_data_out <= { line_buff_dout, line_buff_dout, line_buff_dout };
    end
    
    // -- Memory read dispatcher --
    wire        read_disp_busy;
    reg  [29:0] init_add_rd_vid = 0;
	reg         os_start_rd_vid = 0;
    mem_dispatcher__read #(
		.FIFO_LENGTH    (64),
		.WORDS_TO_READ  (DISP_H_RES_PIX),
        .BUFF_ADDR_BITS (H_POSIT_BITS),
        .PORT_64_BITS   (0)
	)
    mem_dispatcher__read_unit (
        // Clock
		.clk                ( c3_clk0 ),
        // Control
        .os_start           ( os_start_rd_vid ),
		.init_mem_addr      ( init_add_rd_vid ),
        .busy_read_unit     ( read_disp_busy ),
        // Data out
        .data_out__we       ( line_buff_we ),
		.data_out__addr     ( line_buff_wr_addr ),
        .data_out           ( dispatcher_data_out ),
        // Memory interface
        .mem_calib_done     ( c3_calib_done ),
        .port_cmd_en        ( c3_p0_cmd_en ),
        .port_cmd_instr     ( c3_p0_cmd_instr ), 
        .port_cmd_bl        ( c3_p0_cmd_bl ),
        .port_cmd_byte_addr ( c3_p0_cmd_byte_addr ),
        .port_rd_en         ( c3_p0_rd_en ),
        .port_rd_data_in    ( c3_p0_rd_data ),        
        .port_rd_empty      ( c3_p0_rd_empty )    
	);

    // -- Video reader --
    localparam [27:0] DISP_VIDEO_BASE_ADDR = 0;

    always @( posedge vid_clk ) begin
        // One-shot start signal
        if( os_start_rd_vid )
            os_start_rd_vid <= 0;
        // Wait for preload signal
        if( vid_preload_line ) begin
            os_start_rd_vid <= 1;
            init_add_rd_vid <= DISP_VIDEO_BASE_ADDR + vid_vpos*DISP_H_RES_PIX_FIX; // TODO: Eliminate multiplier
            // init_add_rd_vid <= (new_vid_frame)? (DISP_VIDEO_BASE_ADDR) : (init_add_rd_vid + DISP_H_RES_PIX_FIX);
        end
    end


    // ----- Video writer -----
    reg  [3:0] app_state       = 0;
    reg  [2:0] pix_count_1     = 0;
    reg [10:0] vid_line_number = 0;
    reg        vid_line_active = 0;
    
    reg  [29:0] init_add_wr = 0;
    wire [15:0] dispatcher_addr;
    wire        write_disp_busy;
    wire [31:0] dispatcher_din;
    
    //wire [a:0] ncm_varram_addr_mux_1;
    //wire [a:0] ncm_varram_addr_mux_2;

    // Processing buffer
    reg  [15:0] proc_line_buff_wr_addr = 0;
    reg         proc_line_buff_we      = 0;
    reg  [7:0]  proc_line_buff_wr_din;
    wire [7:0]  proc_line_buff_rd_dout;
    
    
    wire [15:0] filter_in_addr;
    wire [7:0]  filter_in_data;
    

    RAM__indep_rd_port__wr_port #(
        .WORDS_COUNT (DISP_H_RES_PIX),
        .WORDS_BITS  (8),
        .ADDR_BITS   (16)
    )
    proc_line_buffer_1 (
        // Write port (app)
        .wr_port_clk  (app_clk),
        .wr_port_addr (proc_line_buff_wr_addr),
        .wr_port_we   (proc_line_buff_we),
        .wr_port_din  (proc_line_buff_wr_din),
        // Read port (filter)
        .rd_port_clk  (app_clk),
        .rd_port_addr (filter_in_addr),
        .rd_port_dout (filter_in_data)
    );
    assign dispatcher_din = { 24'd0, proc_line_buff_rd_dout[7:0] };
    
    
    reg         filter_start  = 0;
    wire        filter_busy;
    wire [8:0]  filter_line_out_num;
    wire        filter_line_out_act;
    //
    reg  [15:0] line_out_addr_1 = 0;
    wire [7:0]  line_out_data_1;

    generate
    if( SYNT_NODE_2 == 1 ) begin
        filter__sobel #(
            .H_RES_PIX   (640),
            .V_RES_PIX   (480),
            .LINE_N_BITS (9),  // Bits for the line number
            .LINE_B_BITS (16),  // Bits for the line-buffer address (input and output)
            .THRESHOLD   (20000)
        )
        filter_1 (
            .clk            (app_clk),
            // [in] Control and status signals
            .line_in_ready  (filter_start),
            .line_in_num    (vid_line_number[8:0]),
            .line_in_active (vid_line_active),
            // [out] Control and status signals
            .busy            (filter_busy),
            .line_out_num    (filter_line_out_num),
            .line_out_active (filter_line_out_act),
            // Input line-buffer interface
            .line_in_addr   (filter_in_addr),
            .line_in_data   (filter_in_data),
            // Output line-buffer interface 1: user_app
            .line_out_clk_1   (app_clk),
            .line_out_addr_1  (line_out_addr_1),
            .line_out_data_1  (line_out_data_1),
            // Output line-buffer interface 2: Write dispatcher (RAM)
            .line_out_clk_2   (c3_clk0),
            .line_out_addr_2  (dispatcher_addr),
            .line_out_data_2  (proc_line_buff_rd_dout)
        );
    end
    else begin
        filter__sobel #(
            .H_RES_PIX   (640),
            .V_RES_PIX   (480),
            .LINE_N_BITS (9),  // Bits for the line number
            .LINE_B_BITS (16),  // Bits for the line-buffer address (input and output)
            .THRESHOLD   (60000)
        )
        filter_2 (
            .clk            (app_clk),
            // [in] Control and status signals
            .line_in_ready  (filter_start),
            .line_in_num    (vid_line_number[8:0]),
            .line_in_active (vid_line_active),
            // [out] Control and status signals
            .busy            (filter_busy),
            .line_out_num    (filter_line_out_num),
            .line_out_active (filter_line_out_act),
            // Input line-buffer interface
            .line_in_addr   (filter_in_addr),
            .line_in_data   (filter_in_data),
            // Output line-buffer interface 1: user_app
            .line_out_clk_1   (app_clk),
            .line_out_addr_1  (line_out_addr_1),
            .line_out_data_1  (line_out_data_1),
            // Output line-buffer interface 2: Write dispatcher (RAM)
            .line_out_clk_2   (c3_clk0),
            .line_out_addr_2  (dispatcher_addr),
            .line_out_data_2  (proc_line_buff_rd_dout)
        );
    end
    endgenerate
    
    reg [10:0] rcv_vid_line_number;
    reg        write_vid_line_to_ram = 0;
    
    wire       copy_stop_cond_var_3  = (ncm_varram_addr == (VARRAM_VAR_3_ADDR+VARRAM_VAR_3_SIZE));
    reg [2:0]  pix_count_3           = 0;
    reg        rcv_active_line       = 0;
    reg        line__non_black_data  = 0;
    
    always @( posedge app_clk ) begin
        
        // State 0: Wait timer signal
        if( app_state == 0 ) begin
            pix_count_1           <= 0;
            pix_count_3           <= 0;
            line_out_addr_1       <= 0;
            filter_start          <= 0;
            ncm_varram_wen        <= 0;
            write_vid_line_to_ram <= 0;
            line__non_black_data  <= 0;
            app_status            <= 0;
            if( app_timer_tick ) begin
                rcv_active_line        <= ncm_varram_dout[30];
                rcv_vid_line_number    <= ncm_varram_dout[21:11]; // Global line number (from master)
                ncm_varram_addr        <= ncm_varram_addr + 1'b1;
                proc_line_buff_wr_addr <= 0;
                app_state              <= 1;
            end
            else
                ncm_varram_addr <= VARRAM_VAR_1_ADDR;
        end
        
        // State 1: Jump to copy black data or output from varram (and wait varram read delay)
        if( app_state == 1 ) begin
            app_status <= 1;
            // Active and valid lines
            if( receive_fault_empty == 0 ) begin
                app_state             <= 2;
                proc_line_buff_wr_din <= ncm_varram_dout[31:24];
                vid_line_number       <= rcv_vid_line_number;
                vid_line_active       <= rcv_active_line;
            end
            else begin
                app_state             <= 3;
                proc_line_buff_wr_din <= 8'd0;
                vid_line_number       <= (vid_line_number==DISP_V_RES_PIX-1)? (0) : (vid_line_number + 1'b1);
            end
        end
        
        // State 2: Copy data from varram to proc buffer
        if( app_state == 2 ) begin
            if( proc_line_buff_we )
                proc_line_buff_wr_addr <= proc_line_buff_wr_addr + 1'b1;
                pix_count_1            <= (pix_count_1 == PIXELS_PER_WORD-1)? (0) : (pix_count_1 + 1'b1);
            if( pix_count_1 == 0 ) begin
                proc_line_buff_wr_din <= ncm_varram_dout[31:24];
            end
            //
            if( pix_count_1 == 1 ) begin
                proc_line_buff_wr_din <= ncm_varram_dout[23:16];
            end
            //
            if( pix_count_1 == 2 ) begin
                proc_line_buff_wr_din <= ncm_varram_dout[15:8];
                ncm_varram_addr       <= ncm_varram_addr + 1'b1;
            end
            //
            if( pix_count_1 == 3 ) begin
                proc_line_buff_wr_din <= ncm_varram_dout[7:0];
            end
            //
            if( proc_line_buff_wr_addr == H_IMG_RES-1 ) begin
                proc_line_buff_we <= 0;
                app_state         <= 4;
            end
            else
                proc_line_buff_we <= 1;
        end

        // State 3: Copy black-data to write buffer
        if( app_state == 3 ) begin
            proc_line_buff_wr_din <= 8'd0;
            //
            if( proc_line_buff_we )
                proc_line_buff_wr_addr <= proc_line_buff_wr_addr + 1'b1;
            //
            if( proc_line_buff_wr_addr == H_IMG_RES-1 ) begin
                proc_line_buff_we <= 0;
                app_state         <= 4;
            end
            else
                proc_line_buff_we <= 1;
        end

        // State 4: Begin processing
        if( app_state == 4 ) begin
            // Copy or skip filtered line output
            if( filter_busy == 0 ) begin
                app_state             <= 5;
                ncm_varram_addr       <= VARRAM_VAR_3_ADDR;
                ncm_varram_din        <= { {1'b0,filter_line_out_act,8'd0}, vid_line_number[10:0], {2'd0,filter_line_out_num[8:0]} }; // global pos: 11 bits; curr_vpos: 11 bits
                write_vid_line_to_ram <= 1;
            end
            else begin
                app_state <= 0;
            end
            
        end
        
        // State 5: Copy first word to varram
        if( app_state == 5 ) begin
            if( ncm_varram_wen == 0 ) begin
                ncm_varram_wen  <= 1;
            end
            if( ncm_varram_wen == 1 ) begin
                ncm_varram_wen  <= 0;
                line_out_addr_1 <= line_out_addr_1 + 1'b1;
                app_state       <= 6;
            end
        end
        
        // State 6: Copy data from filter
        if( app_state == 6 ) begin
        
            write_vid_line_to_ram <= 0;
            
            line_out_addr_1  <= line_out_addr_1 + 1'b1;
            pix_count_3      <= (pix_count_3 == PIXELS_PER_WORD-1)? (0) : (pix_count_3 + 1'b1);
            //
            if( pix_count_3 == 0 ) begin
                ncm_varram_din[31:24] <= line_out_data_1;
                if( line_out_addr_1 < DISP_H_RES_PIX-1 )
                    line__non_black_data  <= (line__non_black_data | (line_out_data_1!=0) );
            end
            //
            if( pix_count_3 == 1 ) begin
                ncm_varram_din[23:16] <= line_out_data_1;
                line__non_black_data  <= (line__non_black_data | (line_out_data_1!=0) );
            end
            //
            if( pix_count_3 == 2 ) begin
                ncm_varram_din[15:8] <= line_out_data_1;
                line__non_black_data <= (line__non_black_data | (line_out_data_1!=0) );
            end
            //
            if( pix_count_3 == 3 ) begin
                ncm_varram_wen        <= 1;
                ncm_varram_din[7:0]   <= line_out_data_1;
                line__non_black_data  <= (line__non_black_data | (line_out_data_1!=0) );
                ncm_varram_addr       <= ncm_varram_addr + 1'b1;
            end
            else
                ncm_varram_wen <= 0;
            //
            if( copy_stop_cond_var_3 ) begin
                filter_start   <= 1;
                ncm_varram_wen <= 0;
                app_state      <= 7;
            end
        end
        
        // State 7: Copy additional variable/data
        if( app_state == 7 ) begin
            ncm_varram_addr <= VARRAM_VAR_2_ADDR;
            ncm_varram_wen  <= 1;
            app_state       <= 8;
            if( vid_line_number[10:0] < (DISP_V_RES_PIX/4) )
                ncm_varram_din  <= ((~dynamic_TDMA__en|line__non_black_data) && (SEND_EVEN_LINES));
            else if( vid_line_number[10:0] < (DISP_V_RES_PIX/2) )
                ncm_varram_din  <= ((~dynamic_TDMA__en|line__non_black_data) && (!SEND_EVEN_LINES));
            else if( vid_line_number[10:0] < ((3*DISP_V_RES_PIX)/4) )
                ncm_varram_din  <= ((~dynamic_TDMA__en|line__non_black_data) && (SEND_EVEN_LINES));
            else
                ncm_varram_din  <= ((~dynamic_TDMA__en|line__non_black_data) && (!SEND_EVEN_LINES));
        end
        
        
        // State 8: Wait for write dispatcher if necessary
        if( app_state == 8 ) begin
            ncm_varram_wen <= 0;
            if( write_disp_busy == 0 ) begin
                app_state    <= 0;
            end
        end

    end // always
    


    // ---- Write line to memory ---- 
    reg os_start_wr      = 0;
    
    always @( posedge app_clk ) begin

        // Calculate DDR write address and start copying
        if( write_vid_line_to_ram ) begin
            init_add_wr <= filter_line_out_num*DISP_H_RES_PIX_FIX;
            os_start_wr <= 1;
        end
        else
            os_start_wr <= 0;
    end
    
    mem_dispatcher__write # (
		.MICRO_TOP        (64),
		.MACRO_TOP        (H_IMG_RES),
		.RAM_ADDR_BITS    (16),
		.DDR_PORT_BITS    (32)
	)
    mem_dispatcher__write_unit (
		.clk                (c3_clk0),
        // Control
		.os_start           (os_start_wr),
		.init_mem_addr      (init_add_wr),
        .busy_unit          (write_disp_busy),
        // Data input
		.data_in__addr      (dispatcher_addr),
		.data_in            (dispatcher_din),
        // RAM controller interface
        .mem_calib_done     (c3_calib_done),
        .port_cmd_en        (c3_p2_cmd_en),
		.port_cmd_instr     (c3_p2_cmd_instr),
		.port_cmd_bl        (c3_p2_cmd_bl),
		.port_cmd_byte_addr (c3_p2_cmd_byte_addr),
        .port_wr_en         (c3_p2_wr_en),
		.port_wr_data_out   (c3_p2_wr_data),
		.port_wr_full       (c3_p2_wr_full)
	);


endmodule
