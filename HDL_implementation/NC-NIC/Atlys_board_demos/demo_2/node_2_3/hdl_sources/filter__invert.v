`timescale 1ns / 1ps

module filter__invert (
    clk,
    // [in] Control and status signals
    line_in_ready,
    line_in_num,
    line_in_active,
    // [out] Control and status signals
    busy,
    line_out_num,
    line_out_active,
    // Input line-buffer interface
    line_in_addr,
    line_in_data,
    // Output line-buffer interface 1
    line_out_clk_1,
    line_out_addr_1,
    line_out_data_1,
    // Output line-buffer interface 2
    line_out_clk_2,
    line_out_addr_2,
    line_out_data_2
);


// ---------- INCLUDES ----------

// ---------- PARAMETERS ----------
    parameter H_RES_PIX   = 640;
    parameter V_RES_PIX   = 480;
    parameter LINE_N_BITS = 9;  // Bits for the line number
    parameter LINE_B_BITS = 10;  // Bits for the line-buffer address (input and output)


// ---------- LOCAL PARAMETERS ----------

// ---------- INPUTS AND OUTPUTS ----------
    input  wire                   clk;
    // [in] Control and status signals
    input  wire                   line_in_ready;
    input  wire [LINE_N_BITS-1:0] line_in_num;
    input  wire                   line_in_active;
    // [out] Control and status signals
    output reg                    busy            = 0;
    output reg  [LINE_N_BITS-1:0] line_out_num    = 0;
    output reg                    line_out_active = 0;
    // Input line-buffer interface
    output reg  [LINE_B_BITS-1:0] line_in_addr    = 0;
    input  wire [7:0]             line_in_data;
    // Output line-buffer interface 1
    input  wire                   line_out_clk_1;
    input  wire [LINE_B_BITS-1:0] line_out_addr_1;
    output reg  [7:0]             line_out_data_1 = 0;
    // Output line-buffer interface 2
    input  wire                   line_out_clk_2;
    input  wire [LINE_B_BITS-1:0] line_out_addr_2;
    output reg  [7:0]             line_out_data_2 = 0;


// ---------- MODULE ----------

    // Output line-buffer
    reg [7:0] line_buffer_out_1 [0:H_RES_PIX-1];
    reg [7:0] line_buffer_out_2 [0:H_RES_PIX-1];
    // Write interface
    reg                   line_buffer_we   = 0;
    reg [LINE_B_BITS-1:0] line_buffer_addr = 0;
    reg [7:0]             line_buffer_din  = 0;
    
    // Write interface (internal)
    always @( posedge clk ) begin
        if( line_buffer_we ) begin
            line_buffer_out_1[line_buffer_addr] <= line_buffer_din;
            line_buffer_out_2[line_buffer_addr] <= line_buffer_din;
        end
    end
    // Output line-buffer interface 1
    always @( posedge line_out_clk_1 ) begin
        line_out_data_1 <= line_buffer_out_1[line_out_addr_1];
    end
    
    // Output line-buffer interface 2
    always @( posedge line_out_clk_2 ) begin
        line_out_data_2 <= line_buffer_out_2[line_out_addr_2];
    end

    reg [1:0]             FSM_state   = 0;
    reg [LINE_N_BITS-1:0] in_line     = 0;
    reg                   in_line_act = 0;

    always @( posedge clk ) begin

        // State 0: Wait for a new line
        if( FSM_state == 0 ) begin
            line_buffer_we   <= 0;
            line_buffer_addr <= 0;
            if( line_in_ready ) begin
                in_line      <= line_in_num;    // Copy from input line
                in_line_act  <= line_in_active; // Copy from input line
                line_in_addr <= line_in_addr + 1'b1;
                busy         <= 1;
                FSM_state    <= 1;
            end
            else begin
                busy         <= 0;
                line_in_addr <= 0;
            end
        end

        // State 1: Copy inverted data
        if( FSM_state == 1 ) begin
            if( line_buffer_we )
                line_buffer_addr <= line_buffer_addr + 1'b1;
            //
            line_in_addr     <= line_in_addr + 1'b1;
            line_buffer_we   <= 1;
            line_buffer_din  <= ~line_in_data;
            if( line_buffer_addr == (H_RES_PIX-1) ) begin
                line_out_num    <= in_line;
                line_out_active <= line_in_active;
                FSM_state       <= 0;
            end
        end
    end


endmodule