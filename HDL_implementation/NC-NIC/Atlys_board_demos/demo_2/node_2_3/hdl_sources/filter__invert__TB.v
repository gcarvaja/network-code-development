`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////

module filter__invert__TB;

	// Inputs
	reg        clk;
    // [in] Control and status signals
	reg        line_in_ready;
	reg [8:0]  line_in_num;
	reg        line_in_active;
    //
	reg [7:0]  line_in_data;
	reg        line_out_clk_1;
	reg [15:0] line_out_addr_1;
	reg        line_out_clk_2;
	reg [15:0] line_out_addr_2;

	// Outputs
    // [out] Control and status signals
	wire       busy;
	wire [8:0] line_out_num;
	wire       line_out_active;
    //
	wire [15:0] line_in_addr;
	wire [7:0]  line_out_data_1;
	wire [7:0]  line_out_data_2;


    // Instantiate the input buffer (block-ram)
    reg [7:0] inp_buff [0:650];
    always @( posedge clk ) begin
        line_in_data <= inp_buff[line_in_addr];
    end
    
    
    integer idx = 0;
    initial begin
        for( idx = 0; idx < 640; idx = idx + 1 )
            inp_buff[idx] = idx;
            
    end


	// Instantiate the Unit Under Test (UUT)
	filter__invert # (
        .H_RES_PIX   (640),
        .V_RES_PIX   (480),
        .LINE_N_BITS (9),  // Bits for the line number
        .LINE_B_BITS (16)  // Bits for the line-buffer address (input and output)
    )
    uut (
		.clk             (clk), 
		.line_in_ready   (line_in_ready), 
		.line_in_num     (line_in_num), 
		.line_in_active  (line_in_active), 
		.busy            (busy), 
		.line_out_num    (line_out_num), 
		.line_out_active (line_out_active), 
		.line_in_addr    (line_in_addr), 
		.line_in_data    (line_in_data), 
		.line_out_clk_1  (line_out_clk_1), 
		.line_out_addr_1 (line_out_addr_1), 
		.line_out_data_1 (line_out_data_1), 
		.line_out_clk_2  (line_out_clk_2), 
		.line_out_addr_2 (line_out_addr_2), 
		.line_out_data_2 (line_out_data_2)
	);


    parameter PERIOD = 4;

    always begin
        clk = 1'b0;
        #(PERIOD/2) clk = 1'b1;
        #(PERIOD/2);
    end 


	initial begin
		// Initialize Inputs
		line_in_ready   = 0;
		line_in_num     = 0;
		line_in_active  = 0;
        //
		line_in_data    = 0;
        //
		line_out_clk_1  = 0;
		line_out_addr_1 = 0;
		line_out_clk_2  = 0;
		line_out_addr_2 = 0;

		// Wait 100 ns for global reset to finish
		#100;
        
		// Add stimulus here
        line_in_num    = 34;
		line_in_active = 1;
        line_in_ready  = 1;
        #(PERIOD);
        line_in_ready  = 0;
        //...
	end
      
endmodule

