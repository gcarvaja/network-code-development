# README #

This demo demonstrates the advantages of real-time capable nodes against best-effort ones, which can't display the video correctly when the traffic is increased in the network.


### Node's role ###

- NetFPGAs: Real-Time capable switches and 1 as a Best-Effort traffic injector.
- Node 1 [master]: Captures a video signal from HDMI and sends it as RT and BE traffic.
- Node 2 [slave]: Sends RT traffic to enhance the interference in BE traffic.
- Node 3 [slave]: Receives and displays RT traffic.
- Node 4 [slave]: Receives and displays BE traffic.


### Time slots ###

The time slots are summarized in the following table:

Slot |  node_1 |  node_2 |  node_3 |  node_4 
----:|:-------:|:-------:|:-------:|:-------:
  0  | Tx (RT) | ------- | Rx (RT) | -------
  1  | Tx (BE) | Tx (RT) | ------- | Rx (BE)

  
### Node's boards ###

Each node is an Atlys board acting as a communication master or slave.

The boards' slide-switches are numbered from right to left between 0 to 7.
Bellow is a summary of their functions in the different nodes:

+ Node 1:
    - switch[7]: Reset embedded processor
+ Node 2:
    - switch[7]: Reset embedded processor
+ Node 3: None
+ Node 4: None
