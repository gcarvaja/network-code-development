--	Package File Template
--
--	Purpose: This package defines supplemental types, subtypes, 
--		 constants, and functions 


library IEEE;
use IEEE.STD_LOGIC_1164.all;

package ncm_package is

	-- constants
   constant NCM_MAC               : std_logic_vector(47 downto 0) := X"123456789aff";

   -- address range selector (64 kB) (high 5 bits)
   constant ADDR_PROGROM          : std_logic_vector(6 downto 0) := "0000010";     -- X0400 .. X05FF
   constant ADDR_CFGROM           : std_logic_vector(6 downto 0) := "0000011";     -- X0600 .. X07FF
	
	-- added for queue support
	constant ADDR_CFGROM_QUEUE     : std_logic_vector(6 downto 0) := "0000100";    -- X0800 .. X09FF
	constant ADDR_VARRAM           : std_logic_vector(6 downto 0) := "0001000";     -- X1000 .. X1FFF
   
   -- the NCM codeset
   constant CONTROL_NOP           : std_logic_vector(3 downto 0) := "0000";
   constant CONTROL_CREATE        : std_logic_vector(3 downto 0) := "0001";
   constant CONTROL_SEND          : std_logic_vector(3 downto 0) := "0010";
   constant CONTROL_RCV           : std_logic_vector(3 downto 0) := "0011";
   constant CONTROL_HALT          : std_logic_vector(3 downto 0) := "0100";
   constant CONTROL_FUTURE        : std_logic_vector(3 downto 0) := "0101";
   constant CONTROL_BRANCH        : std_logic_vector(3 downto 0) := "0111";
   constant CONTROL_COUNT         : std_logic_vector(3 downto 0) := "1000";
   constant CONTROL_SIGNAL        : std_logic_vector(3 downto 0) := "1001";
   constant CONTROL_SYNC          : std_logic_vector(3 downto 0) := "1111";

   -- the NCM branch masks
   constant BRANCH_FALSE    : std_logic_vector(3 downto 0) := "0000";      -- always 0
   constant BRANCH_TRUE     : std_logic_vector(3 downto 0) := "0111";      -- always 1
   constant BRANCH_BUFEMPTY : std_logic_vector(3 downto 0) := "0001";      -- true if rcv/snd buf empty
   constant BRANCH_TESTCNT  : std_logic_vector(3 downto 0) := "0010";      -- true if cnt == val
   constant BRANCH_STATUS   : std_logic_vector(3 downto 0) := "0011";
   constant BRANCH_EQVAR    : std_logic_vector(3 downto 0) := "1000";      -- v1 == v2
   constant BRANCH_GTVAR    : std_logic_vector(3 downto 0) := "1001";      -- v1 > v2
   constant BRANCH_LTVAR    : std_logic_vector(3 downto 0) := "1010";      -- v1 < v2
   constant BRANCH_TESTVAR  : std_logic_vector(3 downto 0) := "1011";      -- test for Z, NZ, PE and PO
	
	constant BRANCH_VARQUEUEEMPTY: std_logic_vector(3 downto 0) := "0101"; 
	constant BRANCH_VARQUEUEFULL:  std_logic_vector(3 downto 0) := "0110";

   constant STATUS_SYNC_TO_POS         : integer := 15;    -- sync timeout
   constant STATUS_SHOULDSTOP_POS      : integer := 13;    -- shouldstop signal from host
   constant STATUS_RCV_FAULT_POS       : integer := 10;    -- rcv fault
   constant STATUS_SOFTMODE_POS        : integer := 8;     -- softmode is on
   constant STATUS_USERBIT7_POS        : integer := 7;     -- user bit 1
   constant STATUS_USERBIT6_POS        : integer := 6;     -- user bit 2
   constant STATUS_USERBIT5_POS        : integer := 5;     -- user bit 3
   constant STATUS_USERBIT4_POS        : integer := 4;     -- user bit 4
   constant STATUS_USERBIT3_POS        : integer := 3;     -- user bit 5
   constant STATUS_USERBIT2_POS        : integer := 2;     -- user bit 6
   constant STATUS_USERBIT1_POS        : integer := 1;     -- user bit 7
   constant STATUS_USERBIT0_POS        : integer := 0;     -- user bit 8
   
   -- obsolete
   constant STATUS_LINKFAULT_POS       : integer := 4;     -- network link fault
   constant STATUS_TRANSPORTERROR_POS  : integer := 0;     -- transport error  
   
   constant BRANCH_TEST_NZ_POS         : integer := 3;     -- all zero
   constant BRANCH_TEST_Z_POS          : integer := 2;     -- not zero
   constant BRANCH_TEST_PE_POS         : integer := 1;     -- parity even
   constant BRANCH_TEST_PO_POS         : integer := 0;     -- parity odd

   -- opb_ncm_2_00_a bit numbers
   -- for edk ppc405 - bit 0 is left !
   -- one register (slv_reg0) for all status/cmd bits
   constant NCP_START_POS              : integer := 0;     -- start NCP program
   constant NCP_STOP_POS               : integer := 1;     -- stop NCP program / force soft mode
   constant NCP_SHOULDSTOP_POS         : integer := 4;     -- shouldstop signal bit
   constant TX_LOCAL_POS               : integer := 5;     -- use loopback : tx -> rx without network (valid with tx_start)
   constant TX_START_POS               : integer := 6;     -- start tx   
   constant RX_CLR_POS                 : integer := 7;     -- confirm read
   constant NCP_ACTIVE_POS             : integer := 8;     -- NCP is running
   constant NCP_SOFTMODE_POS           : integer := 9;     -- NCP is in soft mode
   constant RX_AVAIL_POS               : integer := 10;    -- signal packet in mem
   constant TX_BUSY_POS                : integer := 11;    -- tx in progress
   constant NCP_RESET_CNTRS            : integer := 15;    -- reset counter registers 

   constant NCP_USER1_POS              : integer := 24;     -- user bit 1
   constant NCP_USER2_POS              : integer := 25;     -- user bit 2
   constant NCP_USER3_POS              : integer := 26;     -- user bit 3
   constant NCP_USER4_POS              : integer := 27;     -- user bit 4
   constant NCP_USER5_POS              : integer := 28;     -- user bit 5
   constant NCP_USER6_POS              : integer := 29;     -- user bit 6
   constant NCP_USER7_POS              : integer := 30;     -- user bit 7
   constant NCP_USER8_POS              : integer := 31;     -- user bit 8

--   -- interrupts
--   constant IRX_POS                    : integer := 0;
--   constant ITX_POS                    : integer := 1;
--   constant ISIG_POS                   : integer := 2;
--   constant INCM_POS                   : integer := 3;

	-- components
	component RS_FF is
    Port ( clk : in  STD_LOGIC;
           reset : in std_logic;
           R : in  STD_LOGIC;
           S : in  STD_LOGIC;
           Q : out  STD_LOGIC);
	end component;
   
   component parity8 is
    Port ( data : in std_logic_vector(7 downto 0);
           par_even : out std_logic);
   end component;
   
   component prescaler is
    Generic ( DIVIDER : integer range 1 to 1023 := 1000 );
    Port ( clk : in std_logic;
           rst_n : in std_logic;
           en : in std_logic;
           pulse : out std_logic);
   end component;
	
   component open_prescaler is
    Port ( clk : in std_logic;
           rst : in std_logic;
           en : in std_logic;
           pulse : out std_logic;
           divider : in std_logic_vector(10 downto 0));
   end component;
	
	component generic_ram_dual_port is
	generic (
        DATA_WIDTH : integer := 8;
        ADDR_WIDTH : integer := 8;
        TOTAL_ELEM : integer := 256;
        INIT_FILE  : string  := ""
    );
    port (
        p1_clk      : in  STD_LOGIC;
        p1_addr     : in  STD_LOGIC_VECTOR (7  downto 0);
        p1_data_out : out STD_LOGIC_VECTOR (31 downto 0);
        p2_clk      : in  STD_LOGIC;
        p2_addr     : in  STD_LOGIC_VECTOR (7  downto 0);
        p2_we       : in  STD_LOGIC;
        p2_data_in  : in  STD_LOGIC_VECTOR (31 downto 0);
        p2_data_out : out STD_LOGIC_VECTOR (31 downto 0)
    );
    end component;
	
	component config_rom_queue is
    Port ( clk_ncm : in  STD_LOGIC;
			clk_ext_bus : in  STD_LOGIC;
           -- interface to uP
           bus_address : in  STD_LOGIC_VECTOR (7 downto 0);
           bus_data_in : in  STD_LOGIC_VECTOR (31 downto 0);
           bus_data_out : out  STD_LOGIC_VECTOR (31 downto 0);
           bus_rw : in  STD_LOGIC;
           -- NCM interface
           ncm_var_num : in  STD_LOGIC_VECTOR (7 downto 0);
           ncm_cfg_queue_data_out : out  STD_LOGIC_VECTOR (31 downto 0);
			  ncm_cfg_queue_update : in  STD_LOGIC_VECTOR (31 downto 0);
			  ncm_rw : in  STD_LOGIC
			  );
	end component;
	
	component mux_cfgrom_queue is
	Port ( 	write_back_data_in_from_create: in std_logic_vector (31 downto 0);
				write_back_data_in_from_receive: in std_logic_vector (31 downto 0);
				create_busy:  in std_logic;
				receive_busy:  in std_logic;
				write_back_data_out: out std_logic_vector (31 downto 0));
end component;
	
   component cfgrom_ctrl is
    Port ( create_address : in  STD_LOGIC_VECTOR (7 downto 0);
           rcv_address : in  STD_LOGIC_VECTOR (7 downto 0);
           branch_address : in  STD_LOGIC_VECTOR (7 downto 0);
           create_busy : in  STD_LOGIC;
           rcv_busy : in  STD_LOGIC;
           branch_busy : in  STD_LOGIC;
           cfg_address : out  STD_LOGIC_VECTOR (7 downto 0));
   end component;

	component variable_ram is
    Port ( clk_ext_bus : in  STD_LOGIC;
           clk_ncm : in  STD_LOGIC;
			  -- interface to uP
           bus_address : in  STD_LOGIC_VECTOR (15 downto 0);
           bus_data_in : in  STD_LOGIC_VECTOR (31 downto 0);
           bus_data_out : out  STD_LOGIC_VECTOR (31 downto 0);
           bus_rw : in  STD_LOGIC;
           -- interface to NCM
           ncm_address : in  STD_LOGIC_VECTOR (15 downto 0);
           ncm_data_in : in  STD_LOGIC_VECTOR (31 downto 0);
           ncm_data_out : out  STD_LOGIC_VECTOR (31 downto 0);
           ncm_rw : in  STD_LOGIC);
	end component;
	
   component varram_ctrl is
    Port ( create_busy : in  STD_LOGIC;
           rcv_busy : in  STD_LOGIC;
           branch_busy : in  STD_LOGIC;
           create_address : in  STD_LOGIC_VECTOR (15 downto 0);
           rcv_address : in  STD_LOGIC_VECTOR (15 downto 0);
           branch_address : in  STD_LOGIC_VECTOR (15 downto 0);
           rcv_wen : in  STD_LOGIC;
           rcv_din : in  STD_LOGIC_VECTOR (31 downto 0);
           varram_address : out  STD_LOGIC_VECTOR (15 downto 0);
           varram_wen : out  STD_LOGIC;
           varram_din : out  STD_LOGIC_VECTOR (31 downto 0));
   end component;

component temac_wrapper is
  port (
		reset           : in std_logic;
		gmii_tx_clk       : in std_logic;
		gmii_rx_clk       : in std_logic;
		sys_clk           : in std_logic;

		gmii_txd          : out std_logic_vector(0 to 7); 
		gmii_tx_en        : out std_logic;
		gmii_tx_er        : out std_logic;
		gmii_rxd          : in std_logic_vector (0 to 7);
		gmii_rx_dv        : in std_logic;
		gmii_rx_er        : in std_logic;

		tx_fifo_data      : in std_logic_vector (35 downto 0);
		tx_fifo_wren      : in std_logic;
		
		rx_fifo_data      : out std_logic_vector (35 downto 0);
		autorec_start		: out std_logic;
		rx_fifo_rden      : in std_logic;

		run_NCM				: in std_logic;
		control_reg_hit	: in std_logic;
		
		rx_fifo_empty		: out std_logic;
		rx_fifo_data_valid: out std_logic;
				NCSync_type					: in std_logic_vector (15 downto 0);
				NCData_type					: in std_logic_vector (15 downto 0);		
				NCAck_type					: in std_logic_vector (15 downto 0);	
		start_send_ack		: out std_logic;
		receiving_sync		: out std_logic;
		reset_rx_fifo					: in std_logic;
		
		  rx_goodframes_RT_data_cnt: out std_logic_vector(31 downto 0);
		  rx_goodframes_RT_sync_cnt: out std_logic_vector(31 downto 0);
		  rx_badframes_RT_data_cnt: out std_logic_vector(31 downto 0);
		  rx_badframes_RT_sync_cnt: out std_logic_vector(31 downto 0);
		  rx_bytes_RT_cnt: out std_logic_vector(63 downto 0);
		  rx_goodframes_non_RT_cnt: out std_logic_vector(31 downto 0);
		  rx_badframes_non_RT_cnt: out std_logic_vector(31 downto 0);
		  rx_bytes_non_RT_cnt: out std_logic_vector(63 downto 0);
		  running_time_tx_cnt:  out std_logic_vector(63 downto 0);
		  total_tx_bytes_cnt : out std_logic_vector(63 downto 0);
		  running_time_rx_cnt:  out std_logic_vector(63 downto 0);
		  total_rx_bytes_cnt : out std_logic_vector(63 downto 0);
		  tx_fifo_overflow_cnt: out std_logic_vector(31 downto 0);
        rx_good_frames_error_cnt : out  STD_LOGIC_VECTOR (31 downto 0);
		  rx_total_RT_data_frames_cnt : out  STD_LOGIC_VECTOR (31 downto 0);
		  rx_total_RT_sync_frames_cnt : out  STD_LOGIC_VECTOR (31 downto 0);
		  rx_total_RT_ack_frames_cnt : out  STD_LOGIC_VECTOR (31 downto 0);
		  rx_total_nonRT_frames_cnt : out  STD_LOGIC_VECTOR (31 downto 0);
		  rx_goodframes_total_cnt : out  STD_LOGIC_VECTOR (31 downto 0);
		  rx_badframes_total_cnt : out  STD_LOGIC_VECTOR (31 downto 0);
		  bytes_read_from_txfifo_cnt: out std_logic_vector (31 downto 0)
		
		
    );
end component;

	component create_cmd is
    Port ( clk : in  STD_LOGIC;
           -- cmd interface
           start : in  STD_LOGIC;
           rdy : out  STD_LOGIC;
           varnum : in  STD_LOGIC_VECTOR (7 downto 0);
           datalen : out std_logic_vector(15 downto 0);        -- register of the last data length 
			  
			  varnum_to_send: out STD_LOGIC_VECTOR (7 downto 0);
           -- read config setup
           cfgaddress : out  STD_LOGIC_VECTOR (7 downto 0);
           cfgdata : in  STD_LOGIC_VECTOR (31 downto 0);
			  cfgdata_queue : in  STD_LOGIC_VECTOR (31 downto 0);
			  
			  cfgdata_queue_write_back: out  STD_LOGIC_VECTOR (31 downto 0);
			  cfgdata_queue_wen: out std_logic;
           -- copy variable
           varaddress : out  STD_LOGIC_VECTOR (15 downto 0);
           vardata : in  STD_LOGIC_VECTOR (31 downto 0);
           fifo_rst : out std_logic;
           fifo_en : out  STD_LOGIC;
           fifo_data : out  STD_LOGIC_VECTOR (31 downto 0);
			  created_frames_cnt: out  STD_LOGIC_VECTOR (31 downto 0)
			  );
	end component;
 
	component send_fifo is
    Port ( clk : in  STD_LOGIC;
           w_en : in  STD_LOGIC;
           r_en : in std_logic;
           reset : in std_logic;
           data_in : in  STD_LOGIC_VECTOR (31 downto 0);
           empty : out  STD_LOGIC;
           full : out  STD_LOGIC;
           data_out : out  STD_LOGIC_VECTOR (31 downto 0));
	end component;
 
	component send_cmd is
    Port ( clk 						: in  STD_LOGIC;
           start 						: in  STD_LOGIC;
           rdy 						: out  STD_LOGIC;

           datalen 					: in  STD_LOGIC_VECTOR (15 downto 0);
           channel 					: in  STD_LOGIC_VECTOR (7 downto 0);
           macaddress 				: in std_logic_vector(47 downto 0);
			  BE_traffic_mode			: in std_logic;
			  BE_type					: in  STD_LOGIC_VECTOR (15 downto 0);
			  NCData_type				: in  STD_LOGIC_VECTOR (15 downto 0);
			  NCSync_type				: in  STD_LOGIC_VECTOR (15 downto 0);
			  NCAck_type				: in  STD_LOGIC_VECTOR (15 downto 0);
			  
           sync 						: in  STD_LOGIC;
			  receiving_sync			: in  std_logic;
			  
			  varnum						: in STD_LOGIC_VECTOR (7 downto 0); -- variable id from last create call
			  TTL							: in STD_LOGIC_VECTOR (7 downto 0);

           fifo_data 				: in  STD_LOGIC_VECTOR (31 downto 0);
           fifo_empty 				: in  STD_LOGIC;
           fifo_en 					: out  STD_LOGIC;

           tx_en 						: out  STD_LOGIC;
           tx_data 					: out  STD_LOGIC_VECTOR (35 downto 0);	-- 32 is frame flag, 31..0 is data
           tx_full 					: in  STD_LOGIC;
			  
           master_sync_sent_cnt 	: out std_logic_vector (31 downto 0);
           ack_sync_sent_cnt 		: out std_logic_vector (31 downto 0);
           data_frames_sent_cnt 	: out std_logic_vector (31 downto 0)			  
			  );
	end component;
		
	component auto_receiver is
    Port ( clk 						: in  STD_LOGIC;
           rst 						: in  STD_LOGIC;
           busy 						: out std_logic;               
           start 						: in  std_logic;
			  
			  reset_rx_fifo					: out std_logic;

           NCSync_type 			: in  STD_LOGIC_VECTOR (15 downto 0);
           NCData_type 			: in  STD_LOGIC_VECTOR (15 downto 0);		
			  
			  -- emac interface
           rx_fifo_data 			: in  STD_LOGIC_VECTOR (35 downto 0);
           rx_fifo_en 				: out STD_LOGIC;
           rx_valid 					: in  STD_LOGIC;            
           rx_fifo_empty			: in  std_logic;
			  -- ncm interface to rcv_fifo
           ncm_sync 					: out STD_LOGIC;
           ncm_rcv_channel 		: out STD_LOGIC_VECTOR (3 downto 0);
           ncm_rcv_en 				: out STD_LOGIC;
           ncm_rcv_data 			: out STD_LOGIC_VECTOR (31 downto 0);
           ncm_rcv_reset 			: out STD_LOGIC;
           -- statistics interface
           goodcnt_sync 			: out std_logic_vector(31 downto 0);
			  goodcnt_data 			: out std_logic_vector(31 downto 0);
			  badcnt_data				: out std_logic_vector(31 downto 0);
           nobroadcst 				: out std_logic_vector(31 downto 0);
           badcnt 					: out std_logic_vector(31 downto 0);
			  debugstate				: out std_logic_vector(3 downto 0)		  
           );
	end component;
 
   component rcv_fifo is
    Port ( rdclk : in  STD_LOGIC;
           wrclk : in  STD_LOGIC;
           -- from auto receiver
           w_en : in  STD_LOGIC;
           w_channel : in  STD_LOGIC_VECTOR (3 downto 0);
           data_in : in  STD_LOGIC_VECTOR (31 downto 0);
           reset : in  STD_LOGIC;
           empty_for_w : out  STD_LOGIC;
           full_for_w : out  STD_LOGIC;
           -- to receive_cmd
           empty_for_r : out  STD_LOGIC;
           full_for_r : out  STD_LOGIC;
           r_en : in  STD_LOGIC;
           r_channel : in  STD_LOGIC_VECTOR (3 downto 0);
           data_out : out  STD_LOGIC_VECTOR (31 downto 0);
           -- status output
			  rd_count: out std_logic_vector(11 downto 0);
			  wr_count: out std_logic_vector(11 downto 0);
			  
           wr_ack : out  STD_LOGIC;
			  overflow: out  STD_LOGIC;
			  valid: out  STD_LOGIC;
			  underflow: out  STD_LOGIC;
			  empty: out std_logic_vector (15 downto 0)
           );
   end component;

    component receive_cmd is
    Port(
        clk                      : in  STD_LOGIC;
        -- cmd interface
        start                    : in  STD_LOGIC;
        rdy                      : out STD_LOGIC;
        fault                    : out STD_LOGIC;
        fault_empty              : out STD_LOGIC;
        varnum                   : in  STD_LOGIC_VECTOR (7 downto 0);
        channel                  : in  STD_LOGIC_VECTOR (7 downto 0);
        offset                   : in  STD_LOGIC_VECTOR (7 downto 0);
        -- read config setup
        cfgaddress               : out STD_LOGIC_VECTOR (7 downto 0);
        cfgdata                  : in  STD_LOGIC_VECTOR (31 downto 0); 
        cfgdata_queue            : in  STD_LOGIC_VECTOR (31 downto 0);
        cfgdata_queue_write_back : out STD_LOGIC_VECTOR (31 downto 0);
        cfgdata_queue_wen        : out STD_LOGIC;      
        -- copy variable
        varaddress               : out STD_LOGIC_VECTOR (15 downto 0);
        vardata                  : out STD_LOGIC_VECTOR (31 downto 0);
        var_wen                  : out STD_LOGIC;
        -- from fifo
        fifo_en                  : out STD_LOGIC;
        fifo_empty               : in  STD_LOGIC;
        fifo_reset               : out STD_LOGIC;
        fifo_channel             : out STD_LOGIC_VECTOR (3 downto 0);
        fifo_data                : in  STD_LOGIC_VECTOR (31 downto 0);
        fifo_wr_ack              : in  STD_LOGIC;
        --
        rcv_cmd_fault_cnt        : out STD_LOGIC_VECTOR (31 downto 0);
        rcv_cmd_ok_cnt           : out STD_LOGIC_VECTOR (31 downto 0)
    );
    end component;

   component countdown is
    Port ( clk : in  STD_LOGIC;
           reset : in  STD_LOGIC;
           tick : in  STD_LOGIC;
           set : in  STD_LOGIC;
           t_in : in  STD_LOGIC_VECTOR (15 downto 0);
           active_ct : out std_logic;
           timeover : out  STD_LOGIC;
           --allzero : out std_logic;
           remaining : out  STD_LOGIC_VECTOR (15 downto 0));
   end component;

   component future_cmd is
    Port ( clk : in  STD_LOGIC;
           reset : in  STD_LOGIC;
           pulse : in  STD_LOGIC;
           set : in  STD_LOGIC;
           timetowait : in  STD_LOGIC_VECTOR (15 downto 0);
           address : in  STD_LOGIC_VECTOR (7 downto 0);
           rdy : out  STD_LOGIC;
           future_int : out  STD_LOGIC;
           jumpto : out  STD_LOGIC_VECTOR (7 downto 0));
   end component;

	component branchcmd is
    Port ( clk : in  STD_LOGIC;
           rst : in  STD_LOGIC;
           -- controller if
           start : in  STD_LOGIC;
           rdy : out  STD_LOGIC;
           result : out  STD_LOGIC;
           guard : in  STD_LOGIC_VECTOR (19 downto 0);
           -- states
           rcvbufempty : in  STD_LOGIC_VECTOR (15 downto 0);
           sndbufempty : in  STD_LOGIC;
           counter : in  STD_LOGIC_VECTOR (15 downto 0);
           status : in STD_LOGIC_VECTOR (15 downto 0);
           stopack : out std_logic;
           -- cfgrom
           cfgaddress : out  STD_LOGIC_VECTOR (7 downto 0);
           cfgdata : in  STD_LOGIC_VECTOR (31 downto 0);
			  
			  cfgdata_queue : in  STD_LOGIC_VECTOR (31 downto 0);
			  
           -- varram
           varaddress : out  STD_LOGIC_VECTOR (15 downto 0);
           vardata : in  STD_LOGIC_VECTOR (31 downto 0)
	   );
	end component;

   component NCM_fetch is
    Port ( clk : in  STD_LOGIC;
           rst : in  STD_LOGIC;
           fetchnext : in  STD_LOGIC;
           dobranch : in  STD_LOGIC;
           memdata : in  STD_LOGIC_VECTOR (31 downto 0);
           halt_wakeup : in  STD_LOGIC;
           jumpaddr : in  STD_LOGIC_VECTOR (7 downto 0);
           memaddr : out  STD_LOGIC_VECTOR (7 downto 0);
           cmd : out  STD_LOGIC_VECTOR (31 downto 0);
           cmdok : out  STD_LOGIC;
           branchaddr : in  STD_LOGIC_VECTOR (7 downto 0));
   end component;

   component NCM_counter is
    Port ( clk : in  STD_LOGIC;
           rst : in  STD_LOGIC;
           inc : in  STD_LOGIC;
           dec : in  STD_LOGIC;
           value : out  STD_LOGIC_VECTOR (15 downto 0));
   end component;

   component NCM_counter2 is
    Port ( clk : in  STD_LOGIC;
           en  : in std_logic;
           rst : in  STD_LOGIC;
           add : in  STD_LOGIC;
           set : in  STD_LOGIC;
           value_in : in std_logic_vector(15 downto 0);
           value_out : out  STD_LOGIC_VECTOR (15 downto 0));
   end component;


   component NCM_controller is
    Port ( clk 							: in  STD_LOGIC;
           rst 							: in  STD_LOGIC;
           run 							: in std_logic;         -- switch on/off controller
           controller_clock 			: in std_logic;     -- 10 us pulses
           softmode 						: out std_logic;    -- switch on/off soft mode
           controlstate 				: out std_logic_vector(4 downto 0);    -- for debug
           app_sync_pulse               : out std_logic;

           createcmd_busy 				: in std_logic;
           sendcmd_busy 				: in std_logic;
           rcvcmd_busy 					: in std_logic;
           branch_busy 					: in std_logic;
           
           start_create 				: out std_logic;
           createcmd_varnum 			: out std_logic_vector(7 downto 0);
           createcmd_dlen 				: in std_logic_vector(15 downto 0);
           createcmd_rdy 				: in std_logic;                       -- capture len
           
           sendfifo_empty 				: in std_logic;
           start_send 					: out std_logic;
           sendcmd_sync 				: out std_logic;
           sendcmd_channel 			: out std_logic_vector(7 downto 0);
           sendcmd_dlen 				: out std_logic_vector(15 downto 0);
			  TTL		 						: out STD_LOGIC_VECTOR (7 downto 0);
           sendcmd_BE_mode				: out std_logic;

           sync_received 				: in std_logic;                       -- one clock sync pulse
           sync_timeout 				: out std_logic;
           sync_timeout_pulse 		: out std_logic;                 -- one clock pulse
			  sync_timeout_cnt 			: out std_logic_vector(31 downto 0);
			  expected_sync_ok_cnt 		: out std_logic_vector(31 downto 0);  
			  total_sync_cnt 				: out std_logic_vector(31 downto 0);
			  master_sync_cnt				: out std_logic_vector(31 downto 0);
			  slave_sync_cnt				: out std_logic_vector(31 downto 0);
           
           rcv_start 					: out std_logic;
           rcv_varnum 					: out std_logic_vector(7 downto 0);
           rcv_channel 					: out std_logic_vector(7 downto 0);
           rcv_offset 					: out std_logic_vector(7 downto 0);
           rcv_fault 					: in std_logic;
           rcv_rdy 						: in std_logic;

           future_reset 				: out std_logic;
           future_set 					: out std_logic;
           future_timetowait 			: out std_logic_vector(15 downto 0);
           future_address 				: out std_logic_vector(7 downto 0);
           future_ack 					: in std_logic;
           future_int 					: in std_logic;
           future_vector 				: in std_logic_vector(7 downto 0);

           branch_start 				: out std_logic;
           branch_guard 				: out std_logic_vector(19 downto 0);
           branch_count 				: out std_logic_vector(15 downto 0);
           branch_result 				: in std_logic;
           branch_rdy 					: in std_logic;        -- get result

           mode_switch 					: out std_logic;
           mode_delay 					: out std_logic_vector(15 downto 0);

           ncm_data 						: in  STD_LOGIC_VECTOR (31 downto 0);
           ncm_addr 						: out  STD_LOGIC_VECTOR (7 downto 0)
           );
   end component;

   component bram_decoder is
    Port ( clk : in std_logic;
    
           bram_addr : in  STD_LOGIC_VECTOR (15 downto 0);
           din : in  STD_LOGIC_VECTOR (31 downto 0);
           dout : out  STD_LOGIC_VECTOR (31 downto 0);
           wen : in  STD_LOGIC;
           
           progrom_addr : out  STD_LOGIC_VECTOR (15 downto 0);
           progrom_din : out  STD_LOGIC_VECTOR (31 downto 0);
           progrom_dout : in  STD_LOGIC_VECTOR (31 downto 0);
           progrom_wen : out  STD_LOGIC;

           cfgrom_addr : out  STD_LOGIC_VECTOR (15 downto 0);
           cfgrom_din : out  STD_LOGIC_VECTOR (31 downto 0);
           cfgrom_dout : in  STD_LOGIC_VECTOR (31 downto 0);
           cfgrom_wen : out  STD_LOGIC;

           cfgrom_queue_addr : out  STD_LOGIC_VECTOR (15 downto 0);
           cfgrom_queue_din : out  STD_LOGIC_VECTOR (31 downto 0);
           cfgrom_queue_dout : in  STD_LOGIC_VECTOR (31 downto 0);
           cfgrom_queue_wen : out  STD_LOGIC;

           varram_addr : out  STD_LOGIC_VECTOR (15 downto 0);
           varram_din : out  STD_LOGIC_VECTOR (31 downto 0);
           varram_dout : in  STD_LOGIC_VECTOR (31 downto 0);
           varram_wen : out  STD_LOGIC
           );
   end component;

   component keydetect is
    Generic ( SLOTS : integer range 5 to 100 := 9 );
    Port ( clk 		: in  STD_LOGIC;
           key 		: in  STD_LOGIC;
           pulse 		: out  STD_LOGIC);
   end component;

   component varram_counter is
    Port ( clk 		: in  STD_LOGIC;
           rst_n 		: in  STD_LOGIC;
           tick 		: in  STD_LOGIC;
           address 	: out  STD_LOGIC_VECTOR (15 downto 0);
           data 		: out  STD_LOGIC_VECTOR (31 downto 0);
           rw 			: out  STD_LOGIC);
   end component;

-- master interface component ---------------------------------------------------------------------------

component ncm_frame is
    generic(
        PROG_MEM_INIT_FILE : string := "";
        CONF_MEM_INIT_FILE : string := ""
    );
    Port(
        clk_ncm                     : in  STD_LOGIC;
        clk_app                     : in  STD_LOGIC;
        clk_ext_bus                 : in  STD_LOGIC;
        sys_rst                     : in  STD_LOGIC;
        control_reg_hit             : in  STD_LOGIC;
        pclock_divider              : in  STD_LOGIC_VECTOR (10 downto 0);
        app_sync_pulse              : out STD_LOGIC;
        -- memory interface to cpu
        varram_app_wen              : in  STD_LOGIC;
        varram_app_addr             : in  STD_LOGIC_VECTOR (15 downto 0);
        varram_app_din              : in  STD_LOGIC_VECTOR (31 downto 0);
        varram_app_dout             : out STD_LOGIC_VECTOR (31 downto 0);
        --
        progrom_bus_address         : in  STD_LOGIC_VECTOR (7 downto 0);
        progrom_bus_din             : in  STD_LOGIC_VECTOR (31 downto 0);
        progrom_bus_dout            : out STD_LOGIC_VECTOR (31 downto 0);
        progrom_bus_wen             : in  STD_LOGIC ;
        --
        cfgrom_bus_address          : in  STD_LOGIC_VECTOR (7 downto 0);
        cfgrom_bus_din              : in  STD_LOGIC_VECTOR (31 downto 0);
        cfgrom_bus_dout             : out STD_LOGIC_VECTOR (31 downto 0);
        cfgrom_bus_wen              : in  STD_LOGIC;
        --
        cfgrom_queue_bus_address    : in  STD_LOGIC_VECTOR (7 downto 0);
        cfgrom_queue_bus_din        : in  STD_LOGIC_VECTOR (31 downto 0);
        cfgrom_queue_bus_dout       : out STD_LOGIC_VECTOR (31 downto 0);
        cfgrom_queue_bus_wen        : in  STD_LOGIC;
        --
        counters_bus_address        : in  STD_LOGIC_VECTOR (7 downto 0);
        counters_bus_din            : in  STD_LOGIC_VECTOR (31 downto 0);
        counters_bus_dout           : out STD_LOGIC_VECTOR (31 downto 0);
        counters_bus_wen            : in  STD_LOGIC;
        -- GMII MAC
        gmii_rx_clk                 : in  STD_LOGIC;
        gmii_rx_dv                  : in  STD_LOGIC;
        gmii_rx_er                  : in  STD_LOGIC;
        gmii_rxd                    : in  STD_LOGIC_VECTOR (7 downto 0);
        gmii_tx_clk                 : in  STD_LOGIC;
        gmii_tx_en                  : out STD_LOGIC;
        gmii_tx_er                  : out STD_LOGIC;
        gmii_txd                    : out STD_LOGIC_VECTOR (7 downto 0);
        -- MAC address
        mac_high                    : in  STD_LOGIC_VECTOR (23 downto 0);
        mac_low                     : in  STD_LOGIC_VECTOR (23 downto 0);
        --
        BE_type                     : in  STD_LOGIC_VECTOR (15 downto 0);
        NCData_type                 : in  STD_LOGIC_VECTOR (15 downto 0);
        NCSync_type                 : in  STD_LOGIC_VECTOR (15 downto 0);
        NCAck_type                  : in  STD_LOGIC_VECTOR (15 downto 0);
        sync_ack_en                 : in  STD_LOGIC;
        -- status lines
        ncm_shouldstopeval          : out STD_LOGIC;
        -- command bits
        ncm_user                    : in  STD_LOGIC_VECTOR (7 downto 0);
        ncm_shouldstop              : in  STD_LOGIC;
        -- switch NCM on/off
        run_NCM                     : in  STD_LOGIC;
        -- Receive command status
        receive_fault_empty         : out STD_LOGIC
    );  -- data out
end component;


	component tx_queue is
        port( 
		     reset 								: in std_logic;
			  run_NCM							: in std_logic;
		  
		     tx_clk 							: in  STD_LOGIC;
			  clk_ncm							: in std_logic;
			  data_from_ncm_to_tx_fifo		: in std_logic_vector (35 downto 0);
			  tx_fifo_wren						: in std_logic;
			  data_from_tx_fifo_to_mac		: out std_logic_vector (0 to 7);
			  tx_data_valid_to_mac			: out std_logic;
			  tx_ack_from_mac_to_fifo		: in std_logic;			  
			  tx_fifo_overflow_cnt 			: out std_logic_vector (31 downto 0);
			  tx_bytes_read_cnt				: out std_logic_vector (31 downto 0)
			  );
			  
	end component;


	component tx_mac_fifo
	port (
		din									: IN std_logic_VECTOR(35 downto 0);
		rd_clk								: IN std_logic;
		rd_en									: IN std_logic;
		rst									: IN std_logic;
		wr_clk								: IN std_logic;
		wr_en									: IN std_logic;
		dout									: OUT std_logic_VECTOR(8 downto 0);
		empty									: OUT std_logic;
		full									: OUT std_logic);
	end component; 
 
	component rx_queue Port (
				reset								: in std_logic;
				clk_ncm							: in std_logic;							
           
				reset_rx_fifo					: in std_logic;	
			  
				rxcoreclk 						: in std_logic;					
				gmac_rx_data_temp      		: in std_logic_vector (7 downto 0); 		
				gmac_rx_dvld_temp 			: in std_logic;		
				gmac_rx_goodframe_temp 		: in std_logic;		
				gmac_rx_badframe_temp 		: in std_logic;
				rx_fifo_rden					: in std_logic;	
				start_send_ack					: out std_logic;		
				receiving_sync					: out std_logic;		
				data_from_rx_fifo_to_autorec : out std_logic_vector (35 downto 0);
				autorec_start					: out std_logic;			
				rx_fifo_empty              : out std_logic;
				rx_fifo_data_valid         : out std_logic;
				rx_rden_from_autorec 		: in std_logic;
				rx_fifo_empty_RT 				: out std_logic;
			  
			  				NCSync_type					: in std_logic_vector (15 downto 0);
				NCData_type					: in std_logic_vector (15 downto 0);
				NCAck_type					: in std_logic_vector (15 downto 0);
				
			  rx_goodframes_RT_data_cnt	: out std_logic_vector (31 downto 0);
			  rx_goodframes_RT_sync_cnt 	: out std_logic_vector (31 downto 0);
			  rx_badframes_RT_data_cnt		: out std_logic_vector (31 downto 0);
			  rx_badframes_RT_sync_cnt		: out std_logic_vector (31 downto 0);
			  rx_bytes_RT_cnt    			: out std_logic_vector (63 downto 0);
			  rx_goodframes_non_RT_cnt   	: out std_logic_vector (31 downto 0);
			  rx_badframes_non_RT_cnt   	: out std_logic_vector (31 downto 0);
			  rx_bytes_non_RT_cnt   		: out std_logic_vector (63 downto 0);
			  rx_good_frames_error_cnt 	: out std_logic_vector (31 downto 0);
			  rx_total_RT_data_frames_cnt	: out std_logic_vector (31 downto 0);
			  rx_total_RT_sync_frames_cnt	: out std_logic_vector (31 downto 0);
			  rx_total_nonRT_frames_cnt	: out std_logic_vector (31 downto 0);
			  rx_goodframes_total_cnt 		: out std_logic_vector (31 downto 0);
			  rx_badframes_total_cnt 		: out std_logic_vector (31 downto 0);
			  rx_total_RT_ack_frames_cnt	: out std_logic_vector (31 downto 0)
	  ); 
 
	end component;

 
	component pulse_synchronizer is
	Port (
      pulse_in_clkA	: in std_logic;
		clkA				: in std_logic;
		pulse_out_clkB : out std_logic;
		clkB				: in std_logic;
		reset_clkA		: in std_logic;
		reset_clkB		: in std_logic
		);
	end component; 
 
	component blk_mem_gen_v2_8 IS
	port (
		clka					: IN std_logic;
		addra					: IN std_logic_VECTOR(13 downto 0);
		douta					: OUT std_logic_VECTOR(31 downto 0));
	END component;
 
 
component gig_eth_mac is
port(
  reset 						: in std_logic;
  tx_clk						: in std_logic;
  rx_clk						: in std_logic;

  conf_tx_en				: in std_logic;
  conf_rx_en				: in std_logic;
  conf_tx_no_gen_crc		: in std_logic;
  conf_rx_no_chk_crc		: in std_logic;
  conf_tx_jumbo_en		: in std_logic;
  conf_rx_jumbo_en		: in std_logic;

  mac_tx_data	 			: in std_logic_vector(7 downto 0);
  mac_tx_dvld 				: in std_logic;
  mac_tx_ack				: out std_logic;
  mac_tx_underrun			: in std_logic;

  mac_rx_data				: out std_logic_vector(7 downto 0);  
  mac_rx_dvld 				: out std_logic;
  mac_rx_goodframe 		: out std_logic;
  mac_rx_badframe 		: out std_logic;

  gmii_txd					: out std_logic_vector(7 downto 0); 
  gmii_txen 				: out std_logic; 
  gmii_txer 				: out std_logic;

  gmii_rxd 					: in std_logic_vector(7 downto 0); 
  gmii_rxdv 				: in std_logic;
  gmii_rxer 				: in std_logic
); 
end component;

component clk_125_tx is
port
 (-- Clock in ports
  CLK_IN1           : in     std_logic;
  -- Clock out ports
  CLK_OUT1          : out    std_logic;
  CLK_OUT2          : out    std_logic;
  CLK_OUT3          : out    std_logic;
  CLK_OUT4          : out    std_logic;
  -- Status and control signals
  RESET             : in     std_logic;
  LOCKED            : out    std_logic
 );
end component; 

COMPONENT shift_ram
  PORT (
    d : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    clk : IN STD_LOGIC;
    q : OUT STD_LOGIC_VECTOR(15 DOWNTO 0)
  );
END COMPONENT;

COMPONENT fifo
  PORT (
    rst : IN STD_LOGIC;
    wr_clk : IN STD_LOGIC;
    rd_clk : IN STD_LOGIC;
    din : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    wr_en : IN STD_LOGIC;
    rd_en : IN STD_LOGIC;
    dout : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    full : OUT STD_LOGIC;
    wr_ack : OUT STD_LOGIC;
    overflow : OUT STD_LOGIC;
    empty : OUT STD_LOGIC;
    valid : OUT STD_LOGIC;
    underflow : OUT STD_LOGIC;
    rd_data_count : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
    wr_data_count : OUT STD_LOGIC_VECTOR(11 DOWNTO 0)
  );
END COMPONENT;

component phy_mac_interface is
    port(
        -- Synchronous resets
        tx_reset                      : in  std_logic;
        rx_reset                      : in  std_logic;

        -- The following ports are the GMII physical interface: these will be at
        -- pins on the FPGA
        gmii_txd                      : out std_logic_vector(7 downto 0);
        gmii_tx_en                    : out std_logic;
        gmii_tx_er                    : out std_logic;
        gmii_tx_clk                   : out std_logic;
        gmii_rxd                      : in  std_logic_vector(7 downto 0);
        gmii_rx_dv                    : in  std_logic;
        gmii_rx_er                    : in  std_logic;
        gmii_rx_clk                   : in  std_logic;

        -- The following ports are the internal GMII connections from IOB logic
        -- to the TEMAC core
        txd_from_mac                  : in  std_logic_vector(7 downto 0);
        tx_en_from_mac                : in  std_logic;
        tx_er_from_mac                : in  std_logic;
        tx_clk                        : in  std_logic;
        rxd_to_mac                    : out std_logic_vector(7 downto 0);
        rx_dv_to_mac                  : out std_logic;
        rx_er_to_mac                  : out std_logic;

        -- Receiver clock for the MAC and Client Logic
        rx_clk                        : out  std_logic
        );
end component;

component shift_ram_v9_0 IS
	port (
	d: IN std_logic_VECTOR(15 downto 0);
	clk: IN std_logic;
	ce: IN std_logic;
	q: OUT std_logic_VECTOR(15 downto 0));
END component;

COMPONENT rcv_fifo_common
  PORT (
    clk : IN STD_LOGIC;
    rst : IN STD_LOGIC;
    din : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    wr_en : IN STD_LOGIC;
    rd_en : IN STD_LOGIC;
    dout : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    full : OUT STD_LOGIC;
    almost_full : OUT STD_LOGIC;
    wr_ack : OUT STD_LOGIC;
    overflow : OUT STD_LOGIC;
    empty : OUT STD_LOGIC;
    almost_empty : OUT STD_LOGIC;
    valid : OUT STD_LOGIC;
    underflow : OUT STD_LOGIC;
    data_count : OUT STD_LOGIC_VECTOR(10 DOWNTO 0)
  );
END COMPONENT;

component counters is
    Port(
        -- User bus interface
        clk_bus                     : in  STD_LOGIC;
        bus_address                 : in  STD_LOGIC_VECTOR (7 downto 0);
        bus_data_in                 : in  STD_LOGIC_VECTOR (31 downto 0);
        bus_data_out                : out STD_LOGIC_VECTOR (31 downto 0);
        bus_rw                      : in  STD_LOGIC;
        -- NCM interface
        clk_ncm                     : in  STD_LOGIC;
        ncm_rw                      : in  STD_LOGIC;
        -- Counters (using clk_ncm)
        created_frames_cnt          : in  STD_LOGIC_VECTOR (31 downto 0);
        data_frames_sent_cnt        : in  STD_LOGIC_VECTOR (31 downto 0);
        rcv_cmd_ok_cnt              : in  STD_LOGIC_VECTOR (31 downto 0);
        rcv_cmd_fault_cnt           : in  STD_LOGIC_VECTOR (31 downto 0);
        ack_sync_sent_cnt           : in  STD_LOGIC_VECTOR (31 downto 0);
        bytes_read_from_txfifo_cnt  : in  STD_LOGIC_VECTOR (31 downto 0);
        --
        tx_fifo_overflow_cnt        : in  STD_LOGIC_VECTOR (31 downto 0);
        total_tx_bytes_cnt          : in  STD_LOGIC_VECTOR (31 downto 0);
        --
        rx_RT_bytes_cnt             : in  STD_LOGIC_VECTOR (31 downto 0);
        rx_bytes_non_RT_cnt         : in  STD_LOGIC_VECTOR (31 downto 0);
        total_rx_bytes_cnt          : in  STD_LOGIC_VECTOR (31 downto 0);
        --
        rx_good_frames_error_cnt    : in  STD_LOGIC_VECTOR (31 downto 0);
        rx_goodframes_non_RT_cnt    : in  STD_LOGIC_VECTOR (31 downto 0);
        rx_badframes_non_RT_cnt     : in  STD_LOGIC_VECTOR (31 downto 0);
        rx_goodframes_total_cnt     : in  STD_LOGIC_VECTOR (31 downto 0);
        rx_badframes_total_cnt      : in  STD_LOGIC_VECTOR (31 downto 0);
        running_time_tx_cnt         : in  STD_LOGIC_VECTOR (31 downto 0);
        running_time_rx_cnt         : in  STD_LOGIC_VECTOR (31 downto 0);
        --
        master_sync_sent_cnt        : in  STD_LOGIC_VECTOR (31 downto 0);
        expected_sync_ok_cnt        : in  STD_LOGIC_VECTOR (31 downto 0);
        sync_timeout_cnt            : in  STD_LOGIC_VECTOR (31 downto 0);
        total_sync_cnt              : in  STD_LOGIC_VECTOR (31 downto 0);
        master_sync_cnt             : in  STD_LOGIC_VECTOR (31 downto 0);
        slave_sync_cnt              : in  STD_LOGIC_VECTOR (31 downto 0);
        --
        autorec_goodcnt_data        : in  STD_LOGIC_VECTOR (31 downto 0);
        autorec_badcnt_data         : in  STD_LOGIC_VECTOR (31 downto 0);
        autorec_goodcnt_sync        : in  STD_LOGIC_VECTOR (31 downto 0);
        --
        rx_total_RT_data_frames_cnt : in  STD_LOGIC_VECTOR (31 downto 0);
        rx_total_RT_sync_frames_cnt : in  STD_LOGIC_VECTOR (31 downto 0);
        rx_total_RT_ack_frames_cnt  : in  STD_LOGIC_VECTOR (31 downto 0);
        rx_total_nonRT_frames_cnt   : in  STD_LOGIC_VECTOR (31 downto 0);
        --
        rx_goodframes_RT_data_cnt   : in  STD_LOGIC_VECTOR (31 downto 0);
        rx_goodframes_RT_sync_cnt   : in  STD_LOGIC_VECTOR (31 downto 0);
        rx_badframes_RT_data_cnt    : in  STD_LOGIC_VECTOR (31 downto 0);
        rx_badframes_RT_sync_cnt    : in  STD_LOGIC_VECTOR (31 downto 0)
    );
end component;

component tx_fifo IS
  PORT (
    rst : IN STD_LOGIC;
    wr_clk : IN STD_LOGIC;
    rd_clk : IN STD_LOGIC;
    din : IN STD_LOGIC_VECTOR(35 DOWNTO 0);
    wr_en : IN STD_LOGIC;
    rd_en : IN STD_LOGIC;
    dout : OUT STD_LOGIC_VECTOR(8 DOWNTO 0);
    full : OUT STD_LOGIC;
    empty : OUT STD_LOGIC
  );
END component;

end ncm_package;

package body ncm_package is

end ncm_package;