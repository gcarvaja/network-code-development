library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

use work.ncm_package.all;

entity send_cmd is
    Port(
        clk                  : in  STD_LOGIC;
        start                : in  STD_LOGIC;
        rdy                  : out STD_LOGIC;
        -- header info
        datalen              : in  STD_LOGIC_VECTOR (15 downto 0);
        channel              : in  STD_LOGIC_VECTOR (7 downto 0);
        macaddress           : in  STD_LOGIC_VECTOR (47 downto 0);
        BE_traffic_mode      : in  STD_LOGIC;
        BE_type              : in  STD_LOGIC_VECTOR (15 downto 0);
        NCData_type          : in  STD_LOGIC_VECTOR (15 downto 0);
        NCSync_type          : in  STD_LOGIC_VECTOR (15 downto 0);
        NCAck_type           : in  STD_LOGIC_VECTOR (15 downto 0);
        -- send sync packet
        sync                 : in  STD_LOGIC;
        receiving_sync       : in  STD_LOGIC; 
        varnum               : in  STD_LOGIC_VECTOR (7 downto 0); -- variable id from last create call
        TTL                  : in  STD_LOGIC_VECTOR (7 downto 0);
        -- PDU data interface
        fifo_data            : in  STD_LOGIC_VECTOR (31 downto 0);
        fifo_empty           : in  STD_LOGIC;
        fifo_en              : out STD_LOGIC;
        -- tx fifo interface - 32 bit
        tx_en                : out STD_LOGIC;
        tx_data              : out STD_LOGIC_VECTOR (35 downto 0);    -- 32 is frame flag, 31..0 is data
        tx_full              : in  STD_LOGIC;
        --
        master_sync_sent_cnt : out STD_LOGIC_VECTOR (31 downto 0);
        ack_sync_sent_cnt    : out STD_LOGIC_VECTOR (31 downto 0);
        data_frames_sent_cnt : out STD_LOGIC_VECTOR (31 downto 0)              
    );
end send_cmd;


architecture Behavioral of send_cmd is

    type states is (s_idle, s0, s1, s2, s3, s4, s5, s6, s7, s_rdy);

    signal currentstate           : states := s_idle;
    signal sync_i                 : STD_LOGIC;
    signal fifo_en_i              : STD_LOGIC;
    --
    signal telegramcount          : STD_LOGIC_VECTOR (15 downto 0)     := X"0000";
    --
    signal datalenbuf             : STD_LOGIC_VECTOR (15 downto 0)     := X"AAAA";
    signal channelbuf             : STD_LOGIC_VECTOR (7 downto 0)     := X"55"; 
    --
    signal master_sync_sent_cnt_i : STD_LOGIC_VECTOR (31 downto 0)     := X"00000000";
    signal ack_sync_sent_cnt_i    : STD_LOGIC_VECTOR (31 downto 0)     := X"00000000";
    signal data_frames_sent_cnt_i : STD_LOGIC_VECTOR (31 downto 0)     := X"00000000";
    --
    signal BE_TYPE_H              : STD_LOGIC_VECTOR (7 downto 0);
    signal BE_TYPE_L              : STD_LOGIC_VECTOR (7 downto 0);    
    signal NC_DATA_TYPE_H         : STD_LOGIC_VECTOR (7 downto 0);
    signal NC_DATA_TYPE_L         : STD_LOGIC_VECTOR (7 downto 0);    
    signal NC_SYNC_TYPE_H         : STD_LOGIC_VECTOR (7 downto 0);
    signal NC_SYNC_TYPE_L         : STD_LOGIC_VECTOR (7 downto 0);
    signal NC_ACK_TYPE_H          : STD_LOGIC_VECTOR (7 downto 0);
    signal NC_ACK_TYPE_L          : STD_LOGIC_VECTOR (7 downto 0);
begin
    BE_TYPE_H      <= BE_type     (15 downto 8);
    NC_SYNC_TYPE_H <= NCSync_type (15 downto 8);
    NC_DATA_TYPE_H <= NCData_type (15 downto 8);
    NC_ACK_TYPE_H  <= NCAck_type  (15 downto 8);    
    BE_TYPE_L      <= BE_type     (7 downto 0);
    NC_SYNC_TYPE_L <= NCSync_type (7 downto 0);
    NC_DATA_TYPE_L <= NCData_type (7 downto 0);
    NC_ACK_TYPE_L  <= NCAck_type  (7 downto 0);

    fifo_en              <= fifo_en_i;
    master_sync_sent_cnt <= master_sync_sent_cnt_i;
    ack_sync_sent_cnt    <= ack_sync_sent_cnt_i;
    data_frames_sent_cnt <= data_frames_sent_cnt_i;

    FSM: process (clk)
        variable nextstate : states;
        variable cnt : integer range 0 to 2047 := 0;
        variable enough : STD_LOGIC;
    begin

        if( rising_edge(clk) ) then
            nextstate := currentstate;
         
            if( cnt < 59 ) then         -- check minimum packet length
                enough := '0';
            else
                enough := '1';
            end if;

            case currentstate is
                when s_idle =>
                    tx_data   <= "0" & X"DE" & "0" & X"AD" & "0" & X"BE" & "0" & X"EF";
                    rdy       <= '0';
                    tx_en     <= '0';
                    fifo_en_i <= '0';
                    cnt       := 0;      -- init cnt for min 10 data words
                    sync_i    <= sync;   -- store for later use
                    if( start = '1' ) then
                        nextstate := s0;
                        if( sync = '0' ) then
                            channelbuf <= channel;
                            datalenbuf <= datalen;
                        else
                            channelbuf <= X"FF";
                            datalenbuf <= X"0000";
                        end if;
                    end if;
                when s0 =>
                    if( receiving_sync = '0' ) then
                        if( telegramcount = X"FFFF" ) then
                            telegramcount <= X"0001";
                        else
                            telegramcount <= telegramcount + 1;    -- inc telegram counter (sequence number)
                        end if;
                    end if;
                    nextstate := s1;
                when s1 =>    
                    cnt       := cnt + 4;
                    -- first send broadcast dest address
                    tx_en     <= '1';
                    tx_data   <= "0" & X"FF" & "0" & X"FF" & "0" & X"FF" & "0" & X"FF";
                    nextstate := s2;
                when s2 =>
                    cnt       := cnt + 4;
                    -- 2 bytes broadcast + 2 bytes src
                    tx_en      <= '1';
                    tx_data    <= "0" & X"FF" & "0" & X"FF" & "0" & macaddress(47 downto 40) & "0" & macaddress(39 downto 32);
                    nextstate  := s3;
                    datalenbuf <= datalen;
                when s3 =>
                    cnt       := cnt + 4;
                    -- 4 bytes src
                    tx_en     <= '1';
                    tx_data   <= "0" & macaddress(31 downto 24) & "0" & macaddress(23 downto 16) & "0" & macaddress(15 downto 8) & "0" & macaddress(7 downto 0);
                    nextstate := s4;
                when s4 =>
                    cnt   := cnt + 4;
                    -- 2 bytes len + 2 bytes type
                    tx_en <= '1';
                    if( BE_traffic_mode = '1' ) then
                        tx_data   <= "0" & BE_TYPE_H & "0" & BE_TYPE_L & "0" & TTL & "0" & datalenbuf(7 downto 0);
                        fifo_en_i <= '1';
                    elsif( sync_i='1' ) then
                        tx_data                <= "0" & NC_SYNC_TYPE_H & "0" & NC_SYNC_TYPE_L & "0" & TTL & "0" & datalenbuf(7 downto 0);
                        master_sync_sent_cnt_i <= master_sync_sent_cnt_i + 1;
                    elsif( receiving_sync = '1' ) then
                        tx_data             <= "0" & NC_ACK_TYPE_H & "0" & NC_ACK_TYPE_L & "0" & X"01" & "0" & datalenbuf(7 downto 0);
                        ack_sync_sent_cnt_i <= ack_sync_sent_cnt_i + 1;
                    else
                        tx_data                <= "0" & NC_DATA_TYPE_H & "0" & NC_DATA_TYPE_L & "0" & TTL & "0" & datalenbuf(7 downto 0);
                        fifo_en_i              <= '1';
                        data_frames_sent_cnt_i <= data_frames_sent_cnt_i + 1;
                    end if;
                    nextstate := s5;
                when s5 =>
                    cnt     := cnt + 4;
                    -- 2 bytes len + 2 bytes type
                    tx_en   <= '1';
                    tx_data <= "0" & channelbuf & "0" & varnum & "0" & telegramcount(15 downto 8) & "0" & telegramcount(7 downto 0);
                    if( sync_i = '0' and receiving_sync = '0' ) then
                        nextstate := s6;
                    else
                        nextstate := s7;        -- insert dummy bytes only
                    end if;
                when s6 =>
                    tx_en   <= '1';
                    tx_data <= "0" & fifo_data(31 downto 24) & "0" & fifo_data(23 downto 16) & "0" & fifo_data(15 downto 8) & "0" & fifo_data(7 downto 0);
                    cnt     := cnt + 4;
                    if( fifo_empty = '1' ) then
                        fifo_en_i <= '0';
                        nextstate := s7;
                    else
                        nextstate := s6;
                    end if;
                when s7 =>
                    tx_en <= '1';
                    if( enough = '1' ) then
                        tx_data   <= "1" & X"DE" & "1" & X"AD" & "1" & X"BE" & "1" & X"EF";         -- end of packet
                        nextstate := s_rdy;
                    else
                        tx_data   <= "0" & X"AB" & "0" & X"AB" & "0" & X"AB" & "0" & X"AB";
                        nextstate := s7;
                    end if;
                    cnt := cnt + 4;
                when s_rdy =>
                    tx_data   <= "0" & X"DE" & "0" & X"AD" & "0" & X"BE" & "0" & X"EF";
                    tx_en     <= '0';
                    rdy       <= '1';
                    nextstate := s_idle;
                when others =>
                    nextstate := s_idle;
            end case;
         
            currentstate <= nextstate;
         
        end if;
      
    end process;

end Behavioral;