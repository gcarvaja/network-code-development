
--Copyright 2007-2010, Embedded Software Group at the University of
--Waterloo. All rights reserved.  By using this software the USER
--indicates that he or she has read, understood and will comply with the
--following:

--- Embedded Software Group at the University of Waterloo hereby
--grants USER nonexclusive permission to use, copy and/or modify this
--software for internal, noncommercial, research purposes only. Any
--distribution, including commercial sale or license, of this software,
--copies of the software, its associated documentation and/or
--modifications of either is strictly prohibited without the prior
--consent of the Embedded Software Group at the University of Waterloo.
--Title to copyright to this software and its associated documentation
--shall at all times remain with the Embedded Software Group at the
--University of Waterloo.  Appropriate copyright notice shall be placed
--on all software copies, and a complete copy of this notice shall be
--included in all copies of the associated documentation.  No right is
--granted to use in advertising, publicity or otherwise any trademark,
--service mark, or the name of the Embedded Software Group at the
--University of Waterloo.


--- This software and any associated documentation is provided "as is"

--THE EMBEDDED SOFTWARE GROUP AT THE UNIVERSITY OF WATERLOO MAKES NO
--REPRESENTATIONS OR WARRANTIES, EXPRESS OR IMPLIED, INCLUDING THOSE OF
--MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, OR THAT USE OF
--THE SOFTWARE, MODIFICATIONS, OR ASSOCIATED DOCUMENTATION WILL NOT
--INFRINGE ANY PATENTS, COPYRIGHTS, TRADEMARKS OR OTHER INTELLECTUAL
--PROPERTY RIGHTS OF A THIRD PARTY.

--The Embedded Software Group at the University of Waterloo shall not be
--liable under any circumstances for any direct, indirect, special,
--incidental, or consequential damages with respect to any claim by USER
--or any third party on account of or arising from the use, or inability
--to use, this software or its associated documentation, even if The
--Embedded Software Group at the University of Waterloo has been advised
--of the possibility of those damages.

----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    23:25:54 01/03/2007 
-- Design Name: 
-- Module Name:    receive_cmd - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

---- Uncomment the following library declaration if instantiating
---- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity receive_cmd is
    Port(
        clk                      : in  STD_LOGIC;
        -- cmd interface
        start                    : in  STD_LOGIC;
        rdy                      : out STD_LOGIC;
        fault                    : out STD_LOGIC;
        fault_empty              : out STD_LOGIC;
        varnum                   : in  STD_LOGIC_VECTOR (7 downto 0);
        channel                  : in  STD_LOGIC_VECTOR (7 downto 0);
        offset                   : in  STD_LOGIC_VECTOR (7 downto 0);
        -- read config setup
        cfgaddress               : out STD_LOGIC_VECTOR (7 downto 0);
        cfgdata                  : in  STD_LOGIC_VECTOR (31 downto 0); 
        cfgdata_queue            : in  STD_LOGIC_VECTOR (31 downto 0);
        cfgdata_queue_write_back : out STD_LOGIC_VECTOR (31 downto 0);
        cfgdata_queue_wen        : out STD_LOGIC;      
        -- copy variable
        varaddress               : out STD_LOGIC_VECTOR (15 downto 0);
        vardata                  : out STD_LOGIC_VECTOR (31 downto 0);
        var_wen                  : out STD_LOGIC;
        -- from fifo
        fifo_en                  : out STD_LOGIC;
        fifo_empty               : in  STD_LOGIC;
        fifo_reset               : out STD_LOGIC;
        fifo_channel             : out STD_LOGIC_VECTOR (3 downto 0);
        fifo_data                : in  STD_LOGIC_VECTOR (31 downto 0);
        fifo_wr_ack              : in  STD_LOGIC;
        --
        rcv_cmd_fault_cnt        : out STD_LOGIC_VECTOR (31 downto 0);
        rcv_cmd_ok_cnt           : out STD_LOGIC_VECTOR (31 downto 0)
    );
end receive_cmd;


architecture Behavioral of receive_cmd is
    type states is (s_idle, s1, s2, s3, s4, s5, s6, s7, s_rdy, s_eval_queue);
    signal currentstate  : states := s_idle;
    signal varaddress_i  : STD_LOGIC_VECTOR(15 downto 0) := X"FFFF";
    signal varlen_i      : STD_LOGIC_VECTOR(15 downto 0) := X"FFFF";
    signal var_wen_i     : STD_LOGIC := '0';
    signal channel_i     : STD_LOGIC_VECTOR(7 downto 0) := X"00";
    signal fault_i       : STD_LOGIC := '0';
    signal fault_empty_i : STD_LOGIC := '0';
    
    signal num_elements_i      : STD_LOGIC_VECTOR(7 downto 0) := X"FF";
    signal queue_offset        : STD_LOGIC_VECTOR(7 downto 0) := X"FF";
    
    signal rcv_cmd_fault_cnt_i : STD_LOGIC_VECTOR(31 downto 0) := X"00000000";
    signal rcv_cmd_ok_cnt_i    : STD_LOGIC_VECTOR(31 downto 0) := X"00000000";
begin

    cfgdata_queue_write_back <= cfgdata_queue(31 downto 8) & num_elements_i;
    
    -- cfgdata_queue is a 32 bit word where:
    -- cfgdata_queue(31) : queue flag (1 if variable support queue)
    -- cfgdata_queue(30 downto 24): queue size (in number of elements)
    -- cfgdata_queue(23 downto 16): element size (size of every element in 32 bits words)
    -- cfgdata_queue(15 downto 8): start pointer (first element in the queue)
    -- cfgdata_queue(7 downto 0): number of valid elements ( >= 0 and < queue size)
    
    rcv_cmd_fault_cnt <= rcv_cmd_fault_cnt_i;
    rcv_cmd_ok_cnt    <= rcv_cmd_ok_cnt_i;
    
    queue_offset      <= cfgdata_queue(15 downto 8) + cfgdata_queue(7 downto 0);
    
    vardata           <= fifo_data;
    varaddress        <= varaddress_i;
    var_wen           <= var_wen_i;
   
    fifo_channel      <= channel_i(3 downto 0);

    FSM: process(clk)
        variable nextstate          : states;
        variable big_queue_offset   : STD_LOGIC := '0';
        variable temp_queue_address : STD_LOGIC_VECTOR (15 downto 0);
        variable queue_offset_var   : STD_LOGIC_VECTOR (7 downto 0);
    begin

--      if (rst_n = '0') then
--          currentstate <= s_init;
--          nextstate    := s_init;
        if( rising_edge(clk) ) then
            nextstate := currentstate;
            
            case currentstate is
--              when s_init => 
--                  rcv_cmd_fault_cnt <= (others => '0');
--                  rcv_cmd_ok_cnt    <= (others => '0');
--                  nextstate         := s_idle;
                when s_idle =>
                    fault_i           <= '0';
                    rdy               <= '0';
                    fifo_en           <= '0';
                    fifo_reset        <= '0';
                    cfgdata_queue_wen <= '0';
                    if( start = '1' ) then
                        cfgaddress <= varnum;
                        channel_i  <= channel;
                        nextstate  := s1;
                    else
                        cfgaddress <= X"00";
                        channel_i  <= X"00";
                    end if;
                ------------------------------
                when s1 =>           -- check empty bit for the channel
--                  if( fifo_empty = '1' ) then    -- error
--                      if( fifo_wr_ack = '0' ) then
--                          fault_i   <= '1';
--                          nextstate := s_rdy;
--                      end if;
--                  else
                    if( queue_offset > (cfgdata_queue(30 downto 24)-1) ) then        -- save time and do some precalcs for s5 here
                        big_queue_offset := '1';
                    else
                        big_queue_offset := '0';
                    end if;
                    queue_offset_var := queue_offset - cfgdata_queue(30 downto 24);
                    nextstate        := s_eval_queue;   -- aca se modifica
                    -- end if;
                ------------------------------
                when s_eval_queue =>
                    if( cfgdata_queue(31) = '0' ) then -- varlen 0
                        nextstate := s2;
                    elsif( cfgdata_queue(31) = '1' ) then
                        if( big_queue_offset = '1' ) then
                            temp_queue_address := cfgdata_queue(23 downto 16) * queue_offset_var;
                        else
                            temp_queue_address := cfgdata_queue(23 downto 16) * queue_offset;
                        end if;
                        nextstate := s5;
                    end if;
                ------------------------------
                --- No queue procedure, it has no changes since NCM2 -----
                when s2 =>           -- get var description
                    varaddress_i <= cfgdata(31 downto 16);
                    varlen_i     <= cfgdata(15 downto 0);
                    if( cfgdata(15 downto 0) = X"0000" ) then
                        fifo_reset    <= '1';
                        nextstate     := s_rdy;
                    elsif( fifo_empty = '1' ) then    -- error
                        fault_i       <= '1';
                        fault_empty_i <= '1';
                        nextstate     := s_rdy;
                    elsif( cfgdata(15 downto 0) = X"0000" ) then   -- nothing to do
                        nextstate     := s_rdy;
                        fault_empty_i <= '0';
                    else
                        fifo_en       <= '1';
                        fault_empty_i <= '0';
                        nextstate     := s3;
                    end if;
                ------------------------------
                when s3 =>
                    -- spend one cycle to wait for fifo
                    varlen_i  <= varlen_i - 1;
                    var_wen_i <= '1';
                    nextstate := s4;
                when s4 =>
                    if( varlen_i = X"0000" ) then
                        var_wen_i <= '0';
                        nextstate := s_rdy;
                    elsif( fifo_empty = '1' ) then
                        fault_i    <= '1';
                        nextstate  := s_rdy;
                    elsif( varlen_i = X"0001" ) then
                        fifo_en      <= '0';
                        var_wen_i    <= '1';
                        varaddress_i <= varaddress_i + 1;
                        varlen_i     <= varlen_i - 1;
                        nextstate    := s4;
                    else   -- keep on copying
                        fifo_en      <= '1';
                        var_wen_i    <= '1';
                        varaddress_i <= varaddress_i + 1;
                        varlen_i     <= varlen_i - 1;
                        nextstate    := s4;
                    end if;

--                  if (varlen_i = X"0001") then           -- stop fifo
--                      fifo_en <= '0';
--                      var_wen_i <= '1';
--                      varaddress_i <= varaddress_i + 1;
--                      varlen_i <= varlen_i - 1;
--                      nextstate := s4;
--                  elsif (varlen_i = X"0000") then        -- done
--                      var_wen_i <= '0';
--                      nextstate := s_rdy;
--                  else                                   -- keep on copying
--                      fifo_en <= '1';
--                      var_wen_i <= '1';
--                      varaddress_i <= varaddress_i + 1;
--                      varlen_i <= varlen_i - 1;
--                      nextstate := s4;
--                  end if;
                ------------------------------
                -------- Queue procedure -----
                when s5 =>           -- get var description
                    -- check the actual variable position for a circular buffer scheme
                    varaddress_i   <= cfgdata(31 downto 16) + temp_queue_address;
                    varlen_i       <= X"00" & cfgdata_queue(23 downto 16);
                    num_elements_i <= cfgdata_queue(7 downto 0);
                    -- if queue is full , nothing to do
                    if( cfgdata_queue(23 downto 16) = X"00" or cfgdata_queue(30 downto 24) = cfgdata_queue(7 downto 0) ) then   -- nothing to do
                        nextstate := s_rdy;
                    else
                        fifo_en   <= '1';
                        nextstate := s6;
                    end if;
                ------------------------------
                when s6 =>
                    -- spend one cycle to wait for fifo
                    varlen_i          <= varlen_i - 1;
                    var_wen_i         <= '1';
                    -- update of number of valid elements
                    num_elements_i    <= num_elements_i + 1;
                    cfgdata_queue_wen <= '1';
                    nextstate         := s7;
                ------------------------------
                when s7 =>
                    cfgdata_queue_wen <= '0';
                    if( varlen_i = X"0001" ) then           -- stop fifo
                        fifo_en      <= '0';
                        var_wen_i    <= '1';
                        varaddress_i <= varaddress_i + 1;
                        varlen_i     <= varlen_i - 1;
                        nextstate    := s7;
                    elsif( varlen_i = X"0000" ) then        -- done
                        var_wen_i <= '0';
                        nextstate := s_rdy;
                    else                                   -- keep on copying
                        fifo_en      <= '1';
                        var_wen_i    <= '1';
                        varaddress_i <= varaddress_i + 1;
                        varlen_i     <= varlen_i - 1;
                        nextstate    := s7;
                    end if;
                ------------------------------
                when s_rdy =>
                    if( fault_i='1' ) then
                        rcv_cmd_fault_cnt_i <= rcv_cmd_fault_cnt_i + 1;
                    else
                        rcv_cmd_ok_cnt_i <= rcv_cmd_ok_cnt_i + 1;
                    end if;
                    rdy         <= '1';
                    fault       <= fault_i;
                    fault_empty <= fault_empty_i;
                    nextstate   := s_idle;
                ------------------------------
                when others =>
                    nextstate := s_idle;
            end case;

            currentstate <= nextstate;
        end if;

    end process;

end Behavioral;
