`timescale 1ns / 1ps

module rx_buffer(

	input 				clk,
	input 				dvld,
	input 				goodframe,
	input 				badframe,
	input 				RT_frame_marker,
	input 				reset,
	input 		[7:0]	gmac_rx_data,
	output reg 	[7:0]	gmac_rx_data_d1,
	output reg 			eop,
	output 		   	rx_fifo_wr_en,
	output reg 			rx_fifo_wr_en_frame,
	output reg 			send_start_RT,
	output reg 			reset_fifo
    );


   reg 				dvld_d1;
	reg [1:0]      rx_state;
   reg [1:0]      rx_state_nxt;
	reg [1:0]      counter;
	reg [1:0]      pad_counter;
	reg rx_fifo_wr_en_pad;
	
   parameter RX_IDLE            		= 0;
   parameter RX_RCV_PKT         		= 1;
   parameter RX_WAIT_GOOD_OR_BAD		= 2;
   parameter RX_ADD_PAD         		= 3;

	assign rx_fifo_wr_en = rx_fifo_wr_en_pad || rx_fifo_wr_en_frame;
   
	always @(*) begin
      rx_state_nxt           = rx_state;
      eop                    = 0;
		send_start_RT = 0;
		reset_fifo = 0;
		rx_fifo_wr_en_pad = 0;
		rx_fifo_wr_en_frame = 0;
      case(rx_state)

			RX_IDLE: begin
				if(dvld_d1 && RT_frame_marker) begin
					rx_fifo_wr_en_frame   = 1;
					rx_state_nxt    = RX_RCV_PKT;
				end
			end
        
			RX_RCV_PKT: begin
           rx_fifo_wr_en_frame = 1;
				if(!dvld) begin
					eop = 1;
					rx_state_nxt = RX_WAIT_GOOD_OR_BAD;
				end
			end
		  
			RX_WAIT_GOOD_OR_BAD: begin
				if(goodframe) begin
					rx_state_nxt       = RX_ADD_PAD;
					send_start_RT = 1;
				end
				else if(badframe) begin
					reset_fifo = 1;
					rx_state_nxt       = RX_IDLE;
				end
			end // case: WAIT_GOOD_OR_BAD


			RX_ADD_PAD: begin
				send_start_RT = 1;
				if (pad_counter == 0) begin
					rx_state_nxt = RX_IDLE;
				end
				else begin
					rx_fifo_wr_en_pad = 1;
					eop = 1;
					rx_state_nxt = RX_ADD_PAD;
				end
			end
			  	
			default: begin end
		endcase // case(rx_state)
   end // always @ (*)
	
   always @(posedge clk) begin
      if(reset) begin
         rx_state             <= RX_IDLE;
			counter              <= 0;
			pad_counter				<= 0;
			dvld_d1              <= 0;
      end
      else begin
         rx_state           <= rx_state_nxt;
			dvld_d1            <= dvld;
			gmac_rx_data_d1	 <= gmac_rx_data;
			
         if (rx_fifo_wr_en_frame) begin
            counter <= counter + 'b1;
				pad_counter <= pad_counter + 'b1;
			end
			else if (rx_fifo_wr_en_pad)
				pad_counter <= pad_counter + 'b1;
				
         else if(rx_state == RX_IDLE) begin
            counter <= 0;
				pad_counter <= 0;
			end	
		end
   end // always @ (posedge rxcoreclk)	
	
endmodule