`timescale 1ns / 1ps

module phy_time_bytes_cnt(

	input 					clk,
	input 					run_NCM,
	input						reset,
	input						control_reg_hit,
	input						en_bytes_cnt,

	output reg	[31:0]	running_time_cnt,
	output reg	[31:0]	total_bytes_cnt

    );

   parameter INIT            			= 1;
   parameter ACTIVE	         		= 2;
	
	reg [1:0]      state = INIT;
   reg [1:0]      state_nxt;
	reg [32:0]		delay = 0;

	always @(*) begin
      state_nxt           = state;    
		case(state)
			INIT: begin
				if(delay >= 130000000)
					state_nxt = ACTIVE;
				else 
					state_nxt = INIT;
//				if(control_reg_hit)
//					state_nxt = ACTIVE;
//				else 
//					state_nxt = INIT;
			end

			ACTIVE: begin 
				state_nxt = ACTIVE; 
			end
			  	
			default: begin end
		endcase 
   end
	
   always @(posedge clk) begin
      if(reset) begin
         state          <= INIT;
			running_time_cnt 	<= 0;
			total_bytes_cnt 	<= 0;
			delay <= 0;
      end
      
		else begin
         state           <= state_nxt;
			if (state == ACTIVE) begin
					if (run_NCM) begin
						running_time_cnt <= running_time_cnt + 1;
						if(en_bytes_cnt)
							total_bytes_cnt <= total_bytes_cnt + 1;
					end
			end
			else begin //if (state == INIT) begin
					delay <= delay +1;
					running_time_cnt 	<= 0;
					total_bytes_cnt 	<= 0;
			end
		end
	end
endmodule