`timescale 1ns/1ps
  module rx_queue
   (
	 input 										reset,
	 output [35:0]                      data_from_rx_fifo_to_autorec, 

		input										reset_rx_fifo,
	 
	 output                             rx_fifo_empty_RT,
	 output										autorec_start,
	 input  					               rx_rden_from_autorec,
	 output										start_send_ack,
	 output										receiving_sync,
	 
	 input [15:0]								NCData_type,
	 input [15:0]								NCSync_type,
	 input [15:0]								NCAck_type,
	 
    // --- MAC side signals (rxcoreclk domain)

    input  [7:0]                       gmac_rx_data_temp,
    input                              gmac_rx_dvld_temp,
    input                              gmac_rx_goodframe_temp,
    input                              gmac_rx_badframe_temp,

    // --- Register interface
    output reg [31:0]                	rx_goodframes_RT_sync_cnt,
    output reg [31:0]                  rx_goodframes_RT_data_cnt,
    output reg [31:0]                  rx_badframes_RT_sync_cnt,
    output reg [31:0]                  rx_badframes_RT_data_cnt,
	 output reg [63:0]                  rx_bytes_RT_cnt,
    output reg [31:0]                  rx_goodframes_non_RT_cnt,
    output reg [31:0]                  rx_badframes_non_RT_cnt,
    output reg [63:0]                  rx_bytes_non_RT_cnt,
	 output reg [31:0] 						rx_good_frames_error_cnt,
	 output  [31:0] 							rx_total_RT_ack_frames_cnt,
	 output  [31:0] 							rx_total_RT_data_frames_cnt,	 
	 output  [31:0] 							rx_total_RT_sync_frames_cnt,
	 output  [31:0] 							rx_total_nonRT_frames_cnt,	 
	 output reg [31:0] 						rx_goodframes_total_cnt,
	 output reg [31:0] 						rx_badframes_total_cnt,

	 input 										rx_fifo_rden,
	 output 										rx_fifo_empty,
	 output 										rx_fifo_data_valid,
    input                              clk_ncm,
    input                              rxcoreclk
   );

	wire 			gmac_rx_dvld;
	wire [7:0] 	gmac_rx_data;
	wire [7:0] 	gmac_rx_data_d1;
	wire 			gmac_rx_goodframe;
	wire 			gmac_rx_badframe;

	input_class_delay input_class_delay (
		.clk 							(rxcoreclk),
		.rst 							(reset),
		.gmac_rx_data_in 			(gmac_rx_data_temp),
		.gmac_rx_data_out 		(gmac_rx_data),
		.gmac_rx_dvld_in 			(gmac_rx_dvld_temp),
		.gmac_rx_dvld_out 		(gmac_rx_dvld_aux),
		.gmac_rx_goodframe_in 	(gmac_rx_goodframe_temp),
		.gmac_rx_goodframe_out 	(gmac_rx_goodframe_aux),
		.gmac_rx_badframe_in 	(gmac_rx_badframe_temp),
		.gmac_rx_badframe_out 	(gmac_rx_badframe_aux),
		
		.NCSync_type				(NCSync_type),
		.NCData_type				(NCData_type),
		.NCAck_type				(NCAck_type),
		
		.RT_ack_frames_counter	(rx_total_RT_ack_frames_cnt),
		.RT_data_frames_counter	(rx_total_RT_data_frames_cnt),
		.RT_sync_frames_counter	(rx_total_RT_sync_frames_cnt),
		.nonRT_frames_counter	(rx_total_nonRT_frames_cnt),
		.RT_sync_marker 			(RT_sync_marker),
		.RT_data_marker 			(RT_data_marker),
		.RT_ack_marker 			(RT_ack_marker),
		.start_send_ack			(start_send_ack),
		.receiving_sync			(receiving_sync)
);
	  
	assign RT_frame_marker		= RT_sync_marker || RT_data_marker;
	assign BE_frame_marker		= !RT_frame_marker && !RT_ack_marker;
	assign gmac_rx_dvld 			= gmac_rx_dvld_aux && BE_frame_marker;
	assign gmac_rx_goodframe 	= gmac_rx_goodframe_aux && BE_frame_marker;
	assign gmac_rx_badframe 	= gmac_rx_badframe_aux && BE_frame_marker;

	rx_buffer rx_buffer (
		.clk 							(rxcoreclk),
		.dvld   						(gmac_rx_dvld_aux),
		.goodframe 					(gmac_rx_goodframe_aux),
		.badframe 					(gmac_rx_badframe_aux),
		.RT_frame_marker 			(RT_frame_marker),
		.reset 						(reset),
		.gmac_rx_data				(gmac_rx_data),
		.gmac_rx_data_d1			(gmac_rx_data_d1),
		.eop 							(eop_RT),
		.rx_fifo_wr_en 			(wren_RT),
		.rx_fifo_wr_en_frame 	(rx_fifo_wr_en_frame),
		.send_start_RT 			(autorec_start),
		.reset_fifo 				(rst_RT_fifo)
	);

//assign rst_RT_fifo = reset || reset_fifo_RT || !RT_frame_marker ;

assign rst_RT = rst_RT_fifo || reset_rx_fifo ;

	rx_mac_fifo rx_mac_fifo
	(
		.din 				({eop_RT, gmac_rx_data_d1}),
		.rd_clk  		(clk_ncm), //(rxcoreclk),
		.rd_en  			(rx_fifo_rden),
		.rst  			(rst_RT), //(reset),
			
		.wr_clk  		(rxcoreclk),
		.wr_en  			(wren_RT), //(gmac_rx_dvld_aux),
		.dout  			(data_from_rx_fifo_to_autorec),
			
		.empty 			(rx_fifo_empty),
		.full  			(),
		.valid  			(rx_fifo_data_valid)
		);			

   always @(posedge rxcoreclk) begin
			if(reset) begin
					rx_goodframes_RT_sync_cnt <= 0;
					rx_goodframes_RT_data_cnt <= 0;
					rx_badframes_RT_sync_cnt <=0;
					rx_badframes_RT_data_cnt <=0;
					rx_bytes_RT_cnt <=0;
					rx_goodframes_non_RT_cnt <= 0;
					rx_badframes_non_RT_cnt <=0;
					rx_goodframes_total_cnt <= 0;
					rx_badframes_total_cnt <= 0;					
					rx_bytes_non_RT_cnt <=0;
					rx_good_frames_error_cnt <=0;
          end
			 else begin
			if(gmac_rx_dvld_aux) begin
				if(RT_frame_marker)
					rx_bytes_RT_cnt <= rx_bytes_RT_cnt + 1;
				else
					rx_bytes_non_RT_cnt <= rx_bytes_non_RT_cnt + 1;
         end
			
			if (gmac_rx_goodframe_aux) begin
			   rx_goodframes_total_cnt <= rx_goodframes_total_cnt + 1;
				if(RT_sync_marker && !RT_data_marker)
					rx_goodframes_RT_sync_cnt <= rx_goodframes_RT_sync_cnt + 1;
				else if (RT_data_marker && !RT_sync_marker)
					rx_goodframes_RT_data_cnt <= rx_goodframes_RT_data_cnt + 1;
				else if (!RT_data_marker && !RT_sync_marker)
					rx_goodframes_non_RT_cnt <= rx_goodframes_non_RT_cnt + 1;
				else 
					rx_good_frames_error_cnt <= rx_good_frames_error_cnt + 1;
			end	      
			
			if (gmac_rx_badframe_aux) begin
			   rx_badframes_total_cnt <= rx_badframes_total_cnt + 1;
				if(RT_sync_marker && !RT_data_marker)
					rx_badframes_RT_sync_cnt <= rx_badframes_RT_sync_cnt + 1;
				else if (RT_data_marker && !RT_sync_marker)
					rx_badframes_RT_data_cnt <= rx_badframes_RT_data_cnt + 1;
				else if (!RT_data_marker && !RT_sync_marker)
					rx_badframes_non_RT_cnt <= rx_badframes_non_RT_cnt + 1;
				else 
					rx_good_frames_error_cnt <= rx_good_frames_error_cnt + 1;
			end
		
      end // always @ (posedge rxcoreclk)	
		end	
endmodule