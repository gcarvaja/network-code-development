----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    22:40:14 10/27/2006 
-- Design Name: 
-- Module Name:    ncm_main - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

---- Uncomment the following library declaration if instantiating
---- any Xilinx primitives in this code.
library UNISIM;
use UNISIM.VComponents.all;

use work.ncm_package.all;

entity ncm_main is
    generic(
        PROG_MEM_INIT_FILE : string := "";
        CONF_MEM_INIT_FILE : string := ""
    );
    Port(
        sys_clk                 : in  STD_LOGIC;
        app_clk                 : in  STD_LOGIC;
        clk_ext_bus             : in  STD_LOGIC;
        rst                     : in  STD_LOGIC;
        start                   : in  STD_LOGIC;
        stop                    : in  STD_LOGIC;
        app_sync_pulse          : out STD_LOGIC;
        --
        BE_type                 : in  STD_LOGIC_VECTOR (15 downto 0);
        NCSync_type             : in  STD_LOGIC_VECTOR (15 downto 0);
        NCData_type             : in  STD_LOGIC_VECTOR (15 downto 0);
        NCAck_type              : in  STD_LOGIC_VECTOR (15 downto 0);
        sync_ack_en             : in  STD_LOGIC;
        --
        varram_app_wen          : in  STD_LOGIC;
        varram_app_addr         : in  STD_LOGIC_VECTOR (15 downto 0);
        varram_app_din          : in  STD_LOGIC_VECTOR (31 downto 0);
        varram_app_dout         : out STD_LOGIC_VECTOR (31 downto 0);
        --
        progrom_bus_address     : in  STD_LOGIC_VECTOR (7 downto 0);
        progrom_bus_din         : in  STD_LOGIC_VECTOR (31 downto 0);
        progrom_bus_dout        : out STD_LOGIC_VECTOR (31 downto 0);
        progrom_bus_wen         : in  STD_LOGIC ;
        --
        cfgrom_bus_address      : in  STD_LOGIC_VECTOR (7 downto 0);
        cfgrom_bus_din          : in  STD_LOGIC_VECTOR (31 downto 0);
        cfgrom_bus_dout         : out STD_LOGIC_VECTOR (31 downto 0);
        cfgrom_bus_wen          : in  STD_LOGIC;
        --
        counters_bus_address    : in  STD_LOGIC_VECTOR (7 downto 0);
        counters_bus_din        : in  STD_LOGIC_VECTOR (31 downto 0);
        counters_bus_dout       : out STD_LOGIC_VECTOR (31 downto 0);
        counters_bus_wen        : in  STD_LOGIC;
        --
        rx_clk                  : in  STD_LOGIC;
        rx_dv_to_mac            : in  STD_LOGIC;
        rx_er_to_mac            : in  STD_LOGIC;
        rxd_to_mac              : in  STD_LOGIC_VECTOR (7 downto 0); 
        --
        tx_clk                  : in  STD_LOGIC;
        tx_en_from_mac          : out STD_LOGIC;
        tx_er_from_mac          : out STD_LOGIC;
        txd_from_mac            : out STD_LOGIC_VECTOR (7 downto 0);
        phy_rst_n               : out STD_LOGIC;
        --
        userbits                : in  STD_LOGIC_VECTOR (7 downto 0);
        -- Receive command status
        receive_fault_empty     : out STD_LOGIC
    );  -- data out
end ncm_main;


architecture Behavioral of ncm_main is

begin

    phy_rst_n <= '1'; 

    TheNCM : ncm_frame
    generic map(
        PROG_MEM_INIT_FILE => PROG_MEM_INIT_FILE,
        CONF_MEM_INIT_FILE => CONF_MEM_INIT_FILE
    )
    Port Map( 
        clk_ncm                     => sys_clk,
        clk_app                     => app_clk,
        clk_ext_bus                 => clk_ext_bus,
        sys_rst                     => rst,
        control_reg_hit             => '0',
        pclock_divider              => "00000000001",
        app_sync_pulse              => app_sync_pulse,
        -- memory interface to cpu
        varram_app_wen              => varram_app_wen,
        varram_app_addr             => varram_app_addr,
        varram_app_din              => varram_app_din,
        varram_app_dout             => varram_app_dout,
        --
        progrom_bus_address         => progrom_bus_address,
        progrom_bus_din             => progrom_bus_din,
        progrom_bus_dout            => progrom_bus_dout,
        progrom_bus_wen             => progrom_bus_wen,
        --
        cfgrom_bus_address          => cfgrom_bus_address,
        cfgrom_bus_din              => cfgrom_bus_din,
        cfgrom_bus_dout             => cfgrom_bus_dout,
        cfgrom_bus_wen              => cfgrom_bus_wen,
        --
        cfgrom_queue_bus_address    => X"00",
        cfgrom_queue_bus_din        => X"DEADBEEF", 
        cfgrom_queue_bus_dout       => open,
        cfgrom_queue_bus_wen        => '0',
        --
        counters_bus_address        => counters_bus_address,
        counters_bus_din            => counters_bus_din, 
        counters_bus_dout           => counters_bus_dout,
        counters_bus_wen            => counters_bus_wen, 
        -- GMII MAC
        gmii_rx_clk                 => rx_clk,
        gmii_rx_dv                  => rx_dv_to_mac,
        gmii_rx_er                  => rx_er_to_mac, 
        gmii_rxd                    => rxd_to_mac,
        gmii_tx_clk                 => tx_clk,
        gmii_tx_en                  => tx_en_from_mac,
        gmii_tx_er                  => tx_er_from_mac,
        gmii_txd                    => txd_from_mac,
        -- MAC address
        mac_high                    => NCM_MAC (47 downto 24),
        mac_low                     => NCM_MAC (23 downto 0),
        --
        BE_type                     => BE_type,	
        NCData_type                 => NCData_type,
        NCSync_type                 => NCSync_type,
        NCAck_type                  => NCAck_type,
        sync_ack_en                 => sync_ack_en,
        -- status lines
        ncm_shouldstopeval          => open,
        -- command bits
        ncm_user                    => userbits,
        ncm_shouldstop              => '0',
        -- switch NCM on/off
        run_NCM                     => '1',
        -- Receive command status
        receive_fault_empty         => receive_fault_empty
    ); -- data out

end Behavioral;