----------------------------------------------------------------------------------
-- Company: 		 FHWN
-- Engineer: 		 TR
-- 
-- Create Date:    21:36:01 10/27/2006 
-- Design Name: 	 NCM basic
-- Module Name:    config_rom - Behavioral 
-- Project Name: 	 NCM
-- Target Devices: 
-- Tool versions: 
-- Description: 	the variable space rom for the Network Code Machine
--						dual port - A for cpu bus R/W
--										B for NCM processor RO
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

---- Uncomment the following library declaration if instantiating
---- any Xilinx primitives in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity config_rom_queue is
    Port ( clk_ncm : in  STD_LOGIC;
           clk_ext_bus : in  STD_LOGIC;
			  -- interface to uP
           bus_address : in  STD_LOGIC_VECTOR (7 downto 0);
           bus_data_in : in  STD_LOGIC_VECTOR (31 downto 0);
           bus_data_out : out  STD_LOGIC_VECTOR (31 downto 0);
           bus_rw : in  STD_LOGIC;
           -- NCM interface
           ncm_var_num : in  STD_LOGIC_VECTOR (7 downto 0);
           ncm_cfg_queue_data_out : out  STD_LOGIC_VECTOR (31 downto 0);
			  ncm_cfg_queue_update : in  STD_LOGIC_VECTOR (31 downto 0);
			  ncm_rw : in  STD_LOGIC
			  );
			  
end config_rom_queue;

architecture Behavioral of config_rom_queue is

	signal wea : std_logic; --_vector(3 downto 0);
	signal addressa : std_logic_vector(8 downto 0);
	signal addressb : std_logic_vector(8 downto 0);
	signal bus_data_out_i : STD_LOGIC_VECTOR (31 downto 0);
	
begin
	bus_data_out <= bus_data_out_i;
	addressa <= '0' & bus_address; 
	addressb <= '0' & ncm_var_num; 

	BLOCK6 : RAMB16_S36_S36
	generic map (
	INIT_A => X"000000000", -- Value of output RAM registers on Port A at startup
	INIT_B => X"000000000", -- Value of output RAM registers on Port B at startup
	
	SIM_COLLISION_CHECK => "ALL", -- "NONE", "WARNING", "GENERATE_X_ONLY", "ALL"
	
	SRVAL_A => X"000000000", -- Port A ouput value upon SSR assertion
	SRVAL_B => X"000000000", -- Port B ouput value upon SSR assertion
	WRITE_MODE_A => "WRITE_FIRST", -- WRITE_FIRST, READ_FIRST or NO_CHANGE
	WRITE_MODE_B => "WRITE_FIRST", -- WRITE_FIRST, READ_FIRST or NO_CHANGE

      -- The following INIT_xx declarations specify the initial contents of the RAM
      --           7       6       5       4       3       2       1       0 
      INIT_00 => X"0000000000000000000000000000000000000000000000000000000000000000",     -- 00
      INIT_01 => X"0000000000000000000000000000000000000000000000000000000000000000")

   port map (
      DOA => bus_data_out_i,      -- 32-bit A port Data Output
      DOB => ncm_cfg_queue_data_out,      -- 32-bit B port Data Output
      DOPA => open,    -- 4-bit  A port Parity Output
      DOPB => open,    -- 4-bit  B port Parity Output
      ADDRA => addressa,  -- 15-bit A port Address Input
      ADDRB => addressb,  -- 15-bit B port Address Input
      CLKA => clk_ext_bus,    -- Port A Clock
      CLKB => clk_ncm,    -- Port B Clock
      DIA => bus_data_in,      -- 32-bit A port Data Input
      DIB => ncm_cfg_queue_update,      -- 32-bit B port Data Input
      DIPA => "1111",    -- 4-bit  A port parity Input
      DIPB => "1111",    -- 4-bit  B port parity Input
      ENA => '1',      -- 1-bit  A port Enable Input
      ENB => '1',      -- 1-bit  B port Enable Input
      SSRA => '0',    -- 1-bit  A port Synchronous Set/Reset Input
      SSRB => '0',    -- 1-bit  B port Synchronous Set/Reset Input
      WEA => bus_rw,      -- 4-bit  A port Write Enable Input
      WEB => ncm_rw      -- 4-bit  B port Write Enable Input
   );
	
end Behavioral;

