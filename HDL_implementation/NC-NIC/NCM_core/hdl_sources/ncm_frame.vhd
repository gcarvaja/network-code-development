library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

---- Uncomment the following library declaration if instantiating
---- any Xilinx primitives in this code.
library UNISIM;
use UNISIM.VComponents.all;

use work.ncm_package.all;

entity ncm_frame is
    generic(
        PROG_MEM_INIT_FILE : string := "";
        CONF_MEM_INIT_FILE : string := ""
    );
    Port(
        clk_ncm                     : in  STD_LOGIC;
        clk_app                     : in  STD_LOGIC;
        clk_ext_bus                 : in  STD_LOGIC;
        sys_rst                     : in  STD_LOGIC;
        control_reg_hit             : in  STD_LOGIC;
        pclock_divider              : in  STD_LOGIC_VECTOR (10 downto 0);
        -- memory interface to cpu
        varram_app_wen              : in  STD_LOGIC;
        varram_app_addr             : in  STD_LOGIC_VECTOR (15 downto 0);
        varram_app_din              : in  STD_LOGIC_VECTOR (31 downto 0);
        varram_app_dout             : out STD_LOGIC_VECTOR (31 downto 0);
        --
        progrom_bus_address         : in  STD_LOGIC_VECTOR (7 downto 0);
        progrom_bus_din             : in  STD_LOGIC_VECTOR (31 downto 0);
        progrom_bus_dout            : out STD_LOGIC_VECTOR (31 downto 0);
        progrom_bus_wen             : in  STD_LOGIC ;
        --
        cfgrom_bus_address          : in  STD_LOGIC_VECTOR (7 downto 0);
        cfgrom_bus_din              : in  STD_LOGIC_VECTOR (31 downto 0);
        cfgrom_bus_dout             : out STD_LOGIC_VECTOR (31 downto 0);
        cfgrom_bus_wen              : in  STD_LOGIC;
        --
        cfgrom_queue_bus_address    : in  STD_LOGIC_VECTOR (7 downto 0);
        cfgrom_queue_bus_din        : in  STD_LOGIC_VECTOR (31 downto 0);
        cfgrom_queue_bus_dout       : out STD_LOGIC_VECTOR (31 downto 0);
        cfgrom_queue_bus_wen        : in  STD_LOGIC;
        --
        counters_bus_address        : in  STD_LOGIC_VECTOR (7 downto 0);
        counters_bus_din            : in  STD_LOGIC_VECTOR (31 downto 0);
        counters_bus_dout           : out STD_LOGIC_VECTOR (31 downto 0);
        counters_bus_wen            : in  STD_LOGIC;
        -- GMII MAC
        gmii_rx_clk                 : in  STD_LOGIC;
        gmii_rx_dv                  : in  STD_LOGIC;
        gmii_rx_er                  : in  STD_LOGIC;
        gmii_rxd                    : in  STD_LOGIC_VECTOR (7 downto 0);
        gmii_tx_clk                 : in  STD_LOGIC;
        gmii_tx_en                  : out STD_LOGIC;
        gmii_tx_er                  : out STD_LOGIC;
        gmii_txd                    : out STD_LOGIC_VECTOR (7 downto 0);
        -- MAC address
        mac_high                    : in  STD_LOGIC_VECTOR (23 downto 0);
        mac_low                     : in  STD_LOGIC_VECTOR (23 downto 0);
        --
        BE_type                     : in  STD_LOGIC_VECTOR (15 downto 0);
        NCData_type                 : in  STD_LOGIC_VECTOR (15 downto 0);
        NCSync_type                 : in  STD_LOGIC_VECTOR (15 downto 0);
        NCAck_type                  : in  STD_LOGIC_VECTOR (15 downto 0);
        sync_ack_en                 : in  STD_LOGIC;
        --
        app_sync_pulse              : out std_logic;
        -- status lines
        ncm_shouldstopeval          : out STD_LOGIC;
        -- command bits
        ncm_user                    : in  STD_LOGIC_VECTOR (7 downto 0);
        ncm_shouldstop              : in  STD_LOGIC;
        -- switch NCM on/off
        run_NCM                     : in  STD_LOGIC;
        -- Receive command status
        receive_fault_empty         : out STD_LOGIC
    );  -- data out
end ncm_frame;


architecture Behavioral of ncm_frame is

	signal forceSoft                		: STD_LOGIC := '0';         -- this is the internal reset for all fifos & components
   signal halfProcessorClock       		: STD_LOGIC := '0';

   -- clock derived signals
   signal processor_tick           		: STD_LOGIC;                -- 10 us pulse
   
   -- temac controller signals
   signal tx_fifo_data             		: STD_LOGIC_VECTOR(35 downto 0) := (others => '0');
   signal tx_fifo_en               		: STD_LOGIC := '0';
   signal rx_fifo_data             		: STD_LOGIC_VECTOR(35 downto 0) := (others => '0');
   signal rx_fifo_en               		: STD_LOGIC := '1';

   -- program rom interface
   signal progrom_ncm_address      		: STD_LOGIC_VECTOR(7 downto 0) := (others => '0');
   signal progrom_ncm_data         		: STD_LOGIC_VECTOR(31 downto 0);
   
   -- config rom interface
   signal cfgrom_ncm_address       		: STD_LOGIC_VECTOR(7 downto 0) := (others => '0');
   signal cfgrom_ncm_data          		: STD_LOGIC_VECTOR(31 downto 0);

   -- config rom queue interface
	signal cfgrom_queue_wen_create   	: STD_LOGIC;
	signal cfgrom_queue_wen_receive     : STD_LOGIC;
	signal cfgrom_queue_ncm_wen      	: STD_LOGIC;
	signal cfgrom_queue_wen_crt    		: STD_LOGIC;
	signal cfgrom_queue_wen_rcv   		: STD_LOGIC;
	signal cfgrom_queue_update_from_crt : STD_LOGIC_VECTOR(31 downto 0);
	signal cfgrom_queue_update_from_rcv : STD_LOGIC_VECTOR(31 downto 0);
	signal data_queue_update_from_ncm   : STD_LOGIC_VECTOR(31 downto 0);
	signal cfgrom_queue_data_to_ncm  	: STD_LOGIC_VECTOR(31 downto 0);	
   
	-- varram signals
   signal varram_ncm_address       		: STD_LOGIC_VECTOR(15 downto 0) := (others => '0');
   signal varram_ncm_din           		: STD_LOGIC_VECTOR(31 downto 0) := (others => '0');
   signal varram_ncm_dout          		: STD_LOGIC_VECTOR(31 downto 0);
   signal varram_ncm_wen           		: STD_LOGIC := '0';
   
	-- create command interface
   signal createcmd_start          		: STD_LOGIC := '0';
   signal createcmd_rdy            		: STD_LOGIC;
   signal createcmd_varnum         		: STD_LOGIC_VECTOR(7 downto 0) := (others => '0');
   signal createcmd_dlen           		: STD_LOGIC_VECTOR(15 downto 0);
   signal createcmd_cfgaddress     		: STD_LOGIC_VECTOR(7 downto 0) := (others => '0');
   signal createcmd_cfgdata        		: STD_LOGIC_VECTOR(31 downto 0);
   signal createcmd_varaddress     		: STD_LOGIC_VECTOR(15 downto 0) := (others => '0');
   signal createcmd_vardata        		: STD_LOGIC_VECTOR(31 downto 0);
   
   signal createcmd_fifo_rst       		: STD_LOGIC;
   signal createcmd_fifo_en        		: STD_LOGIC;
   signal createcmd_fifo_data      		: STD_LOGIC_VECTOR(31 downto 0);

   -- send fifo interface
   signal sendfifo_empty           		: STD_LOGIC;

   -- send command interface
   signal sendcmd_start            		: STD_LOGIC;
   signal sendcmd_rdy              		: STD_LOGIC;
   signal sendcmd_channel          		: STD_LOGIC_VECTOR(7 downto 0);
   signal sendcmd_macaddress       		: STD_LOGIC_VECTOR(47 downto 0);
   signal sendcmd_dlen             		: STD_LOGIC_VECTOR(15 downto 0);
   signal sendcmd_sync             		: STD_LOGIC;
   signal send_mode             	  		: STD_LOGIC;
   
   signal sendcmd_fifo_en          		: STD_LOGIC;
   signal sendcmd_fifo_data        		: STD_LOGIC_VECTOR(31 downto 0);
	
	signal start_ack							: STD_LOGIC;
	signal start_send_ack					: STD_LOGIC;
	signal receiving_sync					: STD_LOGIC;	
	signal snd_start							: STD_LOGIC;
	signal TTL_send							: STD_LOGIC_VECTOR(7 downto 0);
	signal varnum_to_send           		: STD_LOGIC_VECTOR(7 downto 0);
	
   -- auto receiver interface
   signal autoreceiver_active      		: STD_LOGIC;
   signal autoreceiver_busy        		: STD_LOGIC;
   signal autoreceiver_sync        		: STD_LOGIC;
   signal autoreceiver_channel     		: STD_LOGIC_VECTOR(3 downto 0);
   signal autoreceiver_en          		: STD_LOGIC;
   signal autoreceiver_data        		: STD_LOGIC_VECTOR(31 downto 0);
   signal autoreceiver_reset       		: STD_LOGIC;
   signal rx_fifo_data_valid       		: STD_LOGIC;
   signal rx_fifo_empty       	  		: STD_LOGIC;
   signal autorec_start            		: STD_LOGIC;
   signal autorec_start_rxclk      		: STD_LOGIC;
   signal autoreceiver_sync_rxclk   	: STD_LOGIC;
	signal reset_mac_rx_fifo 				: STD_LOGIC;
   
   -- rcv_fifo interface
   signal rcv_fifo_empty           		: STD_LOGIC_VECTOR(15 downto 0);
	signal rcv_fifo_reset					: STD_LOGIC;
   
   -- rcv command interface
   signal rcvcmd_start             		: STD_LOGIC;
   signal rcvcmd_rdy               		: STD_LOGIC;
   signal rcvcmd_fault             		: STD_LOGIC;
   signal rcvcmd_varnum            		: STD_LOGIC_VECTOR(7 downto 0);
   signal rcvcmd_channel           		: STD_LOGIC_VECTOR(7 downto 0);
   signal rcvcmd_offset            		: STD_LOGIC_VECTOR(7 downto 0);
   signal rcvcmd_cfgaddress        		: STD_LOGIC_VECTOR(7 downto 0);
   signal rcvcmd_cfgdata           		: STD_LOGIC_VECTOR(31 downto 0);
   signal rcvcmd_varaddress        		: STD_LOGIC_VECTOR(15 downto 0);
   signal rcvcmd_vardata           		: STD_LOGIC_VECTOR(31 downto 0);
   signal rcvcmd_var_wen           		: STD_LOGIC;
   signal rcvcmd_fifo_en           		: STD_LOGIC;
   signal rcvcmd_fifo_empty        		: STD_LOGIC;
   signal rcvcmd_fifo_reset        		: STD_LOGIC;
   signal rcvcmd_fifo_channel      		: STD_LOGIC_VECTOR(3 downto 0);
   signal rcvcmd_fifo_data         		: STD_LOGIC_VECTOR(31 downto 0);
	signal wr_ack_from_rcvfifo 							: STD_LOGIC;
   
   -- future command interface
   signal future_reset             		: STD_LOGIC;
   signal future_set               		: STD_LOGIC;
   signal future_timetowait        		: STD_LOGIC_VECTOR(15 downto 0);
   signal future_address           		: STD_LOGIC_VECTOR(7 downto 0);
   signal future_ack               		: STD_LOGIC;
   signal future_int               		: STD_LOGIC;
   signal future_vector            		: STD_LOGIC_VECTOR(7 downto 0);
   
   -- branch command interface
   signal branch_start             		: STD_LOGIC;
   signal branch_rdy               		: STD_LOGIC;
   signal branch_guard             		: STD_LOGIC_VECTOR(19 downto 0);
   signal branch_result            		: STD_LOGIC;
   signal branch_stopack           		: STD_LOGIC;
   signal branchcmd_cfgaddress     		: STD_LOGIC_VECTOR(7 downto 0) := (others => '0');
   signal branchcmd_cfgdata        		: STD_LOGIC_VECTOR(31 downto 0);   
   signal branchcmd_varaddress     		: STD_LOGIC_VECTOR(15 downto 0) := (others => '0');
   signal branchcmd_vardata        		: STD_LOGIC_VECTOR(31 downto 0);
   
   -- controller interface
   signal controller_counter       		: STD_LOGIC_VECTOR(15 downto 0);
   signal controller_state         		: STD_LOGIC_VECTOR(4 downto 0);
   signal sync_to_pulse            		: STD_LOGIC;
   signal pclock_rst             		: STD_LOGIC;
   signal sync_timeout						: STD_LOGIC;
   
   -- status flags
   signal createcmd_busy           		: STD_LOGIC;
   signal sendcmd_busy             		: STD_LOGIC;
   signal rcvcmd_busy              		: STD_LOGIC;
   signal branchcmd_busy           		: STD_LOGIC;
   signal status_flags             		: STD_LOGIC_VECTOR(15 downto 0) := (others => '0');

	-- counters

	signal master_sync_sent_cnt 			: STD_LOGIC_VECTOR (31 downto 0);
	signal ack_sync_sent_cnt 				: STD_LOGIC_VECTOR (31 downto 0);
	signal data_frames_sent_cnt 			: STD_LOGIC_VECTOR (31 downto 0);
	signal autorec_goodcnt_sync 			: STD_LOGIC_VECTOR (31 downto 0);
	signal autorec_goodcnt_data 			: STD_LOGIC_VECTOR (31 downto 0);
	signal autorec_badcnt_data 			: STD_LOGIC_VECTOR (31 downto 0);
	signal autorec_nobroadcst 				: STD_LOGIC_VECTOR (31 downto 0);
	signal created_frames_cnt 				: STD_LOGIC_VECTOR (31 downto 0);
	signal rcv_cmd_fault_cnt 				: STD_LOGIC_VECTOR (31 downto 0);
	signal rcv_cmd_ok_cnt 					: STD_LOGIC_VECTOR (31 downto 0);
	signal sync_timeout_cnt 				: STD_LOGIC_VECTOR (31 downto 0);
	signal expected_sync_ok_cnt			: STD_LOGIC_VECTOR (31 downto 0);
	signal total_sync_cnt					: STD_LOGIC_VECTOR (31 downto 0);
	signal master_sync_cnt					: STD_LOGIC_VECTOR (31 downto 0);
	signal slave_sync_cnt					: STD_LOGIC_VECTOR (31 downto 0);
	signal rx_goodframes_RT_data_cnt  	: STD_LOGIC_VECTOR (31 downto 0);
	signal rx_goodframes_RT_sync_cnt  	: STD_LOGIC_VECTOR (31 downto 0);
	signal rx_badframes_RT_data_cnt 		: STD_LOGIC_VECTOR (31 downto 0);
	signal rx_badframes_RT_sync_cnt 		: STD_LOGIC_VECTOR (31 downto 0);
	signal rx_STP_frames_cnt 				: STD_LOGIC_VECTOR (31 downto 0);
	signal rx_bytes_RT_cnt   				: STD_LOGIC_VECTOR (63 downto 0);
	signal rx_goodframes_non_RT_cnt  	: STD_LOGIC_VECTOR (31 downto 0);
	signal rx_badframes_non_RT_cnt 		: STD_LOGIC_VECTOR (31 downto 0);
	signal rx_bytes_non_RT_cnt   			: STD_LOGIC_VECTOR (63 downto 0);
	signal rx_good_frames_error_cnt  	: STD_LOGIC_VECTOR (31 downto 0);
	signal rx_total_RT_data_frames_cnt  : STD_LOGIC_VECTOR (31 downto 0);
	signal rx_total_RT_sync_frames_cnt  : STD_LOGIC_VECTOR (31 downto 0);
	signal rx_total_RT_ack_frames_cnt  	: STD_LOGIC_VECTOR (31 downto 0);
	signal rx_total_nonRT_frames_cnt   	: STD_LOGIC_VECTOR (31 downto 0);
	signal rx_goodframes_total_cnt   	: STD_LOGIC_VECTOR (31 downto 0);
	signal rx_badframes_total_cnt   		: STD_LOGIC_VECTOR (31 downto 0);
	signal tx_fifo_overflow_cnt   		: STD_LOGIC_VECTOR (31 downto 0);
	signal bytes_read_from_txfifo_cnt	: STD_LOGIC_VECTOR (31 downto 0);
	signal running_time_tx_cnt   			: STD_LOGIC_VECTOR (31 downto 0);
	signal total_tx_bytes_cnt   			: STD_LOGIC_VECTOR (31 downto 0);
	signal running_time_rx_cnt   			: STD_LOGIC_VECTOR (31 downto 0);
	signal total_rx_bytes_cnt   			: STD_LOGIC_VECTOR (31 downto 0);
   ---------------------------------------------
   
   
   

begin

   halfProcessorClock <= not halfProcessorClock when (rising_edge(clk_ncm) and processor_tick = '1');

   -------------------------------------------------------------------

   ncm_shouldstopeval <= branch_stopack;
   
   status_flags(STATUS_RCV_FAULT_POS)      <= rcvcmd_fault;
   status_flags(STATUS_SYNC_TO_POS)        <= sync_timeout;
   status_flags(STATUS_USERBIT7_POS)       <= ncm_user(7);
   status_flags(STATUS_USERBIT6_POS)       <= ncm_user(6);
   status_flags(STATUS_USERBIT5_POS)       <= ncm_user(5);
   status_flags(STATUS_USERBIT4_POS)       <= ncm_user(4);
   status_flags(STATUS_USERBIT3_POS)       <= ncm_user(3);
   status_flags(STATUS_USERBIT2_POS)       <= ncm_user(2);
   status_flags(STATUS_USERBIT1_POS)       <= ncm_user(1);
   status_flags(STATUS_USERBIT0_POS)       <= ncm_user(0);

   pclock_rst <= autoreceiver_sync;

   -- change MAC address due to mode
   sendcmd_macaddress <= mac_high & mac_low;

   -- system ticks
   tick_100kHz : open_prescaler 
      port map (
         clk 		=> clk_ncm,
         rst 	=> '0', --pclock_rst,     -- run always
         en 		=> '1',
         pulse 	=> processor_tick,
         divider 	=> pclock_divider
      );

   -- program rom
    progrom0 : generic_ram_dual_port generic map (
        DATA_WIDTH => 32,
        ADDR_WIDTH => 8,
        TOTAL_ELEM => 256,
        INIT_FILE  => PROG_MEM_INIT_FILE
    )
    port map (
        p1_clk      => clk_ncm,
        p2_clk      => clk_ext_bus,
        p2_addr     => progrom_bus_address,
        p2_data_in  => progrom_bus_din,
        p2_data_out => progrom_bus_dout,
        p2_we       => progrom_bus_wen,
        p1_addr     => progrom_ncm_address,
        p1_data_out => progrom_ncm_data
    );

    -- config rom
    cfgrom0 : generic_ram_dual_port generic map (
        DATA_WIDTH => 32,
        ADDR_WIDTH => 8,
        TOTAL_ELEM => 256,
        INIT_FILE  => CONF_MEM_INIT_FILE
    )
    port map (
        p1_clk      => clk_ncm,
        p2_clk      => clk_ext_bus,
        p2_addr     => cfgrom_bus_address,
        p2_data_in  => cfgrom_bus_din,
        p2_data_out => cfgrom_bus_dout,
        p2_we       => cfgrom_bus_wen,
        p1_addr     => cfgrom_ncm_address,
        p1_data_out => cfgrom_ncm_data
    );
	
	-------ADDED FOR QUEUE SUPPORT -----------
	
	-- queue configuration of variable space
   -- works as an extension of cfg_rom
	cfgrom_queue : config_rom_queue port map (
      clk_ncm 						=> clk_ncm,
      clk_ext_bus					=> clk_ext_bus,
      bus_address 				=> cfgrom_queue_bus_address,
      bus_data_in 				=> cfgrom_queue_bus_din,
      bus_data_out 				=> cfgrom_queue_bus_dout,
      bus_rw 						=> cfgrom_queue_bus_wen,
      
		ncm_var_num 				=> cfgrom_ncm_address,
      ncm_cfg_queue_data_out 	=> cfgrom_queue_data_to_ncm,
		ncm_cfg_queue_update 	=> data_queue_update_from_ncm,
		ncm_rw 						=> cfgrom_queue_ncm_wen
		
   );
	
	-- write enable for config queue from NCM
	cfgrom_queue_ncm_wen <= cfgrom_queue_wen_crt or cfgrom_queue_wen_rcv;
	
	-- data input selection for config queue from NCM
	mux_queue: mux_cfgrom_queue port map ( 
		 write_back_data_in_from_create 	=> cfgrom_queue_update_from_crt,
       write_back_data_in_from_receive => cfgrom_queue_update_from_rcv,
		 create_busy 							=>  createcmd_busy,
		 receive_busy 							=>  rcvcmd_busy,
		 write_back_data_out 				=>  data_queue_update_from_ncm
	);
   ---------------------------------------------------------
	---------------------------------------------------------
	
   -- variable ram 
   varram0 : variable_ram port map (
      clk_ncm 					=> clk_ncm,
      clk_ext_bus 			=> clk_app,
		bus_address 			=> varram_app_addr,
      bus_data_in 			=> varram_app_din,
      bus_data_out 			=> varram_app_dout,
      bus_rw 					=> varram_app_wen,
      ncm_address 			=> varram_ncm_address,
      ncm_data_in 			=> varram_ncm_din, 
      ncm_data_out 			=> varram_ncm_dout,
      ncm_rw 					=> varram_ncm_wen 
   );

   -- create cmd
   createcmd0 : create_cmd port map (
      clk 								=> clk_ncm,
      start 							=> createcmd_start,
      rdy 								=> createcmd_rdy,
      varnum 							=> createcmd_varnum,
      datalen 							=> createcmd_dlen,
      cfgaddress 						=> createcmd_cfgaddress,
      cfgdata 							=> createcmd_cfgdata,
		
		-- added for sending var id on transmited packet
		varnum_to_send 				=> varnum_to_send,
		
      varaddress 						=> createcmd_varaddress,
      vardata 							=> createcmd_vardata,
      fifo_rst 						=> createcmd_fifo_rst,
      fifo_en 							=> createcmd_fifo_en,
      fifo_data 						=> createcmd_fifo_data,

		-- signals added for queue support
		cfgdata_queue 					=> cfgrom_queue_data_to_ncm,	
		cfgdata_queue_write_back 	=> cfgrom_queue_update_from_crt,
		cfgdata_queue_wen 			=> cfgrom_queue_wen_crt,
		
      created_frames_cnt 			=> created_frames_cnt		
      );
      
   -- create cmd status register
   createcmd_state : RS_FF port map (
      clk 	=> clk_ncm,
      reset => forceSoft,
      R 		=> createcmd_rdy,
      S 		=> createcmd_start,
      Q 		=> createcmd_busy
   );
   
   -- send fifo
   sndfifo0 : send_fifo port map (
      clk		=> clk_ncm,
      w_en 		=> createcmd_fifo_en,
      r_en 		=> sendcmd_fifo_en,
      reset 	=> createcmd_fifo_rst,
      data_in 	=> createcmd_fifo_data,
      empty 	=> sendfifo_empty,
      full 		=> open,
      data_out => sendcmd_fifo_data
   );

	snd_start <= sendcmd_start or (start_ack and sync_ack_en);

	sndcmd0 : send_cmd port map (
      clk 							=> clk_ncm,
      start 						=> snd_start,
		receiving_sync				=> receiving_sync,
      rdy 							=> sendcmd_rdy,
      datalen 						=> createcmd_dlen, --sendcmd_dlen,
      channel 						=> sendcmd_channel,
      macaddress 					=> sendcmd_macaddress,
      BE_traffic_mode			=> send_mode,
		BE_type						=> BE_type,
		NCData_type					=> NCData_type,
		NCSync_type					=> NCSync_type,
		NCAck_type					=> NCAck_type,
		sync 							=> sendcmd_sync,
		varnum 						=> varnum_to_send,
		TTL							=> TTL_send,
		
      fifo_data 					=> sendcmd_fifo_data,
      fifo_empty 					=> sendfifo_empty,
      fifo_en 						=> sendcmd_fifo_en,
      tx_en 						=> tx_fifo_en,
      tx_data 						=> tx_fifo_data,
		tx_full						=> '0',
		
		master_sync_sent_cnt 	=> master_sync_sent_cnt,
		ack_sync_sent_cnt 		=> ack_sync_sent_cnt,
		data_frames_sent_cnt 	=> data_frames_sent_cnt	
   );

   -- send cmd status register
   sendcmd_state : RS_FF port map (
      clk 		=> clk_ncm,
      reset 	=> forceSoft,
      R 			=> sendcmd_rdy,
      S 			=> sendcmd_start,
      Q 			=> sendcmd_busy
   );

	-- ethernet controller
	emac0: temac_wrapper 
	port map(
		  gmii_rx_clk        						=> gmii_rx_clk,
        gmii_rxd           						=> gmii_rxd,
        gmii_rx_dv          						=> gmii_rx_dv,
        gmii_rx_er          						=> gmii_rx_er,
		  gmii_tx_clk           					=>	gmii_tx_clk,
        gmii_txd           						=> gmii_txd,
        gmii_tx_en          						=> gmii_tx_en,
        gmii_tx_er          						=> gmii_tx_er,

		  run_NCM										=> run_NCM,
		  control_reg_hit								=> control_reg_hit,
		  
		  reset_rx_fifo								=> reset_mac_rx_fifo,
		  reset                  					=>	sys_rst,
		  sys_clk										=> clk_ncm,

       tx_fifo_data 									=> tx_fifo_data,
       tx_fifo_wren 									=> tx_fifo_en,
       -- rx fifo interface
       rx_fifo_data 									=> rx_fifo_data,
       rx_fifo_rden 									=> rx_fifo_en,
		 rx_fifo_data_valid 							=> rx_fifo_data_valid,
		 rx_fifo_empty									=> rx_fifo_empty,
		 autorec_start    							=> autorec_start_rxclk,
		 start_send_ack								=> start_send_ack,
		 receiving_sync								=> receiving_sync,
		 
		 NCData_type									=> NCData_type,
		 NCSync_type									=> NCSync_type,
		 NCAck_type										=> NCAck_type,
		  
		  rx_goodframes_RT_data_cnt   			=> rx_goodframes_RT_data_cnt,
		  rx_goodframes_RT_sync_cnt   			=> rx_goodframes_RT_sync_cnt,
		  rx_badframes_RT_data_cnt 				=> rx_badframes_RT_data_cnt,
		  rx_badframes_RT_sync_cnt 				=> rx_badframes_RT_sync_cnt,
		  rx_bytes_RT_cnt   							=> rx_bytes_RT_cnt,
	  
		  rx_goodframes_non_RT_cnt   				=> rx_goodframes_non_RT_cnt,
		  rx_badframes_non_RT_cnt 					=> rx_badframes_non_RT_cnt,
		  rx_bytes_non_RT_cnt   					=> rx_bytes_non_RT_cnt,

			running_time_tx_cnt(31 downto 0) 	=> running_time_tx_cnt,
			running_time_tx_cnt(63 downto 32) 	=> open,
			total_tx_bytes_cnt (31 downto 0)	 	=> total_tx_bytes_cnt,
			total_tx_bytes_cnt (63 downto 32) 	=> open,		  

			running_time_rx_cnt(31 downto 0) 	=> running_time_rx_cnt,
			running_time_rx_cnt(63 downto 32) 	=> open,
			total_rx_bytes_cnt (31 downto 0)		=> total_rx_bytes_cnt,
			total_rx_bytes_cnt (63 downto 32) 	=> open,
			tx_fifo_overflow_cnt 					=> tx_fifo_overflow_cnt,
			rx_good_frames_error_cnt 				=> rx_good_frames_error_cnt,
			rx_total_RT_data_frames_cnt 			=> rx_total_RT_data_frames_cnt,
			rx_total_RT_sync_frames_cnt 			=> rx_total_RT_sync_frames_cnt,
			rx_total_RT_ack_frames_cnt 			=> rx_total_RT_ack_frames_cnt,
			rx_total_nonRT_frames_cnt 				=> rx_total_nonRT_frames_cnt,
			rx_goodframes_total_cnt 				=> rx_goodframes_total_cnt,
			rx_badframes_total_cnt 					=> rx_badframes_total_cnt,
			bytes_read_from_txfifo_cnt 			=> bytes_read_from_txfifo_cnt
		  );

	 counters0 : counters port map (
			clk_ncm  							=> clk_ncm,
	      clk_bus 								=> clk_ext_bus,

         bus_address 						=> counters_bus_address,
         bus_data_in 						=> counters_bus_din,
         bus_data_out 						=> counters_bus_dout,
         bus_rw 								=> '0',			
			ncm_rw								=> '1',

			created_frames_cnt 				=> created_frames_cnt,
			data_frames_sent_cnt 			=> data_frames_sent_cnt,
			master_sync_sent_cnt 			=> master_sync_sent_cnt,
			ack_sync_sent_cnt 				=> ack_sync_sent_cnt,
			rx_goodframes_RT_data_cnt 		=> rx_goodframes_RT_data_cnt,
			rx_goodframes_RT_sync_cnt 		=> rx_goodframes_RT_sync_cnt,
			rx_badframes_RT_data_cnt 		=> rx_badframes_RT_data_cnt,
			rx_badframes_RT_sync_cnt 		=> rx_badframes_RT_sync_cnt,
			rx_goodframes_non_RT_cnt 		=> rx_goodframes_non_RT_cnt,
			rx_badframes_non_RT_cnt 		=> rx_badframes_non_RT_cnt,
			rcv_cmd_fault_cnt 				=> rcv_cmd_fault_cnt,
			rcv_cmd_ok_cnt 					=> rcv_cmd_ok_cnt,
			sync_timeout_cnt			    => sync_timeout_cnt,
			expected_sync_ok_cnt 			=> expected_sync_ok_cnt,
			total_tx_bytes_cnt   			=> total_tx_bytes_cnt,
			running_time_tx_cnt  			=> running_time_tx_cnt,
			total_rx_bytes_cnt   			=> total_rx_bytes_cnt,
			running_time_rx_cnt  			=> running_time_rx_cnt,
			tx_fifo_overflow_cnt 			=> tx_fifo_overflow_cnt,
			rx_good_frames_error_cnt 		=> rx_good_frames_error_cnt,
			rx_total_RT_data_frames_cnt 	=> rx_total_RT_data_frames_cnt,
			rx_total_RT_sync_frames_cnt 	=> rx_total_RT_sync_frames_cnt,
			rx_total_RT_ack_frames_cnt 	=> rx_total_RT_ack_frames_cnt,
			rx_total_nonRT_frames_cnt 		=> rx_total_nonRT_frames_cnt,
			rx_goodframes_total_cnt 		=> rx_goodframes_total_cnt,
			rx_badframes_total_cnt 			=> rx_badframes_total_cnt,
			autorec_goodcnt_data 			=> autorec_goodcnt_data,
			autorec_badcnt_data 				=> autorec_badcnt_data,
			autorec_goodcnt_sync 			=> autorec_goodcnt_sync		,
			rx_RT_bytes_cnt 					=> rx_bytes_RT_cnt(31 downto 0),
            rx_bytes_non_rt_cnt             => rx_bytes_non_rt_cnt(31 downto 0),
			bytes_read_from_txfifo_cnt		=> bytes_read_from_txfifo_cnt	,
			total_sync_cnt						=> total_sync_cnt,
			master_sync_cnt					=> master_sync_cnt,
			slave_sync_cnt						=> slave_sync_cnt
   );

   autorec : auto_receiver Port map ( 
       clk 					=> clk_ncm, --gmii_rx_clk, 
       rst 					=> sys_rst,
       busy 				=> open,                 
       start				=> autorec_start,
		 reset_rx_fifo		=> reset_mac_rx_fifo,
		 -- emac interface
       rx_fifo_data 		=> rx_fifo_data,
       rx_fifo_en 		=> rx_fifo_en,
       rx_valid 			=> rx_fifo_data_valid,               -- end packet reception
		 rx_fifo_empty 	=> rx_fifo_empty,
       -- ncm interface to rcv_fifo
       ncm_sync 			=> autoreceiver_sync, --autoreceiver_sync_rxclk,
       ncm_rcv_channel 	=> autoreceiver_channel,
       ncm_rcv_en 		=> autoreceiver_en,
       ncm_rcv_data 		=> autoreceiver_data,
       ncm_rcv_reset 	=> autoreceiver_reset,
		 
		 NCData_type		=> NCData_type,
		 NCSync_type		=> NCSync_type,
       -- statistics interface
       goodcnt_sync 		=> autorec_goodcnt_sync,
       goodcnt_data 		=> autorec_goodcnt_data,
       badcnt_data 		=> autorec_badcnt_data,
       nobroadcst 		=> autorec_nobroadcst,
		 debugstate			=> open
       );

	sync_frame_pulse: pulse_synchronizer Port map (
		pulse_in_clkA 	=> autorec_start_rxclk,
		clkA 				=> gmii_rx_clk,
		pulse_out_clkB =>	autorec_start,
		clkB 				=> clk_ncm,
		reset_clkA 		=> sys_rst,
		reset_clkB 		=> '0'
		);
		
	sync_ack_pulse	: pulse_synchronizer Port map (
		pulse_in_clkA 	=> start_send_ack,
		clkA 				=> gmii_rx_clk,
		pulse_out_clkB =>	start_ack,
		clkB 				=> clk_ncm,
		reset_clkA 		=> sys_rst,
		reset_clkB 		=> '0'
		);

	rcv_fifo_reset <= (autoreceiver_reset and (not rcvcmd_busy)) or (rcvcmd_fifo_reset);
	
	rcvfifo : rcv_fifo Port map ( 
       rdclk 						=> clk_ncm,
       wrclk 						=> gmii_rx_clk,

       -- from auto receiver
       w_en 						=> autoreceiver_en,
       w_channel				 	=> autoreceiver_channel,
		 data_in 					=> autoreceiver_data,
       reset 						=> rcv_fifo_reset,
       empty_for_w 				=> open,
       full_for_w 				=> open,
       -- to receive_cmd
       empty_for_r 				=> rcvcmd_fifo_empty,
       full_for_r 				=> open,
       r_en 						=> rcvcmd_fifo_en,
       r_channel 					=> rcvcmd_fifo_channel,
       data_out 					=> rcvcmd_fifo_data,
       -- status output
       empty 						=> rcv_fifo_empty,
		 		 
		 rd_count 					=> open, 
		 wr_count 					=> open,
			  
       wr_ack   					=> wr_ack_from_rcvfifo, 
		 overflow 					=> open, 
		 valid   					=>  open,
		 underflow 					=> open
		 
       );

    rcvcmd0 : receive_cmd Port map(
        clk                         => clk_ncm,
        -- cmd interface
        start                       => rcvcmd_start,
        rdy                         => rcvcmd_rdy,
        fault                       => rcvcmd_fault,
        fault_empty                 => receive_fault_empty,
        varnum                      => rcvcmd_varnum,
        channel                     => rcvcmd_channel,
        offset                      => rcvcmd_offset,
        -- read config setup
        cfgaddress                  => rcvcmd_cfgaddress,
        cfgdata                     => rcvcmd_cfgdata,
        -- signals added for queue support
        cfgdata_queue               => cfgrom_queue_data_to_ncm,
        cfgdata_queue_write_back    => cfgrom_queue_update_from_rcv,
        cfgdata_queue_wen           => cfgrom_queue_wen_rcv,
        -- copy variable
        varaddress                  => rcvcmd_varaddress,
        vardata                     => rcvcmd_vardata,
        var_wen                     => rcvcmd_var_wen,
        -- from fifo
        fifo_en                     => rcvcmd_fifo_en,
        fifo_empty                  => rcvcmd_fifo_empty,
        fifo_reset                  => rcvcmd_fifo_reset,
        fifo_channel                => rcvcmd_fifo_channel,
        fifo_data                   => rcvcmd_fifo_data,
        fifo_wr_ack                 => wr_ack_from_rcvfifo,
        rcv_cmd_fault_cnt           => rcv_cmd_fault_cnt,
        rcv_cmd_ok_cnt              => rcv_cmd_ok_cnt
       );

   -- rcv cmd status register
   rcvcmd_state : RS_FF port map (
      clk 	=> clk_ncm,
      reset => forceSoft,
      R 		=> rcvcmd_rdy,
      S 		=> rcvcmd_start,
      Q 		=> rcvcmd_busy
   );

   -- future cmd
   futurecmd0 : future_cmd Port map ( 
       clk 			=> clk_ncm,
       reset 		=> future_reset,
       pulse 		=> processor_tick,
       set 			=> future_set,
       timetowait => future_timetowait,
       address 	=> future_address,
       rdy 			=> future_ack,
       future_int => future_int,
       jumpto 		=> future_vector
       );
    
   -- branch cmd
   branchcmd0 : branchcmd Port map ( 
       clk 				=> clk_ncm,
       rst 				=> sys_rst,
       start 			=> branch_start,
       rdy 				=> branch_rdy,
       result 			=> branch_result,
       guard 			=> branch_guard,
       rcvbufempty 	=> rcv_fifo_empty,
       sndbufempty 	=> sendfifo_empty,
       counter 		=> controller_counter,
       status 			=> status_flags,
       stopack 		=> branch_stopack,
       cfgaddress 	=> branchcmd_cfgaddress,
       cfgdata 		=> branchcmd_cfgdata,
		 cfgdata_queue => cfgrom_queue_data_to_ncm,
       varaddress 	=> branchcmd_varaddress,
		 vardata 		=> branchcmd_vardata
       );

   -- branch cmd status register
   branchcmd_state : RS_FF port map (
      clk 		=> clk_ncm,
      reset 	=> forceSoft,
      R 			=> branch_rdy,
      S 			=> branch_start,
      Q 			=> branchcmd_busy
   );
 
   ---------------------------------------------------------------------------------
   -- into cfgrom
   cfgrom_mux : cfgrom_ctrl Port map ( 
      create_address 	=> createcmd_cfgaddress,
      rcv_address 		=> rcvcmd_cfgaddress,
      branch_address 	=> branchcmd_cfgaddress,
      create_busy			=> createcmd_busy,
      rcv_busy 				=> rcvcmd_busy,
      branch_busy 			=> branchcmd_busy,
      cfg_address 		=> cfgrom_ncm_address
      );
                         
   -- out of cfgrom
   createcmd_cfgdata  <= cfgrom_ncm_data;
   rcvcmd_cfgdata     <= cfgrom_ncm_data;
   branchcmd_cfgdata  <= cfgrom_ncm_data;
   -- out of varram   
   createcmd_vardata  <= varram_ncm_dout;
   branchcmd_vardata  <= varram_ncm_dout;
   
   -- into varram
   varram_mux : varram_ctrl Port map ( 
      create_busy 			=> createcmd_busy,
      rcv_busy 				=> rcvcmd_busy,
      branch_busy 			=> branchcmd_busy,
      create_address 	=> createcmd_varaddress,
      rcv_address 		=> rcvcmd_varaddress,
      branch_address 	=> branchcmd_varaddress,
      rcv_wen 				=> rcvcmd_var_wen,
      rcv_din 				=> rcvcmd_vardata,
      varram_address 	=> varram_ncm_address,
      varram_wen 			=> varram_ncm_wen,
      varram_din 			=> varram_ncm_din
      );

   ---------------------------------------------------------------------------------
   thecontroller : NCM_controller Port map ( 
      clk 								=> clk_ncm,
      rst	 							=> sys_rst,
      run 								=> run_NCM,   
      controller_clock 				=> processor_tick,
      softmode 						=> open,         
      controlstate 					=> controller_state,
      app_sync_pulse                => app_sync_pulse,
      createcmd_busy					=> createcmd_busy,
      sendcmd_busy 					=> sendcmd_busy,
      rcvcmd_busy 					=> rcvcmd_busy,
      branch_busy 						=> branchcmd_busy,
      start_create 					=> createcmd_start,
      createcmd_varnum 				=> createcmd_varnum,
      createcmd_dlen 				=> createcmd_dlen,
      createcmd_rdy 					=> '0', --createcmd_rdy,
      sendfifo_empty 				=> sendfifo_empty,
      start_send 						=> sendcmd_start,
      sendcmd_sync 					=> sendcmd_sync,
      sendcmd_channel 				=> sendcmd_channel,
      sendcmd_dlen 					=> sendcmd_dlen,
		TTL								=> TTL_send,
		sendcmd_BE_mode				=> send_mode,
      sync_received 					=> autoreceiver_sync,
      sync_timeout 					=> sync_timeout,
      sync_timeout_pulse 			=> sync_to_pulse,
		sync_timeout_cnt				=> sync_timeout_cnt,
		expected_sync_ok_cnt			=> expected_sync_ok_cnt,
		total_sync_cnt					=> total_sync_cnt,
		master_sync_cnt				=> master_sync_cnt,
		slave_sync_cnt					=> slave_sync_cnt,
      rcv_start 						=> rcvcmd_start,
      rcv_varnum 						=> rcvcmd_varnum,
      rcv_channel 					=> rcvcmd_channel,
      rcv_offset 						=> rcvcmd_offset,
      rcv_fault 						=> rcvcmd_fault,
      rcv_rdy 							=> rcvcmd_rdy,
      future_reset 					=> future_reset,
      future_set 						=> future_set,
      future_timetowait 			=> future_timetowait,
      future_address 				=> future_address,
      future_ack 						=> future_ack,
      future_int 						=> future_int,
      future_vector					=> future_vector,
      branch_start 					=> branch_start,
      branch_guard 					=> branch_guard,
      branch_count					=> controller_counter,
      branch_result 					=> branch_result,
      branch_rdy 						=> branch_rdy,       
      mode_switch 					=> open,
      mode_delay 						=> open,
      ncm_data 						=> progrom_ncm_data,
      ncm_addr 						=> progrom_ncm_address
      );
   
end Behavioral;