`timescale 1ns / 1ps


module uart_mem_manager (
        clk,
        // Control and status
        reset,
        busy,
        // Memories interface
        all_mems_addr,
        all_mems_data_in,
        mem0_we,
        mem0_data_out,
        mem1_we,
        mem1_data_out,
        mem2_we,
        mem2_data_out,
        // Serial signals
        PIN_UartRx,
        PIN_UartTx
    );


// ---------- INCLUDES ----------


// ---------- PARAMETERS ----------
    parameter       IN_CLK_FR      = 100000000;
    parameter       UART_BAUD_RATE = 9600;
    parameter [7:0] MY_STATION_ID  = 1;


// ---------- LOCAL PARAMETERS ----------


// ---------- INPUTS AND OUTPUTS ----------
    input  wire clk;
    // Control and status
    input  wire        reset;
    output reg         busy  = 0;
    // Memories interface
    output wire [7:0]  all_mems_addr;
    output wire [31:0] all_mems_data_in;
    output wire        mem0_we;
    input  wire [31:0] mem0_data_out;
    output wire        mem1_we;
    input  wire [31:0] mem1_data_out;
    output wire        mem2_we;
    input  wire [31:0] mem2_data_out;
    // Serial signals
    input  wire        PIN_UartRx;
    output wire        PIN_UartTx;


// ---------- MODULE ----------


    // - Payload buffer: Two bi-dir ports
    reg  [7:0]  buffer_bram [0:1499];
    // Port 1
    reg [10:0] payload_buff_addr_1;
    reg        payload_buff_we_1;
    reg [7:0]  payload_buff_data_in_1;
    reg [7:0]  payload_buff_data_out_1;
    // Port 2 (handled by NCM_mem_manager module)
    wire [10:0] payload_buff_addr_2;
    wire        payload_buff_we_2;
    wire [7:0]  payload_buff_data_in_2;
    reg  [7:0]  payload_buff_data_out_2;
    
    always @( posedge clk ) begin
        // - Port 1 -
        if( payload_buff_we_1 ) // write
            buffer_bram[payload_buff_addr_1] <= payload_buff_data_in_1;
        //  read
        payload_buff_data_out_1 <= buffer_bram[payload_buff_addr_1];
        // - Port 2 -
        if( payload_buff_we_2 ) // write
            buffer_bram[payload_buff_addr_2] <= payload_buff_data_in_2;
        //  read
        payload_buff_data_out_2 <= buffer_bram[payload_buff_addr_2];
    end
    
    
    // -- UART reception unit
    wire       uart_rx_byte_ready;
    wire [7:0] uart_rx_byte;
    uart_rx #(
        .IN_CLK_FR (IN_CLK_FR),
        .BAUD_RATE (UART_BAUD_RATE)
    )
    uart_rx_1 (
        .clk        (clk), 
        .PIN_RX     (PIN_UartRx), 
        .word       (uart_rx_byte), 
        .word_ready (uart_rx_byte_ready)
    );
    
    // -- UART transmission unit
    reg  [7:0] uart_tx_byte  = 0;
    reg        uart_tx_start = 0;
    wire       uart_tx_busy;
    uart_tx #(
        .IN_CLK_FR (IN_CLK_FR),
        .BAUD_RATE (UART_BAUD_RATE)
    )
    uart_tx_1 (
        .clk      (clk),
        .PIN_TX   (PIN_UartTx),
        .word     (uart_tx_byte),
        .tx_start (uart_tx_start),
        .busy     (uart_tx_busy)
    );
    
    // -- Control FSM
    reg [1:0] FSM_state        = 0;
    reg       buffer_mux_ctrl  = 0;
    reg       uart_frame_ready = 0;
    wire      mem_manager_busy;
    localparam ADDR_LIM       = 3 + 256*4; // header + 256 words of 32 bits

    always @( * ) begin
        payload_buff_we_1      <= (buffer_mux_ctrl)? (uart_rx_byte_ready) : (1'b0);
        payload_buff_data_in_1 <= uart_rx_byte;
    end

    always @( posedge clk ) begin
    
        if( reset ) begin
            busy                <= 0;
            uart_frame_ready    <= 0;
            buffer_mux_ctrl     <= 0;
            payload_buff_addr_1 <= 0;
            FSM_state           <= 0;
        end
        else begin
            // State 0: IDLE
            if( FSM_state == 0 ) begin
                payload_buff_addr_1 <= 0;
                uart_frame_ready    <= 0;
                if( (uart_rx_byte_ready == 1) && (uart_rx_byte == 8'd129) ) begin
                    busy            <= 1;
                    buffer_mux_ctrl <= 1;
                    FSM_state       <= 1;
                end
                else begin
                    busy            <= 0;
                    buffer_mux_ctrl <= 0;
                end
            end
            // State 1: Receive
            if( FSM_state == 1 ) begin
                if( payload_buff_addr_1 != ADDR_LIM ) begin
                    if( uart_rx_byte_ready == 1 ) begin
                        payload_buff_addr_1 <= payload_buff_addr_1 + 1'b1;
                    end
                end
                else begin
                    buffer_mux_ctrl     <= 0;
                    payload_buff_addr_1 <= 0;
                    uart_frame_ready    <= 1;
                    FSM_state           <= 2;
                end
            end
            // State 2: Wait operation
            if( FSM_state == 2 ) begin
                uart_frame_ready <= 0;
                if( mem_manager_busy == 1 ) begin
                    FSM_state <= 3;
                end
            end
            // State 3: Send response
            if( FSM_state == 3 ) begin
                if( (uart_tx_busy == 0) && (uart_tx_start == 0) ) begin
                    uart_tx_byte  <= payload_buff_data_out_1;
                    if( payload_buff_addr_1 != ADDR_LIM ) begin
                        payload_buff_addr_1 <= payload_buff_addr_1 + 1'b1;
                        uart_tx_start       <= 1;
                    end
                    else
                        FSM_state <= 0;
                end
                else
                    uart_tx_start <= 0; 
            end // State
        
        end
    
    end


    NCM_mem_manager #(
        .MEMS_WORD_BITS (32),
        .MEMS_ADDR_BITS (8)
    )
    NCM_mem_manager_1 (
        .clk                (clk),
        .start_op           (uart_frame_ready),
        .busy               (mem_manager_busy),
        // Operation buffer (transmission payload)
        .op_buffer_addr     (payload_buff_addr_2),
        .op_buffer_we       (payload_buff_we_2),
        .op_buffer_data_in  (payload_buff_data_in_2),
        .op_buffer_data_out (payload_buff_data_out_2),
        //
        .all_mems_addr      (all_mems_addr),
        .all_mems_data_in   (all_mems_data_in),
        //
        .mem0_we       (mem0_we),
        .mem0_data_out (mem0_data_out),
        .mem1_we       (mem1_we),
        .mem1_data_out (mem1_data_out),
        .mem2_we       (mem2_we),
        .mem2_data_out (mem2_data_out)
    );


endmodule
