----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    22:11:55 12/28/2006 
-- Design Name:    NCM basic
-- Module Name:    auto_receiver - Behavioral 
-- Project Name:   NCM
-- Target Devices: 
-- Tool versions: 
-- Description:    receives packets from EMAC & decodes variables into receive channels
--                 en is used to turn on/off receivers
--                 receive channels are reset automatically
--                 sync packet is decoded and shown as pulse
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

library UNISIM;
use UNISIM.VComponents.all;

entity tx_counter is
    Port ( clk : in  STD_LOGIC;
			  rst : in std_logic;
           -- emac interface
           
			 gmac_tx_data : in std_logic_vector (7 downto 0);
			 gmac_tx_dvld : in std_logic;
			 gmac_tx_ack : in std_logic;
			 
			 nonRT_frame_counter	: out std_logic_vector (31 downto 0);
			 RT_data_frame_counter	: out std_logic_vector (31 downto 0);
			 RT_sync_frame_counter	: out std_logic_vector (31 downto 0);
			 RT_error_frame_counter	: out std_logic_vector (31 downto 0)
				
           );
end tx_counter;

architecture Behavioral of tx_counter is

   type states is (s_init, s_idle, s_wait, s_dest_src, s_type_h, s_type_l, s_rdy, s_wait_for_ack);
   signal currentstate : states := s_init;
	signal nonRT_frame_counter_i : std_logic_vector (31 downto 0);
	signal RT_data_frame_counter_i : std_logic_vector (31 downto 0);
	signal RT_sync_frame_counter_i : std_logic_vector (31 downto 0);
	signal RT_error_frame_counter_i : std_logic_vector (31 downto 0);
	
begin

nonRT_frame_counter <= nonRT_frame_counter_i;
RT_data_frame_counter <= RT_data_frame_counter_i;
RT_sync_frame_counter <= RT_sync_frame_counter_i;
RT_error_frame_counter <= RT_error_frame_counter_i;

   FSM : process(clk)
      variable nextstate : states := s_init;
      variable cnt : integer range 0 to 1500 := 0;
   begin

     if (rst = '1') then

        currentstate <= s_init;
         nextstate := s_init;
      
    elsif (rising_edge(clk)) then
        
		  nextstate := currentstate;
                  
         case currentstate is
     
            when s_init => 
               cnt := 0;
               nextstate := s_idle;
					nonRT_frame_counter_i <= (others => '0');
					RT_data_frame_counter_i <= (others => '0');
					RT_sync_frame_counter_i <= (others => '0');
					RT_error_frame_counter_i <= (others => '0');
					
               
            when s_idle =>
            
					cnt := 0;
               if (gmac_tx_dvld = '1') then        
                  cnt := cnt + 1;
						nextstate := s_wait_for_ack;
               end if;
					
            when s_wait_for_ack =>       -- dest must be broadcast - otherwise discard packet            
               
					if (gmac_tx_dvld = '0') then
							nextstate := s_rdy;
					elsif (gmac_tx_ack = '1') then
                     nextstate := s_dest_src;
               end if;
                              
            when s_dest_src =>
						if (gmac_tx_dvld = '0') then
							nextstate := s_rdy;
                  else
                     cnt := cnt + 1;
						   if (cnt = 12) then                   -- 6 bytes read
                           nextstate := s_type_h;
                           cnt := 0;
                     end if;
						end if;
                  
            when s_type_h =>        -- simply read src address
         
               	if (gmac_tx_dvld = '0') then
                     nextstate := s_rdy;
                     
                  else
                     if (gmac_tx_data = X"5C") then
									nextstate := s_type_l;
							else
									nonRT_frame_counter_i <= nonRT_frame_counter_i + 1;
									nextstate  := s_wait;
							end if;

						end if;
					
            when s_type_l =>        -- simply read src address
         
						if (gmac_tx_dvld = '0') then
                     nextstate := s_rdy;
                     
                  else
                     if (gmac_tx_data = X"E0") then
								RT_data_frame_counter_i <= RT_data_frame_counter_i + 1 ;
							elsif (gmac_tx_data = X"FF") then
								RT_sync_frame_counter_i <= RT_sync_frame_counter_i + 1 ;
							else
								RT_error_frame_counter_i <= RT_error_frame_counter_i +1;
							end if;
							
						nextstate := s_wait;

						end if;

            when s_wait =>        -- simply read src address
         
						if (gmac_tx_dvld = '0') then
                     nextstate := s_rdy;
						end if;
						
            when s_rdy =>        -- we made it
            
               nextstate := s_idle;

            when others =>
               nextstate := s_idle;       -- to be safe
            
         end case;

         currentstate <= nextstate;

      end if;
         
   end process;


end Behavioral;