----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    22:11:55 12/28/2006 
-- Design Name:    NCM basic
-- Module Name:    auto_receiver - Behavioral 
-- Project Name:   NCM
-- Target Devices: 
-- Tool versions: 
-- Description:    receives packets from EMAC & decodes variables into receive channels
--                 en is used to turn on/off receivers
--                 receive channels are reset automatically
--                 sync packet is decoded and shown as pulse
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

library UNISIM;
use UNISIM.VComponents.all;

entity input_class_delay is
    Port ( clk : in  STD_LOGIC;
			  rst : in std_logic;
           -- emac interface
           
gmac_rx_data : in std_logic_vector (7 downto 0);
gmac_rx_dvld : in std_logic;

rx_nonRT_frame_counter : out std_logic_vector (31 downto 0);
rx_RT_frame_counter : out std_logic_vector (31 downto 0);
rx_total_frame_counter : out std_logic_vector (31 downto 0)
           );
end input_class_delay;

architecture Behavioral of input_class_delay is

   type states is (s_init, s_idle, s_wait, s_dest, s_src, s_type_h, s_type_l, s_rdy);
   signal currentstate : states := s_init;
	signal rx_nonRT_frame_counter_i : std_logic_vector (31 downto 0);
	signal rx_RT_frame_counter_i : std_logic_vector (31 downto 0);
	signal rx_total_frame_counter_i : std_logic_vector (31 downto 0);


begin

rx_nonRT_frame_counter <= rx_nonRT_frame_counter_i;
rx_RT_frame_counter <= rx_RT_frame_counter_i;
rx_total_frame_counter <= rx_total_frame_counter_i;

   FSM : process(clk)
      variable nextstate : states := s_init;
      variable cnt : integer range 0 to 1500 := 0;
   begin

     if (rst = '1') then

        currentstate <= s_init;
         nextstate := s_init;
      
    elsif (rising_edge(clk)) then
        
		  nextstate := currentstate;
                  
         case currentstate is
     
            when s_init => 
					rx_nonRT_frame_counter_i <= (others => '0');
					rx_RT_frame_counter_i <= (others => '0');
               rx_total_frame_counter_i <= (others => '0');

					cnt := 0;
               nextstate := s_idle;

            when s_idle =>
            
					cnt := 0;
               if (gmac_rx_dvld = '1') then        
                  cnt := cnt + 1;
						nextstate := s_dest;
               end if;
					
            when s_dest =>       -- dest must be broadcast - otherwise discard packet            
               
					if (gmac_rx_dvld = '0') then
                     nextstate := s_wait;
                     
                  else
                     cnt := cnt + 1;

                     if (cnt = 6) then                   -- 6 bytes read
                           nextstate := s_src;
                           cnt := 0;
                     end if;
   
                  end if;
                              
            when s_src =>       -- dest must be broadcast - otherwise discard packet            
              -- RT_frame_marker <= '0';   
               
                  if (gmac_rx_dvld = '0') then
                     nextstate := s_rdy;
                     
                  else
                     cnt := cnt + 1;

                     if (cnt = 6) then                   -- 6 bytes read
                           nextstate := s_type_h;
                           cnt := 0;
                     end if;
   
                  end if;
                  
                  
            when s_type_h =>        -- simply read src address
         
               	if (gmac_rx_dvld = '0') then
                     nextstate := s_rdy;
                     
                  else
                     if (gmac_rx_data = X"5C") then
									nextstate := s_type_l;
							else
									rx_nonRT_frame_counter_i <= rx_nonRT_frame_counter_i +1;
									nextstate  := s_wait;
							end if;

						end if;
					
            when s_type_l =>        -- simply read src address
         
               
						if (gmac_rx_dvld = '0') then
                     nextstate := s_rdy;
                     
                  else
                     if (gmac_rx_data = X"E0" or gmac_rx_data = X"FF") then
									rx_RT_frame_counter_i <= rx_RT_frame_counter_i +1;
							end if;
							nextstate := s_wait;

						end if;

            when s_wait =>        -- simply read src address
         
						if (gmac_rx_dvld = '0') then
                     nextstate := s_rdy;
						end if;
						
            when s_rdy =>        -- we made it
            
               rx_total_frame_counter_i <= rx_total_frame_counter_i +1;
					nextstate := s_idle;

            when others =>
               nextstate := s_idle;       -- to be safe
            
         end case;

         currentstate <= nextstate;

      end if;
         
   end process;


end Behavioral;