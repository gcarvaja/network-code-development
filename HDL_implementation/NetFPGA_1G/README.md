# README #

### Folder structure ###

* switch__reference_NetFPGA: contains an unmodified version of the reference COTS switch design as provided in the NetFPGA-1G release 2.1.2. It works as a regular switch.

* switch__RT_no_ncp: contains a modified version of the reference switch design, incorporating the extensions for the dedicated real-time path as described in the Atacama framework. The extensions add the parallel real-time path without modifying the COTS fabric.

* dongle_emulator: contains an early design that emulates multiple real-time capable stations. The design can synthesize up-to four fully-functional independent NICs using a single NetFPGA boards. This design was used in early testing and experimental characterization of timing properties. After porting the NICs design to the newer Spartan-6 FPGAs, the dongle emulator entered to legacy state and is no longer supported.

* common: contains common HDL sources that are used on more than one of the previous projects to minimize redundancy in the source files.


### Synthesizing the designs and implementation issues ###

All the projects in this folder target the Virtex-2 chip included in the original NetFPGA-1G platform. This is currently considered legacy hardware and is no longer manufactured and or supported for recent version of the design tools. The latest version of Xilinx ISE that supported the Virtex-2 pro FPGAs is 10.1 SP1. To simplify the implementation process, each project folder includes a TCL script that builds the project hierarchy for the HDL sources and constraint files.

To create a new project based on the original sources follow these steps:

- Open ISE 10.1.3, and go to Project -> Source Control -> Import...
- In the pop-up menu, fill the corresponding fields with the corresponding TCL script and the Directory to import. The Directory to import MUST be the same directory where the original TCL file is located. For example:

Project file to import
D:\network-code-development\HDL_implementation\NetFPGA_1G\switch__RT_no_ncp\switch__RT_no_ncp_import.tcl

Directory to import
D:\network-code-development\HDL_implementation\NetFPGA_1G\switch__RT_no_ncp\

Failing in this configuration will generate errors during synthesis due to mismatched in the relative paths.

Even following the previous steps, you might get errors during synthesis related to missing files. These errors are known-bugs on ISE, which fails when parsing the hierarchy. For example, you may find an error like this:

ERROR:HDLCompilers:87 - "../common/../common/remove_pkt.v" line 219 Could not find module/primitive 'syncfifo_512x72'

If you check the hierarchy view, you will see the file 'syncfifo_512x72' outside the hierarchy of the top-level module. This bug can be solved opening (double click) the file that originates the error (in this case 'remove_pkt.v'), make a small modification, undoing it, and saving the file to update the file-system time-stamp, then pressing F5 to refresh the hierarchy view. Repeat this procedure for every problematic file.

Once all these issues are fixed, you can implement the design and generate the bit file. This process may take several minutes.

These procedure gives you the chance to check the source files and make modifications as desired.


### Using the bit files for hardware design ###

To program the NetFPGAs with the generated bitfiles you can use the tools provided in the NC_software folder or use Impact to program with a Xilinx JTAG programmer interface.
For convenience, there are release versions of the repository which includes synthesized and ready to run versions of the most recent hardware designs.
Remember that the NetFPGAs must be reprogrammed every time the boards are powered on.