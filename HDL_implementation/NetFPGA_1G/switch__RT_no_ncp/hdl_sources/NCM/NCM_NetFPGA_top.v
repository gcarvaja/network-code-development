module NCM_netFPGA_beta #( parameter CPCI_NF2_ADDR_WIDTH=32, 
									parameter CPCI_NF2_DATA_WIDTH=32 ) 
(
		// To MAC interface
		output  reg [8:0]           			tx_data_0,
		output  reg                         tx_wr_0,
		input                              	tx_rdy_0,

      input [8:0]                        	rx_data_from_MAC_0,
      input                        			rx_fifo_empty_from_MAC_0,
		input											RT_sync_marker_0,
		input											sync_ack_received_0,
		input											sync_received_0,
		input											sync_received_dly_0,
		output                              rx_rden_to_MAC_0,
		input	[15:0]								current_sync_frame_ID_0,
		input											check_sync_frame_ID_0,
//		output										rx_valid_sync_ID_0,

		output  reg [8:0]           			tx_data_1,
		output  reg                         tx_wr_1,
		input                              	tx_rdy_1,

      input [8:0]                        	rx_data_from_MAC_1,
      input                        			rx_fifo_empty_from_MAC_1,
		input											RT_sync_marker_1,
		input											sync_ack_received_1,
		input											sync_received_1,
		input											sync_received_dly_1,
		output                              rx_rden_to_MAC_1,
		input	[15:0]								current_sync_frame_ID_1,
		input											check_sync_frame_ID_1,
//		output										rx_valid_sync_ID_1,
		
		output  reg [8:0]           			tx_data_2,
		output  reg                        	tx_wr_2,
		input                              	tx_rdy_2,
		
      input [8:0]                        	rx_data_from_MAC_2,
      input                        			rx_fifo_empty_from_MAC_2,
		input											RT_sync_marker_2,
		input											sync_ack_received_2,
		input											sync_received_2,
		input											sync_received_dly_2,
		output                              rx_rden_to_MAC_2,
		input	[15:0]								current_sync_frame_ID_2,
		input											check_sync_frame_ID_2,
//		output										rx_valid_sync_ID_2,
	
		output reg [8:0]           			tx_data_3,
		output reg                         	tx_wr_3,
		input                              	tx_rdy_3,
		
      input [8:0]                        	rx_data_from_MAC_3,
      input                        			rx_fifo_empty_from_MAC_3,
		input											RT_sync_marker_3,
		input											sync_ack_received_3,
		input											sync_received_3,
		input											sync_received_dly_3,
		output                              rx_rden_to_MAC_3,
		input	[15:0]								current_sync_frame_ID_3,
		input											check_sync_frame_ID_3,
//		output										rx_valid_sync_ID_3,		

		output										rx_valid_sync_ID,
		input   										clk ,   
		input 										nf2_reset
);

////////////////////////////////////////////////////////////////////

	wire [8:0] 	tx_data [3:0];
	wire [3:0] 	tx_wr_req;
	wire [8:0] 	tx_sync_resp [3:0];
	wire [0:3] 	tx_port_en;
	wire [8:0] 	rx_data_from_MAC [3:0];
	wire [3:0]  rx_fifo_empty_from_MAC;
	wire [3:0]  rx_rden_to_MAC;
	wire [3:0]  RT_sync_marker;
	wire [3:0]  transmiting_sync;
	wire [3:0]  RT_data_tx_en;
	wire [3:0]  sync_ack_received;
	wire [3:0]  sync_received;
	wire [3:0]  sync_received_dly;
	
	assign sync_received_global = sync_received_0 | sync_received_1 | sync_received_2 | sync_received_3;

	always @(tx_wr_req[0] or tx_wr_req[1] or tx_wr_req[2] or tx_wr_req[3]) begin
		if(tx_wr_req[0] == 1 && tx_wr_req[1] == 0 && tx_wr_req[2] == 0 && tx_wr_req[3] == 0) begin
			tx_wr_0 		= transmiting_sync[0];
			tx_data_0 	= tx_sync_resp[0];
			tx_wr_1 		= transmiting_sync[0] | tx_port_en[1];
			tx_data_1 	= tx_data[0];
			tx_wr_2 		= transmiting_sync[0] | tx_port_en[2];
			tx_data_2 	= tx_data[0];
			tx_wr_3 		= transmiting_sync[0] | tx_port_en[3];
			tx_data_3 	= tx_data[0];
		end
	
		else if(tx_wr_req[0] == 0 && tx_wr_req[1] == 1 && tx_wr_req[2] == 0 && tx_wr_req[3] == 0) begin
			tx_wr_1 		= transmiting_sync[1];
			tx_data_1 	= tx_sync_resp[1];
			tx_wr_0 		= transmiting_sync[1] | tx_port_en[0];
			tx_data_0 	= tx_data[1];
			tx_wr_2 		= transmiting_sync[1] | tx_port_en[2];
			tx_data_2 	= tx_data[1];
			tx_wr_3 		= transmiting_sync[1] | tx_port_en[3];
			tx_data_3 	= tx_data[1];
		end
	
		else if(tx_wr_req[0] == 0 && tx_wr_req[1] == 0 && tx_wr_req[2] == 1 && tx_wr_req[3] == 0) begin
			tx_wr_2 		= transmiting_sync[2];
			tx_data_2 	= tx_sync_resp[2];
			tx_wr_0 		= transmiting_sync[2] | tx_port_en[0];
			tx_data_0 	= tx_data[2];
			tx_wr_1 		= transmiting_sync[2] | tx_port_en[1];
			tx_data_1 	= tx_data[2];
			tx_wr_3 		= transmiting_sync[2] | tx_port_en[3];
			tx_data_3 	= tx_data[2];
		end
	
		else if(tx_wr_req[0] == 0 && tx_wr_req[1] == 0 && tx_wr_req[2] == 0 && tx_wr_req[3] == 1) begin
			tx_wr_3 		= transmiting_sync[3];
			tx_data_3 	= tx_sync_resp[3];
			tx_wr_0 		= transmiting_sync[3] | tx_port_en[0];
			tx_data_0 	= tx_data[3];
			tx_wr_1 		= transmiting_sync[3] | tx_port_en[1];
			tx_data_1 	= tx_data[3];
			tx_wr_2 		= transmiting_sync[3] | tx_port_en[2];
			tx_data_2 	= tx_data[3];
		end
		
		else begin
			tx_wr_0 		= 0;
			tx_wr_1 		= 0;
			tx_wr_2 		= 0;
			tx_wr_3 		= 0;
		end
	end

check_valid_sync check_valid_sync (
			.clk								(clk),
			.current_sync_frame_ID_0	(current_sync_frame_ID_0),
			.check_sync_frame_ID_0		(check_sync_frame_ID_0),

			.current_sync_frame_ID_1	(current_sync_frame_ID_1),
			.check_sync_frame_ID_1		(check_sync_frame_ID_1),

			.current_sync_frame_ID_2	(current_sync_frame_ID_2),
			.check_sync_frame_ID_2		(check_sync_frame_ID_2),
			
			.current_sync_frame_ID_3	(current_sync_frame_ID_3),
			.check_sync_frame_ID_3		(check_sync_frame_ID_3),
			
			.rx_valid_sync_ID				(rx_valid_sync_ID)
		);

genvar i;

generate
	for (i=0; i <4 ; i=i+1) begin: tx_enabler

RT_tx_data_enabler RT_tx_data_enabler (
				.clk								(clk),		  
				.tx_wr_en						(tx_port_en[i]),
				.reset							(sync_received_global),
				.sync_received					(sync_received[i]),
				.sync_received_dly			(sync_received_dly[i]),
				.ack_received					(sync_ack_received[i])
				);
	end
endgenerate	

generate
	for (i=0; i <4 ; i=i+1) begin: MC_NCPs

MC_NCP inst (
				.clk								(clk),		  
				.tx_fifo_to_MAC				(tx_data[i]),
				.wr_to_mac_fifo 				(tx_wr_req[i]),
				.tx_sync_resp					(tx_sync_resp[i]),

		      .rx_data_from_MAC	         (rx_data_from_MAC[i]),
		      .rx_fifo_empty_from_MAC    (rx_fifo_empty_from_MAC[i]),
				.RT_sync_marker				(RT_sync_marker[i]),
				.transmiting_sync				(transmiting_sync[i]),
				.rx_rden_to_MAC	         (rx_rden_to_MAC[i])
				);
	end
endgenerate	

assign rx_data_from_MAC[0] 			= rx_data_from_MAC_0;
assign rx_fifo_empty_from_MAC[0] 	= rx_fifo_empty_from_MAC_0;
assign rx_rden_to_MAC_0 				= rx_rden_to_MAC[0];
assign RT_sync_marker[0] 				= RT_sync_marker_0;
assign sync_ack_received[0] 			= sync_ack_received_0;
assign sync_received[0] 				= sync_received_0;
assign sync_received_dly[0] 			= sync_received_dly_0;

assign rx_data_from_MAC[1] 			= rx_data_from_MAC_1;
assign rx_fifo_empty_from_MAC[1] 	= rx_fifo_empty_from_MAC_1;
assign rx_rden_to_MAC_1 				= rx_rden_to_MAC[1];
assign RT_sync_marker[1] 				= RT_sync_marker_1;
assign sync_ack_received[1] 			= sync_ack_received_1;
assign sync_received[1] 				= sync_received_1;
assign sync_received_dly[1] 			= sync_received_dly_1;

assign rx_data_from_MAC[2] 			= rx_data_from_MAC_2;
assign rx_fifo_empty_from_MAC[2] 	= rx_fifo_empty_from_MAC_2;
assign rx_rden_to_MAC_2 				= rx_rden_to_MAC[2];
assign RT_sync_marker[2] 				= RT_sync_marker_2;
assign sync_ack_received[2] 			= sync_ack_received_2;
assign sync_received[2] 				= sync_received_2;
assign sync_received_dly[2] 			= sync_received_dly_2;

assign rx_data_from_MAC[3] 			= rx_data_from_MAC_3;
assign rx_fifo_empty_from_MAC[3] 	= rx_fifo_empty_from_MAC_3;
assign rx_rden_to_MAC_3 				= rx_rden_to_MAC[3];
assign RT_sync_marker[3] 				= RT_sync_marker_3;
assign sync_ack_received[3] 			= sync_ack_received_3;
assign sync_received[3] 				= sync_received_3;
assign sync_received_dly[3] 			= sync_received_dly_3;

endmodule