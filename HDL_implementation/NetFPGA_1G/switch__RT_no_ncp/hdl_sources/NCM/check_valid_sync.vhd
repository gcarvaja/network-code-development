
--Copyright 2007-2010, Embedded Software Group at the University of
--Waterloo. All rights reserved.  By using this software the USER
--indicates that he or she has read, understood and will comply with the
--following:

--- Embedded Software Group at the University of Waterloo hereby
--grants USER nonexclusive permission to use, copy and/or modify this
--software for internal, noncommercial, research purposes only. Any
--distribution, including commercial sale or license, of this software,
--copies of the software, its associated documentation and/or
--modifications of either is strictly prohibited without the prior
--consent of the Embedded Software Group at the University of Waterloo.
--Title to copyright to this software and its associated documentation
--shall at all times remain with the Embedded Software Group at the
--University of Waterloo.  Appropriate copyright notice shall be placed
--on all software copies, and a complete copy of this notice shall be
--included in all copies of the associated documentation.  No right is
--granted to use in advertising, publicity or otherwise any trademark,
--service mark, or the name of the Embedded Software Group at the
--University of Waterloo.


--- This software and any associated documentation is provided "as is"

--THE EMBEDDED SOFTWARE GROUP AT THE UNIVERSITY OF WATERLOO MAKES NO
--REPRESENTATIONS OR WARRANTIES, EXPRESS OR IMPLIED, INCLUDING THOSE OF
--MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, OR THAT USE OF
--THE SOFTWARE, MODIFICATIONS, OR ASSOCIATED DOCUMENTATION WILL NOT
--INFRINGE ANY PATENTS, COPYRIGHTS, TRADEMARKS OR OTHER INTELLECTUAL
--PROPERTY RIGHTS OF A THIRD PARTY.

--The Embedded Software Group at the University of Waterloo shall not be
--liable under any circumstances for any direct, indirect, special,
--incidental, or consequential damages with respect to any claim by USER
--or any third party on account of or arising from the use, or inability
--to use, this software or its associated documentation, even if The
--Embedded Software Group at the University of Waterloo has been advised
--of the possibility of those damages.

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

use work.ncm_package.all;

---- Uncomment the following library declaration if instantiating
---- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity check_valid_sync is
    Port ( clk : in  STD_LOGIC;
           current_sync_frame_ID_0 : in std_logic_vector (15 downto 0);
			  check_sync_frame_ID_0 : in std_logic;

           current_sync_frame_ID_1 : in std_logic_vector (15 downto 0);
			  check_sync_frame_ID_1 : in std_logic;

           current_sync_frame_ID_2 : in std_logic_vector (15 downto 0);
			  check_sync_frame_ID_2 : in std_logic;

           current_sync_frame_ID_3 : in std_logic_vector (15 downto 0);
			  check_sync_frame_ID_3 : in std_logic;			  
			  
			  rx_valid_sync_ID 	: out std_logic
			  );
end check_valid_sync;

architecture Behavioral of check_valid_sync is

	type states is (s_init, s_idle, s_check_port_0, s_check_port_1, s_check_port_2, s_check_port_3, s_check);
	signal currentstate : states := s_init;
	signal current_sync_ID : std_logic_vector (15 downto 0);
	signal prev_sync_ID : 	std_logic_vector (15 downto 0);

begin
   FSM: process (clk)
      variable nextstate : states;

   begin
   
      if (rising_edge(clk)) then
         nextstate := currentstate;
      
         case currentstate is
         
				when s_init =>
					rx_valid_sync_ID <= '0';
					nextstate := s_idle;
					
				when s_idle =>
					if(check_sync_frame_ID_0 = '1') then
						rx_valid_sync_ID <= '0';
						current_sync_ID <= current_sync_frame_ID_0;
						nextstate := s_check;
					
					elsif (check_sync_frame_ID_1 = '1') then
						rx_valid_sync_ID <= '0';
						current_sync_ID <= current_sync_frame_ID_1;
						nextstate := s_check;
					
					elsif (check_sync_frame_ID_2 = '1') then
						rx_valid_sync_ID <= '0';
						current_sync_ID <= current_sync_frame_ID_2;
						nextstate := s_check;
					
					elsif (check_sync_frame_ID_3 = '1') then
						rx_valid_sync_ID <= '0';
						current_sync_ID <= current_sync_frame_ID_3;
						nextstate := s_check;
					else
						nextstate := s_idle;
					end if;
					
				when s_check =>
					if ((current_sync_ID /= prev_sync_ID or prev_sync_ID = X"0000") and current_sync_ID > X"0000") then
						rx_valid_sync_ID <= '1';
						prev_sync_ID	  <= current_sync_ID;
					else
						rx_valid_sync_ID <= '0';
					end if;
						nextstate := s_idle;

				when others => 
					nextstate := s_idle;
         end case;
         
         currentstate <= nextstate;
         
      end if;
   end process;

end Behavioral;