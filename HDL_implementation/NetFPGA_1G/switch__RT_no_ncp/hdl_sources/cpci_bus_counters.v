///////////////////////////////////////////////////////////////////////////////
// vim:set shiftwidth=3 softtabstop=3 expandtab:
// $Id: cpci_bus.v 6061 2010-04-01 20:53:23Z grg $
//
// Module: cpci_bus.v
// Project: NetFPGA-1G
// Description: Virtex/CPCI bus interface.
//
//              Provides synchronization logic for CPCI register bus
//              including logic to insert/remove from the FIFOs.
//
//              Does not implement the register processing logic.
//
//              Note: bus_rd_data and bus_rd_data are NOT registers on input
//
///////////////////////////////////////////////////////////////////////////////

module cpci_bus
   #(
     parameter CPCI_NF2_ADDR_WIDTH = 27,
     parameter CPCI_NF2_DATA_WIDTH = 32
   )

   (
   // --- These are sigs to/from pins going to CPCI device
   input                                  cpci_rd_wr_L,
   input                                  cpci_req,
   input    [CPCI_NF2_ADDR_WIDTH-1:0]  	cpci_addr,
   input    [CPCI_NF2_DATA_WIDTH-1:0]  	cpci_wr_data,
   output   [CPCI_NF2_DATA_WIDTH-1:0]  	cpci_rd_data,
   output                              	cpci_data_tri_en,
   output reg                             cpci_wr_rdy,
   output reg                             cpci_rd_rdy,

   // --- Misc
   input                                  reset,
   input                                  pci_clk,
   input                                  core_clk,
	
	input [31:0]  									tx_non_RT_frame_counter_0,
	input [31:0]	  								tx_RT_data_frame_counter_0,
	input [31:0]	  								tx_RT_sync_frame_counter_0,
	input [31:0]	  								tx_RT_ack_frame_counter_0,
	input [31:0]	  								tx_RT_error_frame_counter_0,
	input [31:0]	  								rx_nonRT_frame_counter_0,
	input [31:0]	  								rx_data_frame_counter_0,
	input [31:0]	  								rx_sync_frame_counter_0,
	input [31:0]	  								rx_ack_frame_counter_0,
	input [31:0]	  								rx_total_frame_counter_0,
	input [31:0]	  								txfifo_eop_end_counter_0,
	input [31:0]	  								txfifo_empty_end_counter_0,
	input [31:0]	  								vld_bytes_rd_from_RT_txfifo_0,
	input [31:0]	  								vld_bytes_wr_to_RT_rxfifo_0,
	input [31:0]	  								rx_nonRT_dropped_counter_0,
	input [31:0]	  								total_tx_bytes_cnt_0,
	input [31:0]	  								total_rx_bytes_cnt_0,
	input [31:0]	  								badframes_RT_data_cnt_0,
	input [31:0]	  								badframes_RT_sync_cnt_0,
	input [31:0]	  								goodframes_RT_data_cnt_0,
	input [31:0]	  								goodframes_RT_sync_cnt_0,
	
	input [31:0]  									tx_non_RT_frame_counter_1,
	input [31:0]	  								tx_RT_data_frame_counter_1,
	input [31:0]	  								tx_RT_sync_frame_counter_1,
	input [31:0]	  								tx_RT_ack_frame_counter_1,
	input [31:0]	  								tx_RT_error_frame_counter_1,
	input [31:0]	  								rx_nonRT_frame_counter_1,
	input [31:0]	  								rx_data_frame_counter_1,
	input [31:0]	  								rx_sync_frame_counter_1,
	input [31:0]	  								rx_ack_frame_counter_1,
	input [31:0]	  								rx_total_frame_counter_1,
	input [31:0]	  								txfifo_eop_end_counter_1,
	input [31:0]	  								txfifo_empty_end_counter_1,
	input [31:0]	  								vld_bytes_rd_from_RT_txfifo_1,
	input [31:0]	  								vld_bytes_wr_to_RT_rxfifo_1,
	input [31:0]	  								rx_nonRT_dropped_counter_1,
	input [31:0]	  								total_tx_bytes_cnt_1,
	input [31:0]	  								total_rx_bytes_cnt_1,
	input [31:0]	  								badframes_RT_data_cnt_1,
	input [31:0]	  								badframes_RT_sync_cnt_1,
	input [31:0]	  								goodframes_RT_data_cnt_1,
	input [31:0]	  								goodframes_RT_sync_cnt_1,
	
	input [31:0]  									tx_non_RT_frame_counter_2,
	input [31:0]	  								tx_RT_data_frame_counter_2,
	input [31:0]	  								tx_RT_sync_frame_counter_2,
	input [31:0]	  								tx_RT_ack_frame_counter_2,
	input [31:0]	  								tx_RT_error_frame_counter_2,
	input [31:0]	  								rx_nonRT_frame_counter_2,
	input [31:0]	  								rx_data_frame_counter_2,
	input [31:0]	  								rx_sync_frame_counter_2,
	input [31:0]	  								rx_ack_frame_counter_2,
	input [31:0]	  								rx_total_frame_counter_2,
	input [31:0]	  								txfifo_eop_end_counter_2,
	input [31:0]	  								txfifo_empty_end_counter_2,
	input [31:0]	  								vld_bytes_rd_from_RT_txfifo_2,
	input [31:0]	  								vld_bytes_wr_to_RT_rxfifo_2,
	input [31:0]	  								rx_nonRT_dropped_counter_2,
	input [31:0]	  								total_tx_bytes_cnt_2,
	input [31:0]	  								total_rx_bytes_cnt_2,
	input [31:0]	  								badframes_RT_data_cnt_2,
	input [31:0]	  								badframes_RT_sync_cnt_2,
	input [31:0]	  								goodframes_RT_data_cnt_2,
	input [31:0]	  								goodframes_RT_sync_cnt_2,
	
	input [31:0]  									tx_non_RT_frame_counter_3,
	input [31:0]	  								tx_RT_data_frame_counter_3,
	input [31:0]	  								tx_RT_sync_frame_counter_3,
	input [31:0]	  								tx_RT_ack_frame_counter_3,
	input [31:0]	  								tx_RT_error_frame_counter_3,
	input [31:0]	  								rx_nonRT_frame_counter_3,
	input [31:0]	  								rx_data_frame_counter_3,
	input [31:0]	  								rx_sync_frame_counter_3,
	input [31:0]	  								rx_ack_frame_counter_3,
	input [31:0]	  								rx_total_frame_counter_3,
	input [31:0]	  								txfifo_eop_end_counter_3,
	input [31:0]	  								txfifo_empty_end_counter_3,
	input [31:0]	  								vld_bytes_rd_from_RT_txfifo_3,
	input [31:0]	  								vld_bytes_wr_to_RT_rxfifo_3,
	input [31:0]	  								rx_nonRT_dropped_counter_3,
	input [31:0]	  								total_tx_bytes_cnt_3,
	input [31:0]	  								total_rx_bytes_cnt_3,
	input [31:0]	  								badframes_RT_data_cnt_3,
	input [31:0]	  								badframes_RT_sync_cnt_3,
	input [31:0]	  								goodframes_RT_data_cnt_3,
	input [31:0]	  								goodframes_RT_sync_cnt_3
);

////////////////////////////////////////////////////////////////////

wire	[CPCI_NF2_ADDR_WIDTH-1:0] 	cpci_reg_addr;
wire 	[CPCI_NF2_DATA_WIDTH-1:0] 	vram_rd_data, ncm_mem_data;
wire 	[CPCI_NF2_DATA_WIDTH-1:0] 	ncm_frame_data[3:0];
//---------------------------------------------
// CPCI interface
//---------------------------------------------
// CPCI  wires
//wire  [CPCI_NF2_DATA_WIDTH-1:0] 	cpci_reg_wr_data,cpci_reg_rd_data; // Internal buses
reg 										cpci_reg_rd_wr_L;

// CPCI Buses
//assign cpci_rd_data 	= 	32'b1; 
assign cpci_data_tri_en 	= 	cpci_rd_wr_L;
//assign cpci_reg_addr 		= 	cpci_addr;
//assign cpci_rd_data 			= 	cpci_reg_rd_data;
//assign cpci_reg_wr_data 	= 	cpci_wr_data;

reg rd_wr_L_lock;
	always@(posedge pci_clk) begin
		
		if(cpci_req)
			cpci_rd_rdy <=1;
		else 
			cpci_rd_rdy <=0;
		
		if(~cpci_rd_wr_L)
			cpci_wr_rdy <=1;
		else 
			cpci_wr_rdy <=0;
		
		if(~cpci_rd_wr_L & ~rd_wr_L_lock) begin // Interlocking ( forces to read after a write...this is a workaround because we are not receiving cpci_req)
			cpci_reg_rd_wr_L <=0;
			rd_wr_L_lock <= 1;
		end
		
		else
			cpci_reg_rd_wr_L <=1;
		
		if(cpci_rd_wr_L)
			rd_wr_L_lock <=0;
end	

/////////////////////////////

	 counters counters (
				.core_clk   				 			(core_clk),
				.pci_clk 					 			(pci_clk),
			  
				.pci_data_in							(),
				.pci_address 							(cpci_addr[9:2]),
				.pci_data_out 			 				(cpci_rd_data),
				.pci_rw 					 				(1'b0), //cfgrom_bus_wen,
			  
				.core_rw									(1'b1),

				.tx_non_RT_frame_counter_0  			(tx_non_RT_frame_counter_0),
				.tx_RT_data_frame_counter_0  			(tx_RT_data_frame_counter_0),
				.tx_RT_sync_frame_counter_0  			(tx_RT_sync_frame_counter_0),
				.tx_RT_ack_frame_counter_0  			(tx_RT_ack_frame_counter_0),
				.tx_RT_error_frame_counter_0  		(tx_RT_error_frame_counter_0),
				.rx_nonRT_frame_counter_0  		(rx_nonRT_frame_counter_0),
				.rx_data_frame_counter_0  			(rx_data_frame_counter_0),
				.rx_sync_frame_counter_0  			(rx_sync_frame_counter_0),
				.rx_ack_frame_counter_0  			(rx_ack_frame_counter_0),
				.rx_total_frame_counter_0  		(rx_total_frame_counter_0),
				.txfifo_eop_end_counter_0   		(txfifo_eop_end_counter_0),		  
				.txfifo_empty_end_counter_0   	(txfifo_empty_end_counter_0),
				.vld_bytes_rd_from_RT_txfifo_0   (vld_bytes_rd_from_RT_txfifo_0),
				.vld_bytes_wr_to_RT_rxfifo_0   	(vld_bytes_wr_to_RT_rxfifo_0),					
				.rx_nonRT_dropped_counter_0		(rx_nonRT_dropped_counter_0),
				.total_tx_bytes_cnt_0				(total_tx_bytes_cnt_0),
				.total_rx_bytes_cnt_0				(total_rx_bytes_cnt_0),
			.goodframes_RT_sync_cnt_0			(goodframes_RT_sync_cnt_0),
			.goodframes_RT_data_cnt_0			(goodframes_RT_data_cnt_0),
			.badframes_RT_sync_cnt_0			(badframes_RT_sync_cnt_0),
			.badframes_RT_data_cnt_0			(badframes_RT_data_cnt_0),

				.tx_non_RT_frame_counter_1  			(tx_non_RT_frame_counter_1),
				.tx_RT_data_frame_counter_1  			(tx_RT_data_frame_counter_1),
				.tx_RT_sync_frame_counter_1  			(tx_RT_sync_frame_counter_1),
				.tx_RT_ack_frame_counter_1  		(tx_RT_ack_frame_counter_1),
				.tx_RT_error_frame_counter_1  		(tx_RT_error_frame_counter_1),
				.rx_nonRT_frame_counter_1  		(rx_nonRT_frame_counter_1),
				.rx_data_frame_counter_1  			(rx_data_frame_counter_1),
				.rx_sync_frame_counter_1  			(rx_sync_frame_counter_1),
				.rx_ack_frame_counter_1  			(rx_ack_frame_counter_1),
				.rx_total_frame_counter_1  		(rx_total_frame_counter_1),
				.txfifo_eop_end_counter_1   		(txfifo_eop_end_counter_1),		  
				.txfifo_empty_end_counter_1   	(txfifo_empty_end_counter_1),
				.vld_bytes_rd_from_RT_txfifo_1   (vld_bytes_rd_from_RT_txfifo_1),
				.vld_bytes_wr_to_RT_rxfifo_1   	(vld_bytes_wr_to_RT_rxfifo_1),		
				.rx_nonRT_dropped_counter_1		(rx_nonRT_dropped_counter_1),
				.total_tx_bytes_cnt_1				(total_tx_bytes_cnt_1),
				.total_rx_bytes_cnt_1				(total_rx_bytes_cnt_1),
			.goodframes_RT_sync_cnt_1			(goodframes_RT_sync_cnt_1),
			.goodframes_RT_data_cnt_1			(goodframes_RT_data_cnt_1),
			.badframes_RT_sync_cnt_1			(badframes_RT_sync_cnt_1),
			.badframes_RT_data_cnt_1			(badframes_RT_data_cnt_1),

				.tx_non_RT_frame_counter_2  			(tx_non_RT_frame_counter_2),
				.tx_RT_data_frame_counter_2  			(tx_RT_data_frame_counter_2),
				.tx_RT_sync_frame_counter_2  			(tx_RT_sync_frame_counter_2),
				.tx_RT_ack_frame_counter_2  		(tx_RT_ack_frame_counter_2),
				.tx_RT_error_frame_counter_2  		(tx_RT_error_frame_counter_2),
				.rx_nonRT_frame_counter_2  		(rx_nonRT_frame_counter_2),
				.rx_data_frame_counter_2  			(rx_data_frame_counter_2),
				.rx_sync_frame_counter_2  			(rx_sync_frame_counter_2),
				.rx_ack_frame_counter_2  			(rx_ack_frame_counter_2),
				.rx_total_frame_counter_2  		(rx_total_frame_counter_2),
				.txfifo_eop_end_counter_2   		(txfifo_eop_end_counter_2),		  
				.txfifo_empty_end_counter_2   	(txfifo_empty_end_counter_2),
				.vld_bytes_rd_from_RT_txfifo_2   (vld_bytes_rd_from_RT_txfifo_2),
				.vld_bytes_wr_to_RT_rxfifo_2   	(vld_bytes_wr_to_RT_rxfifo_2),
				.rx_nonRT_dropped_counter_2		(rx_nonRT_dropped_counter_2),				
				.total_tx_bytes_cnt_2				(total_tx_bytes_cnt_2),
				.total_rx_bytes_cnt_2				(total_rx_bytes_cnt_2),
			.goodframes_RT_sync_cnt_2			(goodframes_RT_sync_cnt_2),
			.goodframes_RT_data_cnt_2			(goodframes_RT_data_cnt_2),
			.badframes_RT_sync_cnt_2			(badframes_RT_sync_cnt_2),
			.badframes_RT_data_cnt_2			(badframes_RT_data_cnt_2),

				.tx_non_RT_frame_counter_3  			(tx_non_RT_frame_counter_3),
				.tx_RT_data_frame_counter_3  			(tx_RT_data_frame_counter_3),
				.tx_RT_sync_frame_counter_3 			(tx_RT_sync_frame_counter_3),
				.tx_RT_ack_frame_counter_3  		(tx_RT_ack_frame_counter_3),
				.tx_RT_error_frame_counter_3  		(tx_RT_error_frame_counter_3),
				.rx_nonRT_frame_counter_3  		(rx_nonRT_frame_counter_3),
				.rx_data_frame_counter_3  			(rx_data_frame_counter_3),
				.rx_sync_frame_counter_3  			(rx_sync_frame_counter_3),
				.rx_ack_frame_counter_3  			(rx_ack_frame_counter_3),
				.rx_total_frame_counter_3  		(rx_total_frame_counter_3),
				.txfifo_eop_end_counter_3   		(txfifo_eop_end_counter_3),		  
				.txfifo_empty_end_counter_3   	(txfifo_empty_end_counter_3),
				.vld_bytes_rd_from_RT_txfifo_3   (vld_bytes_rd_from_RT_txfifo_3),
				.vld_bytes_wr_to_RT_rxfifo_3   	(vld_bytes_wr_to_RT_rxfifo_3),		
				.rx_nonRT_dropped_counter_3		(rx_nonRT_dropped_counter_3),
				.total_tx_bytes_cnt_3				(total_tx_bytes_cnt_3),
				.total_rx_bytes_cnt_3				(total_rx_bytes_cnt_3),
			.goodframes_RT_sync_cnt_3			(goodframes_RT_sync_cnt_3),
			.goodframes_RT_data_cnt_3			(goodframes_RT_data_cnt_3),
			.badframes_RT_sync_cnt_3			(badframes_RT_sync_cnt_3),
			.badframes_RT_data_cnt_3			(badframes_RT_data_cnt_3)
 );

endmodule // cpci_bus