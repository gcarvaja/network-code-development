`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    06:19:13 01/28/2013 
// Design Name: 
// Module Name:    ncm_main 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module ncm_main(
    input	clk_ncm,                		  
	 input [10:0] pclock_divider,
	 input       clk_ext_bus, 		
	 input       rst    ,        
	input		  control_reg_hit	,	
			  
	input		  txgmiimiiclk ,      
	input	     rxgmiimiiclk ,      
			  
    input       NCM_wen       		,
    input [31:0]      address_in  ,     	
    input [31:0]      data_in      , 		
    output [31:0]       data_out    ,  		

    output [0:7] 			  emacphytxd  ,      
     output            emacphytxen,        
     output            emacphytxer ,       
     input [0:7]      phyemacrxd         ,
     input      phyemacrxdv      ,
      input     phyemacrxer     ,

     input [23:0]      mac_high,//           : in std_logic_vector(23 downto 0);
     input [23:0]      mac_low, //            : in std_logic_vector(23 downto 0);

     
     input      ncm_user1 , //         : in std_logic;
     input      ncm_user2 , //         : in std_logic;
		//input	  gen_traffic_mode, //   : in std_logic;
     
	  input run_NCM
	 
	 );

wire [18:0] address ;
wire [15:0] varram_app_address ;
wire [31:0] varram_app_din ;
wire [31:0] varram_app_dout ;

wire [7:0] progrom_bus_address ;
wire [31:0] progrom_bus_din ;
wire [31:0] progrom_bus_dout ;

wire [7:0] cfgrom_bus_address ;
wire [31:0] cfgrom_bus_din ;
wire [31:0] cfgrom_bus_dout ;

wire [7:0] cfgrom_queue_bus_address ;
wire [31:0] cfgrom_queue_bus_din ;
wire [31:0] cfgrom_queue_bus_dout ;

wire [7:0] counters_bus_address ;
wire [31:0] counters_bus_din ;
wire [31:0] counters_bus_dout ;

assign address = address_in[18:0];

	  bram_decoder_for_bus_access bram_decoder_for_bus_access(
      .clk_bus       		(clk_ext_bus),
      .bram_addr     		(address),
      .din           		(data_in),
      .dout          		(data_out),
      .wen           		(NCM_wen),
		
		.progrom_addr  		(progrom_bus_address),
      .progrom_din   		(progrom_bus_din),
      .progrom_dout  		(progrom_bus_dout),
      .progrom_wen   		(progrom_bus_wen),

		.varram_addr  			(varram_app_address[13:0]),
      .varram_din   			(varram_app_din),
      .varram_dout  			(varram_app_dout),
      .varram_wen   			(varram_app_wen),

      .cfgrom_addr		 	(cfgrom_bus_address),
      .cfgrom_din     		(cfgrom_bus_din),
      .cfgrom_dout    		(cfgrom_bus_dout),
      .cfgrom_wen     		(cfgrom_bus_wen),
		
		.cfgrom_queue_addr 	(cfgrom_queue_bus_address),
      .cfgrom_queue_din  	(cfgrom_queue_bus_din),
      .cfgrom_queue_dout 	(cfgrom_queue_bus_dout),
      .cfgrom_queue_wen  	(cfgrom_queue_bus_wen),
		
		.counters_addr 		(counters_bus_address),
      .counters_din  		(counters_bus_din),
      .counters_dout 		(counters_bus_dout),
      .counters_wen  		(counters_bus_wen)
      );


ncm_frame ncm_frame (
				.clk_ncm								(clk_ncm										),
				.pclock_divider				(pclock_divider								),
				.clk_ext_bus 					   (clk_ext_bus									),
				.clk_app								(clk_ext_bus									),
				.sys_rst	                  	(rst										),
				.control_reg_hit				(control_reg_hit								),
		  
		         
 
		  
				.gmii_tx_clk           	(txgmiimiiclk								),
				.gmii_rx_clk         		(rxgmiimiiclk 							),
		  
				.varram_app_wen     (varram_app_wen),
				.varram_app_addr    (varram_app_address),
				.varram_app_din     (varram_app_din),
				.varram_app_dout    (varram_app_dout),

				.progrom_bus_address      (progrom_bus_address),
				.progrom_bus_din          (progrom_bus_din),
				.progrom_bus_dout        (progrom_bus_dout),
				.progrom_bus_wen          (progrom_bus_wen),

				.cfgrom_bus_address       (cfgrom_bus_address),
				.cfgrom_bus_din          (cfgrom_bus_din ),
				.cfgrom_bus_dout         (cfgrom_bus_dout),
				.cfgrom_bus_wen           (cfgrom_bus_wen),

				.cfgrom_queue_bus_address      ( cfgrom_queue_bus_address),
				.cfgrom_queue_bus_din           (cfgrom_queue_bus_din ),
				.cfgrom_queue_bus_dout         (cfgrom_queue_bus_dout),
				.cfgrom_queue_bus_wen           (cfgrom_queue_bus_wen),
		  
				.counters_bus_address       (counters_bus_address),
				.counters_bus_din          (counters_bus_din ),
				.counters_bus_dout         (counters_bus_dout),
				.counters_bus_wen           (counters_bus_wen),
       
			.gmii_txd           		(emacphytxd								),
				.gmii_tx_en           		(emacphytxen							),
				.gmii_tx_er          		(emacphytxer 							),
				.gmii_rxd           		(phyemacrxd								),
				.gmii_rx_dv          		(phyemacrxdv							),
				.gmii_rx_er          		(phyemacrxer							),
		  
				.mac_high						(mac_high										),
				.mac_low							(mac_low										),

				.BE_type			 	 			(16'h0800),
				.NCData_type			 		(16'h5CE0),
				.NCSync_type			 		(16'h5CFF),
				.NCAck_type			 			(16'h5CAA),
				.sync_ack_en			 		(1'b0),
				
				.run_NCM                   (run_NCM										),
				.ncm_user						(8'b0									),
				.ncm_shouldstopeval        (),
				.ncm_shouldstop        (1'b0)
				);
				
endmodule
