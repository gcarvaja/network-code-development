library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

library UNISIM;
use UNISIM.VComponents.all;
use work.ncm_package.all;

entity input_class_delay is
    Port ( 	clk : in  STD_LOGIC;
				rst : in std_logic;
          
				gmac_rx_data_in 			: in std_logic_vector (7 downto 0);
				gmac_rx_data_out 			: out std_logic_vector (7 downto 0);
				gmac_rx_dvld_in 			: in std_logic;
				gmac_rx_dvld_out 			: out std_logic;
				gmac_rx_goodframe_in 	: in std_logic;
				gmac_rx_goodframe_out 	: out std_logic;
				gmac_rx_badframe_in 		: in std_logic;
				gmac_rx_badframe_out 	: out std_logic;
				
				NCSync_type					: in std_logic_vector (15 downto 0);
				NCData_type					: in std_logic_vector (15 downto 0);
				NCAck_type					: in std_logic_vector (15 downto 0);
				
				RT_ack_frames_counter 	: out std_logic_vector(31 downto 0);
				RT_data_frames_counter 	: out std_logic_vector(31 downto 0);
				RT_sync_frames_counter 	: out std_logic_vector(31 downto 0);
				nonRT_frames_counter 	: out std_logic_vector(31 downto 0);
				RT_data_marker 			: out std_logic;
				RT_sync_marker 			: out std_logic;
				RT_ack_marker 				: out std_logic;
				start_send_ack 			: out std_logic;
				receiving_sync 			: out std_logic
           );
end input_class_delay;

architecture Behavioral of input_class_delay is

component shiftreg
	port (
	d		: IN std_logic;
	clk	: IN std_logic;
	rst	: IN std_logic;
	q		: OUT std_logic) ;
end component;

	type states is (s_init, s_idle, s_wait, s_dest, s_src, s_type_h, s_type_l, s_rdy);
   signal currentstate : states 		:= s_init;
	signal RT_data_frames_counter_i 	: std_logic_vector(31 downto 0) := X"00000000";
	signal RT_sync_frames_counter_i 	: std_logic_vector(31 downto 0) := X"00000000";
	signal RT_ack_frames_counter_i 	: std_logic_vector(31 downto 0) := X"00000000";
	signal nonRT_frames_counter_i 	: std_logic_vector(31 downto 0) := X"00000000";
	signal RT_data_marker_temp 		: std_logic;
	signal RT_sync_marker_temp 		: std_logic;
	signal RT_ack_marker_temp 			: std_logic;
	signal start_send_ack_i 			: std_logic;
	signal receiving_sync_i 			: std_logic;
	signal NCM_SYNC_TYPE_H				: std_logic_vector (7 downto 0);
	signal NCM_SYNC_TYPE_L				: std_logic_vector (7 downto 0);
	signal NCM_DATA_TYPE_H				: std_logic_vector (7 downto 0);
	signal NCM_DATA_TYPE_L				: std_logic_vector (7 downto 0);
	signal NCM_ACK_TYPE_H				: std_logic_vector (7 downto 0);
	signal NCM_ACK_TYPE_L				: std_logic_vector (7 downto 0);
begin

NCM_SYNC_TYPE_H <= NCSync_type (15 downto 8);
NCM_SYNC_TYPE_L <= NCSync_type (7 downto 0);
NCM_DATA_TYPE_H <= NCData_type (15 downto 8);
NCM_DATA_TYPE_L <= NCData_type (7 downto 0);
NCM_ACK_TYPE_H <= NCAck_type (15 downto 8);
NCM_ACK_TYPE_L <= NCAck_type (7 downto 0);

RT_ack_frames_counter 	<= RT_ack_frames_counter_i;
RT_data_frames_counter 	<= RT_data_frames_counter_i;
RT_sync_frames_counter 	<= RT_sync_frames_counter_i;
nonRT_frames_counter 	<= nonRT_frames_counter_i;
start_send_ack 			<= start_send_ack_i;
receiving_sync 			<= receiving_sync_i;


shiftreg2 : shiftreg
		port map (
			d 		=> RT_data_marker_temp,
			clk 	=> clk,
			rst 	=> '0',
			q 		=> RT_data_marker
);	

shiftreg3 : shiftreg
		port map (
			d 		=> RT_sync_marker_temp,
			clk 	=> clk,
			rst 	=> '0',
			q 		=> RT_sync_marker
);	

shiftreg4 : shiftreg
		port map (
			d 		=> RT_ack_marker_temp,
			clk	=> clk,
			rst 	=> '0',
			q 		=> RT_ack_marker
);	

shift_register : shift_ram_v9_0
		port map (
			d(15 downto 11)	=> "00000",
			d(10) 				=> gmac_rx_badframe_in,
			d(9) 					=>	gmac_rx_goodframe_in,
			d(8) 					=>	gmac_rx_dvld_in,
			d(7 downto 0) 		=> gmac_rx_data_in,
			clk 					=> clk,
			ce 					=> '1',
			q(15 downto 11) 	=> open,
			q(10) 				=> gmac_rx_badframe_out,
			q(9)					=>	gmac_rx_goodframe_out,
			q(8) 					=>	gmac_rx_dvld_out,
			q(7 downto 0) 		=> gmac_rx_data_out);		


   FSM : process(clk, rst)
      variable nextstate : states := s_init;
      variable cnt : integer range 0 to 1500 := 0;
   begin

     if (rst = '1') then

        currentstate <= s_init;
         nextstate := s_init;
      
    elsif (rising_edge(clk)) then
        
		  nextstate := currentstate;
                  
         case currentstate is
     
            when s_init => 
					RT_sync_marker_temp 			<= '0';
               RT_data_marker_temp 			<= '0';
					RT_ack_frames_counter_i 	<= (others => '0');
					RT_data_frames_counter_i 	<= (others => '0');
					RT_sync_frames_counter_i 	<= (others => '0');
					nonRT_frames_counter_i 		<= (others => '0');
					cnt := 0;
               nextstate := s_idle;

            when s_idle =>
					cnt := 0;
					start_send_ack_i <= '0';
               receiving_sync_i <= '0';
					if (gmac_rx_dvld_in = '1') then        
						cnt := cnt + 1;
						nextstate := s_dest;
                  RT_data_marker_temp <= '0';  
                  RT_ack_marker_temp <= '0';  
						RT_sync_marker_temp <= '0';
					end if;
					
            when s_dest =>                   
               
					if (gmac_rx_dvld_in = '0') then
                     nextstate := s_rdy;
                     
                  else
							cnt := cnt + 1;

                     if (cnt = 6) then                   -- 6 bytes read
                           nextstate := s_src;
                           cnt := 0;
                     end if;
   
                  end if;
                              
            when s_src =>                   
               
                  if (gmac_rx_dvld_in = '0') then
                     nextstate := s_rdy;
                     
                  else
							cnt := cnt + 1;

                     if (cnt = 6) then                   -- 6 bytes read
                           nextstate := s_type_h;
                           cnt := 0;
                     end if;
   
                  end if;
                  
            when s_type_h =>
         
               	if (gmac_rx_dvld_in = '0') then
                     nextstate := s_rdy;
                     
                  else
							if (gmac_rx_data_in = NCM_SYNC_TYPE_H or gmac_rx_data_in = NCM_DATA_TYPE_H or gmac_rx_data_in = NCM_ACK_TYPE_H) then -- or gmac_rx_data_in = X"00") then
									nextstate := s_type_l;
							else
									nonRT_frames_counter_i <= nonRT_frames_counter_i + 1;
									nextstate  := s_wait;
								end if;

							end if;
					
            when s_type_l =>        -- simply read src address
         
						if (gmac_rx_dvld_in = '0') then
                     nextstate := s_rdy;
                     
                  else
                     if (gmac_rx_data_in = NCM_DATA_TYPE_L) then
									RT_data_marker_temp <= '1';
									RT_data_frames_counter_i <= RT_data_frames_counter_i + 1;
									--nextstate := s_wait;
							elsif (gmac_rx_data_in = NCM_SYNC_TYPE_L) then
									RT_sync_marker_temp <= '1';
									RT_sync_frames_counter_i <= RT_sync_frames_counter_i + 1;
									start_send_ack_i <= '1';
									receiving_sync_i <= '1';
									--nextstate := s_wait;
							elsif (gmac_rx_data_in = NCM_ACK_TYPE_L) then
									RT_ack_marker_temp <= '1';
									RT_ack_frames_counter_i <= RT_ack_frames_counter_i + 1;
									--nextstate := s_wait;
							end if;
							nextstate := s_wait;
						end if;

            when s_wait =>
						start_send_ack_i <= '0';
						if (gmac_rx_dvld_in = '0') then
							nextstate := s_rdy;
						end if;
						
            when s_rdy =>       
					receiving_sync_i <= '0';
               nextstate := s_idle;
					
            when others =>
               nextstate := s_idle;       -- to be safe
            
         end case;

         currentstate <= nextstate;

      end if;
         
   end process;

end Behavioral;