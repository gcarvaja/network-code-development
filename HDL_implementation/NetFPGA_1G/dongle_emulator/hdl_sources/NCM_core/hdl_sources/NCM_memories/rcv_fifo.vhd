----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    21:03:52 12/28/2006 
-- Design Name:    NCM basic
-- Module Name:    rcv_fifo - Behavioral 
-- Project Name:   NCM 
-- Target Devices: 
-- Tool versions: 
-- Description:    8 receive channel fifos for data reception
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

---- Uncomment the following library declaration if instantiating
---- any Xilinx primitives in this code.
library UNISIM;
use UNISIM.VComponents.all;
use work.ncm_package.all;

entity rcv_fifo is
    Port ( rdclk : in  STD_LOGIC;
			  wrclk : in  STD_LOGIC;

           -- from auto receiver
           w_en : in  STD_LOGIC;
           w_channel : in  STD_LOGIC_VECTOR (3 downto 0);
           data_in : in  STD_LOGIC_VECTOR (31 downto 0);
           reset : in  STD_LOGIC;
           empty_for_w : out  STD_LOGIC;
           full_for_w : out  STD_LOGIC;
           -- to receive_cmd
           empty_for_r : out  STD_LOGIC;
           full_for_r : out  STD_LOGIC;
           r_en : in  STD_LOGIC;
           r_channel : in  STD_LOGIC_VECTOR (3 downto 0);
           data_out : out  STD_LOGIC_VECTOR (31 downto 0);
           -- status output
			  rd_count: out std_logic_vector(11 downto 0);
			  wr_count: out std_logic_vector(11 downto 0);
			  
           wr_ack : out  STD_LOGIC;
			  overflow: out  STD_LOGIC;
			  valid: out  STD_LOGIC;
			  underflow: out  STD_LOGIC;
			  empty: out std_logic_vector (15 downto 0)

           );
end rcv_fifo;

architecture Behavioral of rcv_fifo is

   signal r_channel_i : std_logic_vector(3 downto 0);
   signal w_channel_i : std_logic_vector(3 downto 0);

   signal w_en_i : std_logic_vector(15 downto 0) := (others => '0'); 
   signal r_en_i : std_logic_vector(15 downto 0) := (others => '0'); 

   signal empty_w : std_logic;   -- decoded signals
   signal empty_r : std_logic;
   signal full_w  : std_logic;
   signal full_r  : std_logic;

   signal empty_i : std_logic_vector(15 downto 0) := (others => '0');
   signal full_i  : std_logic_vector(15 downto 0) := (others => '0');

   signal rderr_i : std_logic_vector(15 downto 0) := (others => '0');
   signal wrerr_i : std_logic_vector(15 downto 0) := (others => '0');

   signal reset_i : std_logic_vector(15 downto 0) := (others => '0');

   signal data_out_i : std_logic_vector(31 downto 0);

   signal data_out_0  : std_logic_vector(31 downto 0) := (others => '0');
   signal data_out_1  : std_logic_vector(31 downto 0) := (others => '0');
   signal data_out_2  : std_logic_vector(31 downto 0) := (others => '0');
   signal data_out_3  : std_logic_vector(31 downto 0) := (others => '0');
   signal data_out_4  : std_logic_vector(31 downto 0) := (others => '0');
   signal data_out_5  : std_logic_vector(31 downto 0) := (others => '0');
   signal data_out_6  : std_logic_vector(31 downto 0) := (others => '0');
   signal data_out_7  : std_logic_vector(31 downto 0) := (others => '0');
	
	component rcv_buffer
	port (
	din: IN std_logic_VECTOR(31 downto 0);
	rd_clk: IN std_logic;
	rd_en: IN std_logic;
	rst: IN std_logic;
	wr_clk: IN std_logic;
	wr_en: IN std_logic;
	dout: OUT std_logic_VECTOR(31 downto 0);
	empty: OUT std_logic;
	full: OUT std_logic;
	overflow: OUT std_logic;
	underflow: OUT std_logic	);
end component;

begin

   r_channel_i <= r_channel(3 downto 0);        -- limit to 16 channels
   w_channel_i <= w_channel(3 downto 0);

   empty_for_r <= empty_r;
   empty_for_w <= empty_w;
   full_for_r <= full_r;
   full_for_w <= full_w;
   
   data_out <= data_out_i;
   empty <= empty_i;

   -- the write enable selector
   w_en_i(0)  <= w_en when w_channel_i = "0000" else '0';
   w_en_i(1)  <= w_en when w_channel_i = "0001" else '0';
   w_en_i(2)  <= w_en when w_channel_i = "0010" else '0';
   w_en_i(3)  <= w_en when w_channel_i = "0011" else '0';
   w_en_i(4)  <= w_en when w_channel_i = "0100" else '0';
   w_en_i(5)  <= w_en when w_channel_i = "0101" else '0';
   w_en_i(6)  <= w_en when w_channel_i = "0110" else '0';
   w_en_i(7)  <= w_en when w_channel_i = "0111" else '0';

   -- the read enable selector
   r_en_i(0)  <= r_en when r_channel_i = "0000" else '0';
   r_en_i(1)  <= r_en when r_channel_i = "0001" else '0';
   r_en_i(2)  <= r_en when r_channel_i = "0010" else '0';
   r_en_i(3)  <= r_en when r_channel_i = "0011" else '0';
   r_en_i(4)  <= r_en when r_channel_i = "0100" else '0';
   r_en_i(5)  <= r_en when r_channel_i = "0101" else '0';
   r_en_i(6)  <= r_en when r_channel_i = "0110" else '0';
   r_en_i(7)  <= r_en when r_channel_i = "0111" else '0';

   -- the fifo reset selector
   reset_i(0)  <= reset when w_channel_i = "0000" else '0';
   reset_i(1)  <= reset when w_channel_i = "0001" else '0';
   reset_i(2)  <= reset when w_channel_i = "0010" else '0';
   reset_i(3)  <= reset when w_channel_i = "0011" else '0';
   reset_i(4)  <= reset when w_channel_i = "0100" else '0';
   reset_i(5)  <= reset when w_channel_i = "0101" else '0';
   reset_i(6)  <= reset when w_channel_i = "0110" else '0';
   reset_i(7)  <= reset when w_channel_i = "0111" else '0';

   -- the empty flag
   empty_w <= empty_i(0)  when w_channel_i = "0000" else
              empty_i(1)  when w_channel_i = "0001" else
              empty_i(2)  when w_channel_i = "0010" else
              empty_i(3)  when w_channel_i = "0011" else
              empty_i(4)  when w_channel_i = "0100" else
              empty_i(5)  when w_channel_i = "0101" else
              empty_i(6)  when w_channel_i = "0110" else
              empty_i(7)  when w_channel_i = "0111" else
              '1';

   empty_r <= empty_i(0)  when r_channel_i = "0000" else
              empty_i(1)  when r_channel_i = "0001" else
              empty_i(2)  when r_channel_i = "0010" else
              empty_i(3)  when r_channel_i = "0011" else
              empty_i(4)  when r_channel_i = "0100" else
              empty_i(5)  when r_channel_i = "0101" else
              empty_i(6)  when r_channel_i = "0110" else
              empty_i(7)  when r_channel_i = "0111" else
              '1';

   -- the full flag
   full_w  <= full_i(0)  when w_channel_i = "0000" else
              full_i(1)  when w_channel_i = "0001" else
              full_i(2)  when w_channel_i = "0010" else
              full_i(3)  when w_channel_i = "0011" else
              full_i(4)  when w_channel_i = "0100" else
              full_i(5)  when w_channel_i = "0101" else
              full_i(6)  when w_channel_i = "0110" else
              full_i(7)  when w_channel_i = "0111" else
              '0';

   full_r  <= full_i(0)  when r_channel_i = "0000" else
              full_i(1)  when r_channel_i = "0001" else
              full_i(2)  when r_channel_i = "0010" else
              full_i(3)  when r_channel_i = "0011" else
              full_i(4)  when r_channel_i = "0100" else
              full_i(5)  when r_channel_i = "0101" else
              full_i(6)  when r_channel_i = "0110" else
              full_i(7)  when r_channel_i = "0111" else
              '0';

   -- data_out
   data_out_i <= data_out_0  when r_channel_i = "0000" else
                 data_out_1  when r_channel_i = "0001" else
                 data_out_2  when r_channel_i = "0010" else
                 data_out_3  when r_channel_i = "0011" else
                 data_out_4  when r_channel_i = "0100" else
                 data_out_5  when r_channel_i = "0101" else
                 data_out_6  when r_channel_i = "0110" else
                 data_out_7  when r_channel_i = "0111" else
                 (others => '0');
                 
		FIFO16_inst_0 : rcv_buffer
		port map (
			din => data_in,
			rd_clk => rdclk,
			rd_en => r_en_i(0),
			rst => reset_i(0),
			wr_clk => wrclk,
			wr_en => w_en_i(0),
			dout => data_out_0,
			empty => empty_i(0),
			full => full_i(0),
			overflow => wrerr_i(0),
			underflow => rderr_i(0));
                 
		FIFO16_inst_1 : rcv_buffer
		port map (
			din => data_in,
			rd_clk => rdclk,
			rd_en => r_en_i(1),
			rst => reset_i(1),
			wr_clk => wrclk,
			wr_en => w_en_i(1),
			dout => data_out_1,
			empty => empty_i(1),
			full => full_i(1),
			overflow => wrerr_i(1),
			underflow => rderr_i(1));
                 
		FIFO16_inst_2 : rcv_buffer
		port map (
			din => data_in,
			rd_clk => rdclk,
			rd_en => r_en_i(2),
			rst => reset_i(2),
			wr_clk => wrclk,
			wr_en => w_en_i(2),
			dout => data_out_2,
			empty => empty_i(2),
			full => full_i(2),
			overflow => wrerr_i(2),
			underflow => rderr_i(2));
                 
	
		FIFO16_inst_3 : rcv_buffer
		port map (
			din => data_in,
			rd_clk => rdclk,
			rd_en => r_en_i(3),
			rst => reset_i(3),
			wr_clk => wrclk,
			wr_en => w_en_i(3),
			dout => data_out_3,
			empty => empty_i(3),
			full => full_i(3),
			overflow => wrerr_i(3),
			underflow => rderr_i(3));
                 
end Behavioral;