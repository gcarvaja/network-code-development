
--Copyright 2007-2010, Embedded Software Group at the University of
--Waterloo. All rights reserved.  By using this software the USER
--indicates that he or she has read, understood and will comply with the
--following:

--- Embedded Software Group at the University of Waterloo hereby
--grants USER nonexclusive permission to use, copy and/or modify this
--software for internal, noncommercial, research purposes only. Any
--distribution, including commercial sale or license, of this software,
--copies of the software, its associated documentation and/or
--modifications of either is strictly prohibited without the prior
--consent of the Embedded Software Group at the University of Waterloo.
--Title to copyright to this software and its associated documentation
--shall at all times remain with the Embedded Software Group at the
--University of Waterloo.  Appropriate copyright notice shall be placed
--on all software copies, and a complete copy of this notice shall be
--included in all copies of the associated documentation.  No right is
--granted to use in advertising, publicity or otherwise any trademark,
--service mark, or the name of the Embedded Software Group at the
--University of Waterloo.


--- This software and any associated documentation is provided "as is"

--THE EMBEDDED SOFTWARE GROUP AT THE UNIVERSITY OF WATERLOO MAKES NO
--REPRESENTATIONS OR WARRANTIES, EXPRESS OR IMPLIED, INCLUDING THOSE OF
--MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, OR THAT USE OF
--THE SOFTWARE, MODIFICATIONS, OR ASSOCIATED DOCUMENTATION WILL NOT
--INFRINGE ANY PATENTS, COPYRIGHTS, TRADEMARKS OR OTHER INTELLECTUAL
--PROPERTY RIGHTS OF A THIRD PARTY.

--The Embedded Software Group at the University of Waterloo shall not be
--liable under any circumstances for any direct, indirect, special,
--incidental, or consequential damages with respect to any claim by USER
--or any third party on account of or arising from the use, or inability
--to use, this software or its associated documentation, even if The
--Embedded Software Group at the University of Waterloo has been advised
--of the possibility of those damages.

----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    08:51:06 05/08/2007 
-- Design Name: 
-- Module Name:    branchcmd - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

use work.ncm_package.all;

---- Uncomment the following library declaration if instantiating
---- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity branchcmd is
    Port ( clk : in  STD_LOGIC;
           rst : in  STD_LOGIC;
           -- controller if
           start : in  STD_LOGIC;
           rdy : out  STD_LOGIC;
           result : out  STD_LOGIC;
           guard : in  STD_LOGIC_VECTOR (19 downto 0);
           -- states
           rcvbufempty : in  STD_LOGIC_VECTOR (15 downto 0);
           sndbufempty : in  STD_LOGIC;
           counter : in  STD_LOGIC_VECTOR (15 downto 0);
           status : in STD_LOGIC_VECTOR (15 downto 0);
           stopack : out std_logic;
           -- cfgrom
           cfgaddress : out  STD_LOGIC_VECTOR (7 downto 0);
           cfgdata : in  STD_LOGIC_VECTOR (31 downto 0);
			  
			  cfgdata_queue : in  STD_LOGIC_VECTOR (31 downto 0);
			  
           -- varram
           varaddress : out  STD_LOGIC_VECTOR (15 downto 0);
           vardata : in  STD_LOGIC_VECTOR (31 downto 0)
	   );
end branchcmd;

architecture Behavioral of branchcmd is

   signal guardmask : std_logic_vector(3 downto 0) := (others => '0');
   signal guardparams : std_logic_vector(15 downto 0) := (others => '0');
   
   signal rcvbufstate : std_logic;     -- async evaluated bit
   signal counterequal : std_logic;    -- async evaluated counter compare
   signal statusres   : std_logic;     -- async status eval
   
   signal var1address : std_logic_vector(15 downto 0) := (others => '0');
   signal var1len     : std_logic_vector(15 downto 0) := (others => '0');
   signal var2address : std_logic_vector(15 downto 0) := (others => '0');
   signal var2len     : std_logic_vector(15 downto 0) := (others => '0');

   signal vardatabuf  : std_logic_vector(31 downto 0) := (others => '0');

   signal lt          : std_logic := '0';
   signal gt          : std_logic := '0';
   
   signal allzero     : std_logic := '0';
   signal parity      : std_logic := '0';
   signal testres     : std_logic := '0';
   
   signal varparity   : std_logic;     -- split parity generator to bytes
   signal parity1     : std_logic;
   signal parity2     : std_logic;
   signal parity3     : std_logic;
   signal parity4     : std_logic;
   signal varzero     : std_logic;

   signal readystop_i : std_logic := '0';

   type states is (s_idle, s_decode, s_cfgvar1, s_cfgvar2, 
                   s_test1, s_test2, s_test3, s_test4, s_test5, s_test6, 
                   s1, s2, s3, s4, s5, s_rdy, check_queue_full, check_queue_empty);
   signal currentstate : states := s_idle;

begin

   stopack <= readystop_i;

   FSM: process(clk, rst)
      variable nextstate : states;
      variable dummy16 : std_logic_vector(15 downto 0);
      variable gti, lti : std_logic;
   begin
      if (rst = '1') then
         result <= '0';
         rdy <= '0';
         readystop_i <= '0';
         nextstate := s_idle;
         currentstate <= s_idle;
      elsif (rising_edge(clk)) then
      
         nextstate := currentstate;
         
         case currentstate is
         
            when s_idle =>
               rdy <= '0';
               result <= '0';
               varaddress <= (others => '0');
         
               if (start = '1') then
                  guardmask <= guard(19 downto 16);
                  guardparams <= guard(15 downto 0);
                  cfgaddress <= guard(15 downto 8);      -- get cfg info for first var anyway
                  nextstate := s_decode;
               else
                  cfgaddress <= X"00";
               end if;
               
            when s_decode =>
            
               case guardmask is
               
                  when BRANCH_FALSE =>
                     result <= '0';
                     nextstate := s_rdy;

                  when BRANCH_TRUE =>
                     result <= '1';
                     nextstate := s_rdy;

                  when BRANCH_BUFEMPTY =>
                  
                     if (guardparams(0) = '1') then
                        result <= sndbufempty;
                        nextstate := s_rdy;
                     else
                        result <= rcvbufstate;
                        nextstate := s_rdy;
                     end if;
                  
                  when BRANCH_TESTCNT =>
                        result <= counterequal;
                        nextstate := s_rdy;
                        
                  when BRANCH_STATUS =>
                        result <= statusres;
                        if (guardparams(13) = '1' and statusres  = '1') then     -- monoflop
                           readystop_i <= '1';
                        end if;
                        nextstate := s_rdy;

                  when BRANCH_EQVAR =>
                     cfgaddress <= guardparams(7 downto 0);      -- get cfg info for second var
                     nextstate := s_cfgvar1;
                     
                  when BRANCH_GTVAR =>
                     cfgaddress <= guardparams(7 downto 0);      -- get cfg info for second var
                     nextstate := s_cfgvar1;
                     
                  when BRANCH_LTVAR =>
                     cfgaddress <= guardparams(7 downto 0);      -- get cfg info for second var
                     nextstate := s_cfgvar1;
                     
                  when BRANCH_TESTVAR =>
                     -- only first var for test
                     nextstate := s_test1;
														
						-- new guards for checking queue state						
						when BRANCH_VARQUEUEFULL =>
						   nextstate := check_queue_full;
							 
						when BRANCH_VARQUEUEEMPTY =>
						  	nextstate := check_queue_empty;
                  
                  when others =>
                     result <= '0';
                     nextstate := s_rdy;
               
               end case;
					
					
			   when check_queue_full =>
				    
					 -- if size queue = number of valid elements  => queue is full
					 if (cfgdata_queue (31) = '1' and cfgdata_queue (6 downto 0)= (cfgdata_queue (30 downto 24))) then
						 result <= '1';
					 end if;
				 	--aux_debug <= X"00000001";
					nextstate := s_rdy;
				
				when check_queue_empty =>
				    -- if number of valid elements=0  => queue is empty
				    if (cfgdata_queue (31) = '1' and cfgdata_queue (7 downto 0)= X"00") then
						 result <= '1';
					 end if;
					--aux_debug <= X"00000002";
					nextstate := s_rdy;
            
            when s_cfgvar1 =>
               var1address <= cfgdata(31 downto 16);
               var1len <= cfgdata(15 downto 0);
               
               gti := '0';
               lti := '0';
               
               nextstate := s_cfgvar2;
            
            when s_cfgvar2 =>
               var2address <= cfgdata(31 downto 16);
               var2len <= cfgdata(15 downto 0);
               
               dummy16 := cfgdata(15 downto 0);
               -- check eq and len
               if (guardmask = BRANCH_EQVAR) then
                  if (var1len = dummy16) then 
                     nextstate := s1;
                  else
                     result <= '0';
                     nextstate := s_rdy;
                  end if;
               else
                  nextstate := s1;
               end if;
        
            when s_test1 =>
               var1address <= cfgdata(31 downto 16);
               var1len <= cfgdata(15 downto 0);
               
               allzero <= '1';               -- we assume all zero
               parity <= '0';
               
               nextstate := s_test2;
               
            when s_test2 =>
               varaddress <= var1address;    -- output var
               var1address <= var1address + 1;
               nextstate := s_test3;

            when s_test3 =>                  -- wait for var
               nextstate := s_test4;

            when s_test4 =>
               if (var1len /= X"0000") then
                  varaddress <= var1address;    -- start over var
                  var1address <= var1address + 1;
                  var1len <= var1len - 1;
               end if;

               vardatabuf <= vardata;        -- get data, calc results async

               nextstate := s_test5;
               
            when s_test5 =>                  -- eval, loop to test4
            
               allzero <= allzero and varzero;     -- clear if not
               parity <= parity xor varparity;     -- sum up parity bits
               
               if (var1len = X"0000") then
                  nextstate := s_test6;           
               else
                  nextstate := s_test4;
               end if;
        
            when s_test6 =>                     -- get res
               result <= testres;
               rdy <= '1';               
               nextstate := s_idle;
        
            -- do compare in qwords
            when s1 =>                          -- output var1
               varaddress <= var1address;
               var1address <= var1address + 1;
               nextstate := s2;
               
            when s2 =>                          -- output var2
               varaddress <= var2address;
               var2address <= var2address + 1;
               nextstate := s3;
               
            when s3 =>                          -- var1 rdy
               vardatabuf <= vardata;
               nextstate := s4;

               -- start over var1
               varaddress <= var1address;
               var1address <= var1address + 1;
               var1len <= var1len - 1;
               
            when s4 =>

               -- get next v2
               varaddress <= var2address;
               var2address <= var2address + 1;
               var2len <= var2len - 1;

               if (vardatabuf < vardata) then
                  lti := '1';
                  gti := '0';                
               elsif (vardatabuf > vardata) then
                  gti := '1';
                  lti := '0';
               else
                  gti := '0';
                  lti := '0';
               end if;

               if (var1len = X"0000") then
                  -- we are finished
                  if (guardmask = BRANCH_LTVAR) then
                     result <= lti;
                  elsif (guardmask = BRANCH_GTVAR) then
                     result <= gti;
                  elsif (guardmask = BRANCH_EQVAR) then
                     result <= not gti and not lti;
                  else
                     result <= '0';              -- should not happen anyway
                  end if;
                  
                  nextstate := s_rdy;                 
               else
               
                  if (guardmask = BRANCH_LTVAR) then
                     if (lti = '1') then
                        result <= '1';
                        nextstate := s_rdy;
                     else 
                        nextstate := s3;
                     end if;
                  elsif (guardmask = BRANCH_GTVAR) then
                     if (gti = '1') then
                        result <= '1';
                        nextstate := s_rdy;
                     else
                        nextstate := s3;
                     end if;
                  elsif (guardmask = BRANCH_EQVAR) then
                     if (gti = '1' or lti = '1') then
                        result <= '0';
                        nextstate := s_rdy;
                     else
                        nextstate := s3;
                     end if;
                  else
                     nextstate := s3;              -- loop
                  end if;
             
               end if;              
               
            when s_rdy =>
               rdy <= '1';
               nextstate := s_idle;
               
            when others =>
               result <= '0';          -- echo false as result per default
               rdy <= '1';
               nextstate := s_idle;
         
         end case;
      
         gt <= gti;        -- for debug purposes
         lt <= lti;
      
         currentstate <= nextstate;
      
      end if;
      
   end process;

   -- evaluate empty bit for rcvbuf

   rcvbufstate <= rcvbufempty(0)  when guardparams(11 downto 8) = "0000" else
                  rcvbufempty(1)  when guardparams(11 downto 8) = "0001" else
                  rcvbufempty(2)  when guardparams(11 downto 8) = "0010" else
                  rcvbufempty(3)  when guardparams(11 downto 8) = "0011" else
                  rcvbufempty(4)  when guardparams(11 downto 8) = "0100" else
                  rcvbufempty(5)  when guardparams(11 downto 8) = "0101" else
                  rcvbufempty(6)  when guardparams(11 downto 8) = "0110" else
                  rcvbufempty(7)  when guardparams(11 downto 8) = "0111" else
                  rcvbufempty(8)  when guardparams(11 downto 8) = "1000" else
                  rcvbufempty(9)  when guardparams(11 downto 8) = "1001" else
                  rcvbufempty(10) when guardparams(11 downto 8) = "1010" else
                  rcvbufempty(11) when guardparams(11 downto 8) = "1011" else
                  rcvbufempty(12) when guardparams(11 downto 8) = "1100" else
                  rcvbufempty(13) when guardparams(11 downto 8) = "1101" else
                  rcvbufempty(14) when guardparams(11 downto 8) = "1110" else
                  rcvbufempty(15) when guardparams(11 downto 8) = "1111" else
                  '0';
                  
   counterequal <= '1' when guardparams(15 downto 0) = counter else
                   '0';
                   
   statusres <= status(15) when guardparams(15) = '1' else
                status(14) when guardparams(14) = '1' else
                status(13) when guardparams(13) = '1' else
                status(12) when guardparams(12) = '1' else
                status(11) when guardparams(11) = '1' else
                status(10) when guardparams(10) = '1' else
                status(9)  when guardparams(9) = '1' else
                status(8)  when guardparams(8) = '1' else
                status(7)  when guardparams(7) = '1' else
                status(6)  when guardparams(6) = '1' else
                status(5)  when guardparams(5) = '1' else
                status(4)  when guardparams(4) = '1' else
                status(3)  when guardparams(3) = '1' else
                status(2)  when guardparams(2) = '1' else
                status(1)  when guardparams(1) = '1' else
                status(0)  when guardparams(0) = '1' else
                '0';
                  
   par1 : parity8 port map (
      data => vardatabuf(7 downto 0), 
      par_even => parity1
      );

   par2 : parity8 port map (
      data => vardatabuf(15 downto 8), 
      par_even => parity2
      );

   par3 : parity8 port map (
      data => vardatabuf(23 downto 16), 
      par_even => parity3
      );
                  
   par4 : parity8 port map (
      data => vardatabuf(31 downto 24), 
      par_even => parity4
      );

   varparity <= parity1 xor parity2 xor parity3 xor parity4;
   
   varzero <= '1' when vardatabuf = X"00000000" else '0';
   
   testres <=  not allzero when guardparams(BRANCH_TEST_NZ_POS) = '1' else
               allzero     when guardparams(BRANCH_TEST_Z_POS)  = '1' else
               not parity  when guardparams(BRANCH_TEST_PE_POS) = '1' else      -- we calc even parity so the parity is even if this is 0
               parity      when guardparams(BRANCH_TEST_PO_POS) = '1' else
               '0';
               
end Behavioral;
