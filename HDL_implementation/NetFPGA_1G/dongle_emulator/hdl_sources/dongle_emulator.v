
//Copyright 2007-2010, Embedded Software Group at the University of
//Waterloo. All rights reserved.  By using this software the USER
//indicates that he or she has read, understood and will comply with the
//following:

//-- Embedded Software Group at the University of Waterloo hereby
//grants USER nonexclusive permission to use, copy and/or modify this
//software for internal, noncommercial, research purposes only. Any
//distribution, including commercial sale or license, of this software,
//copies of the software, its associated documentation and/or
//modifications of either is strictly prohibited without the prior
//consent of the Embedded Software Group at the University of Waterloo.
//Title to copyright to this software and its associated documentation
//shall at all times remain with the Embedded Software Group at the
//University of Waterloo.  Appropriate copyright notice shall be placed
//on all software copies, and a complete copy of this notice shall be
//included in all copies of the associated documentation.  No right is
//granted to use in advertising, publicity or otherwise any trademark,
//service mark, or the name of the Embedded Software Group at the
//University of Waterloo.


//-- This software and any associated documentation is provided "as is"

//THE EMBEDDED SOFTWARE GROUP AT THE UNIVERSITY OF WATERLOO MAKES NO
//REPRESENTATIONS OR WARRANTIES, EXPRESS OR IMPLIED, INCLUDING THOSE OF
//MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, OR THAT USE OF
//THE SOFTWARE, MODIFICATIONS, OR ASSOCIATED DOCUMENTATION WILL NOT
//INFRINGE ANY PATENTS, COPYRIGHTS, TRADEMARKS OR OTHER INTELLECTUAL
//PROPERTY RIGHTS OF A THIRD PARTY.

//The Embedded Software Group at the University of Waterloo shall not be
//liable under any circumstances for any direct, indirect, special,
//incidental, or consequential damages with respect to any claim by USER
//or any third party on account of or arising from the use, or inability
//to use, this software or its associated documentation, even if The
//Embedded Software Group at the University of Waterloo has been advised
//of the possibility of those damages.

//////////////////////////////////////////////////////////////////////////////
// vim:set shiftwidth=3 softtabstop=3 expandtab:
// $Id: nf2_top.v 1929 2007-07-14 00:49:37Z grg $
//
// Module: nf2_top.v
// Project: NetFPGA
// Description: Top level module for a NetFPGA design.
//                
// This is the top level module - it instantiates the I/Os and
// clocks, and then instantiates a 'core' which contains all other
// logic.
//
///////////////////////////////////////////////////////////////////////////////

module dongle_emulator #( parameter CPCI_NF2_ADDR_WIDTH=32, 
									parameter CPCI_NF2_DATA_WIDTH=32, 
									parameter DMA_DATA_WIDTH=32) 
(
    // RGMII interfaces for 4 MACs.
    
	 output [3:0] rgmii_0_txd,
    output       rgmii_0_tx_ctl,
    output       rgmii_0_txc,
    input  [3:0] rgmii_0_rxd,
    input        rgmii_0_rx_ctl,
    input        rgmii_0_rxc,
	 
	 output [3:0] rgmii_1_txd,
    output       rgmii_1_tx_ctl,
    output       rgmii_1_txc,
    input  [3:0] rgmii_1_rxd,
    input        rgmii_1_rx_ctl,
    input        rgmii_1_rxc,
	 
    output [3:0] rgmii_2_txd,
    output       rgmii_2_tx_ctl,
    output       rgmii_2_txc,
    input  [3:0] rgmii_2_rxd,
    input        rgmii_2_rx_ctl,
    input        rgmii_2_rxc,

    output [3:0] rgmii_3_txd,
    output       rgmii_3_tx_ctl,
    output       rgmii_3_txc,
    input  [3:0] rgmii_3_rxd,
    input        rgmii_3_rx_ctl,
    input        rgmii_3_rxc,
	 
    // CPCI interface and clock

    input                               	cpci_clk,   // 62.5 MHz
    input                               	cpci_rd_wr_L,   // read on (1) write on (0)
    input                               	cpci_req,  
    input   [CPCI_NF2_ADDR_WIDTH-1:0]  	cpci_addr, 
    inout   [CPCI_NF2_DATA_WIDTH-1:0]  	cpci_data,
    output          reg              		cpci_wr_rdy,
    output          reg              		cpci_rd_rdy,
    output                              	nf2_err,  // was cnet_err	 

	 input   	gtx_clk  ,    // common TX clk reference 125MHz.
	 input 		nf2_reset,
	     input      core_clk,
	 
	 	 // this should be grounded 
    output [1:0]  					dma_op_code_ack,
    output        					dma_vld_n2c,
    inout [DMA_DATA_WIDTH-1:0] 	dma_data,
    output        					dma_q_nearly_full_n2c,

    output   							cpci_rp_en,
    output   							cpci_rp_prog_b,
    output   							cpci_rp_din
    );

////////////////////////////////////////////////////////////////////

// This signals should be    grounded 
assign dma_op_code_ack = 0;
assign dma_vld_n2c = 0;
assign dma_data = 0;
assign dma_q_nearly_full_n2c = 0;
assign cpci_rp_en = 0;
assign cpci_rp_prog_b = 0;
assign cpci_rp_din = 0;
assign nf2_err = 1'b0;

assign reset = nf2_reset; // || !core_locked;
	
// CPCI  wires
wire	[CPCI_NF2_DATA_WIDTH-1:0] 	cpci_rd_data; 
wire	[CPCI_NF2_ADDR_WIDTH-1:0] 	cpci_reg_addr;
wire  [CPCI_NF2_DATA_WIDTH-1:0] 	cpci_reg_wr_data,cpci_reg_rd_data; // Internal buses
wire 										cpci_data_tri_en;
reg 										cpci_reg_rd_wr_L;
wire 										cpci_clk_ibuf,cpci_clk_int;

// Memory Hit signals
//wire 				var_ram_hit;
wire 	[3:0]		ncm_frame_hit;
assign 			control_reg_hit = ~cpci_addr[21] & ~cpci_addr[20] ;
//assign 			var_ram_hit = ~cpci_addr[21] & cpci_addr[20] ;
assign 			ncm_frame_hit[0] = (cpci_addr[21] & ~cpci_addr[20] & ~cpci_addr[19]);
assign 			ncm_frame_hit[1] = (cpci_addr[21] & ~cpci_addr[20] & cpci_addr[19]);
assign 			ncm_frame_hit[2] = (cpci_addr[21] &   cpci_addr[20] & ~ cpci_addr[19]);
assign 			ncm_frame_hit[3] = (cpci_addr[21] &   cpci_addr[20] &  cpci_addr[19]);

// Control Register
reg 	[31:0] 	control_reg = 0;
// CPCI Buses
//wire [CPCI_NF2_DATA_WIDTH-1:0] vram_rd_data, ncm_mem_data;
wire [CPCI_NF2_DATA_WIDTH-1:0] ncm_frame_data[3:0], ncm_mem_data;

assign cpci_data = cpci_data_tri_en ? cpci_rd_data : 'hz;
//assign cpci_reg_rd_data = var_ram_hit ? vram_rd_data : (control_reg_hit ? control_reg : ncm_mem_data);
assign cpci_reg_rd_data = control_reg_hit ? control_reg : ncm_mem_data;


//assign cpci_reg_rd_data = var_ram_hit ? vram_rd_data : (ncm_mem_data);
// No Cross domain fifos testing
//assign cpci_reg_rd_wr_L = cpci_rd_wr_L;
assign cpci_data_tri_en 	= 	cpci_rd_wr_L;
assign cpci_reg_addr 		= 	cpci_addr;
assign cpci_rd_data 			= 	cpci_reg_rd_data;
assign cpci_reg_wr_data 	= 	cpci_data;

always@(cpci_clk_ibuf)
	if(nf2_reset)
		control_reg <= 32'b0;
	else if(~cpci_reg_rd_wr_L & control_reg_hit) 
		control_reg <= cpci_data;

   // --- core clock logic.
   IBUFG inst_core_clk_ibuf  (.I(core_clk),  .O(core_clk_ibuf));
	
	wire [3:0] core_clk_int;
   
	DCM CORE_DCM_CLK0 (
                     .CLKIN(core_clk_ibuf),
                     .CLKFB(core_clk_int[0]),  // feedback from BUFGMUX
                     .DSSEN(1'b0),
                     .PSINCDEC(1'b0),
                     .PSEN(1'b0),
                     .PSCLK(1'b0),
                     .RST(nf2_reset & ~disable_reset),
                     .CLK0(core_clk0_0),
                     .CLK90(),
                     .CLK180(),
                     .CLK270(),
                     .CLK2X(),
                     .CLK2X180(),
                     .CLKDV(),
                     .CLKFX(),
                     .CLKFX180(),
                     .PSDONE(),
                     .STATUS(),
                     .LOCKED(core_locked0)
                     );

   BUFGMUX BUFGMUX_CORE_CLK0 (
                             .O(core_clk_int[0]),
                             .I1(cpci_clk_ibuf),  // not used.
                             .I0(core_clk0_0),
                             .S(1'b0)
                             );


	DCM CORE_DCM_CLK1 (
                     .CLKIN(core_clk_ibuf),
                     .CLKFB(core_clk_int[1]),  // feedback from BUFGMUX
                     .DSSEN(1'b0),
                     .PSINCDEC(1'b0),
                     .PSEN(1'b0),
                     .PSCLK(1'b0),
                     .RST(nf2_reset & ~disable_reset),
                     .CLK0(core_clk0_1),
                     .CLK90(),
                     .CLK180(),
                     .CLK270(),
                     .CLK2X(),
                     .CLK2X180(),
                     .CLKDV(),
                     .CLKFX(),
                     .CLKFX180(),
                     .PSDONE(),
                     .STATUS(),
                     .LOCKED(core_locked1)
                     );

   BUFGMUX BUFGMUX_CORE_CLK1 (
                             .O(core_clk_int[1]),
                             .I1(cpci_clk_ibuf),  // not used.
                             .I0(core_clk0_1),
                             .S(1'b0)
                             );


	DCM CORE_DCM_CLK2 (
                     .CLKIN(core_clk_ibuf),
                     .CLKFB(core_clk_int[2]),  // feedback from BUFGMUX
                     .DSSEN(1'b0),
                     .PSINCDEC(1'b0),
                     .PSEN(1'b0),
                     .PSCLK(1'b0),
                     .RST(nf2_reset & ~disable_reset),
                     .CLK0(core_clk0_2),
                     .CLK90(),
                     .CLK180(),
                     .CLK270(),
                     .CLK2X(),
                     .CLK2X180(),
                     .CLKDV(),
                     .CLKFX(),
                     .CLKFX180(),
                     .PSDONE(),
                     .STATUS(),
                     .LOCKED(core_locked2)
                     );

   BUFGMUX BUFGMUX_CORE_CLK2 (
                             .O(core_clk_int[2]),
                             .I1(cpci_clk_ibuf),  // not used.
                             .I0(core_clk0_2),
                             .S(1'b0)
                             );

   // --- end of core clock logic.

	
IBUFG inst_cpci_clk_ibuf  (.I(cpci_clk),  .O(cpci_clk_ibuf));

BUFGMUX BUFGMUX_CPCI_CLK (    .O(cpci_clk_int),
                              .I0(cpci_clk_ibuf),
                              .I1(0),  // not used
                              .S(1'b0)    
                              );

   // Disable the reset signal
   //
   // Be careful -- this is potentially dangerous as a badly behaved design 
   // could lock itself and require reprogramming.
   //
   // Purpose: Spartan reprogramming designs
   //          Reset has to be disabled to prevent the Spartan from resetting
   //          the Virtex mid-programming when it's IOs come online.
//   wire        disable_reset;

   
   //----------------------------------------------------------------------
   // gtx_clk Clock Management
   //
   // This is the input 125MHz Transmit reference clock for the MACs. 
   // It is common to all four MACs and thus is instantiated here.
   //----------------------------------------------------------------------
   
   IBUF ibufg_gtx_clk (.I(gtx_clk),           .O(gtx_clk_ibufg));
			

	DCM RGMII_TX_DCM (.CLKIN(gtx_clk_ibufg),
                     .CLKFB(tx_rgmii_clk_int),
                     .DSSEN(1'b0),
                     .PSINCDEC(1'b0),
                     .PSEN(1'b0),
                     .PSCLK(1'b0),
                     .RST(reset),
                     .CLK0(tx_clk0),
                     .CLK90(tx_clk90),
                     .CLK180(),
                     .CLK270(),
                     .CLK2X(),
                     .CLK2X180(),
                     .CLKDV(),
                     .CLKFX(),
                     .CLKFX180(),
                     .PSDONE(),
                     .STATUS(),
                     .LOCKED());

   //-- Now we need to feed these to BUFGMUXes so that they are global.
   // We must feed both clocks to both BUFGMUXes even though the mux select
   // is fixed. This is due to a constraint on BUFGMUXES in the Virtex 2 Pro.
   
    BUFGMUX BUFGMUX_TXCLK (.O(tx_rgmii_clk_int),
                           .I0(tx_clk0),
                           .I1(tx_clk90),  // not used
                           .S(1'b0)    
                           );

   //-- To maintain the 2ns set-up and hold requirement we need to use the 90
   //-- degrees out of phase clock output to generate the rgmii_txc output.

   BUFGMUX BUFGMUX_TXCLK90 (.O(tx_rgmii_clk90_int),
                            .I1(tx_clk0),  // not used
                            .I0(tx_clk90),
                            .S(1'b0)
                            );
   //----------------------------------------------------------------
   // Receive clocks.
   //
   // There are 4 receive clocks. Each has its own DCM.
   // We simply route the incoming rxc to a DCM via an IBUFG.
   //
   // They are defined here simply to make the constraints easier 
   // to specify.
   //----------------------------------------------------------------

   IBUFG inst_rgmii_0_rxc_ibuf  (.I(rgmii_0_rxc),  .O(rgmii_0_rxc_ibuf));
   IBUFG inst_rgmii_1_rxc_ibuf  (.I(rgmii_1_rxc),  .O(rgmii_1_rxc_ibuf));
	IBUFG inst_rgmii_2_rxc_ibuf  (.I(rgmii_2_rxc),  .O(rgmii_2_rxc_ibuf));
	IBUFG inst_rgmii_3_rxc_ibuf  (.I(rgmii_3_rxc),  .O(rgmii_3_rxc_ibuf));
	
	wire [3:0] rx_rgmii_clk_int;
	
   DCM RGMII_0_RX_DCM (.CLKIN(rgmii_0_rxc_ibuf),
                       .CLKFB(rx_rgmii_clk_int[0]),  // feedback from BUFGMUX
                       .DSSEN(1'b0),
                       .PSINCDEC(1'b0),
                       .PSEN(1'b0),
                       .PSCLK(1'b0),
                       .RST(reset),
                       .CLK0(rgmii_0_clk0),
                       .CLK90(),
                       .CLK180(),
                       .CLK270(),
                       .CLK2X(),
                       .CLK2X180(),
                       .CLKDV(),
                       .CLKFX(),
                       .CLKFX180(),
                       .PSDONE(),
                       .STATUS(),
                       .LOCKED()
                       );
//
   BUFGMUX BUFGMUX_RGMII_0_RXCLK (.O	(rx_rgmii_clk_int[0]),
                                  .I0	(rgmii_0_clk0),
                                  .I1	(),
                                  .S	(1'b0)
                                  );
											 
	 DCM RGMII_1_RX_DCM(.CLKIN		(rgmii_1_rxc_ibuf),
                       .CLKFB		(rx_rgmii_clk_int[1]),  // feedback from BUFGMUX
                       .DSSEN		(1'b0),
                       .PSINCDEC	(1'b0),
                       .PSEN		(1'b0),
                       .PSCLK		(1'b0),
                       .RST		(reset),
                       .CLK0		(rgmii_1_clk0),
                       .CLK90		(),
                       .CLK180	(),
                       .CLK270	(),
                       .CLK2X		(),
                       .CLK2X180	(),
                       .CLKDV		(),
                       .CLKFX		(),
                       .CLKFX180	(),
                       .PSDONE	(),
                       .STATUS	(),
                       .LOCKED	()
                       );

   BUFGMUX BUFGMUX_RGMII_1_RXCLK (.O	(rx_rgmii_clk_int[1]),
                                  .I0	(rgmii_1_clk0),
                                  .I1	(),
                                  .S	(1'b0)
                                  );
											 
	 DCM RGMII_2_RX_DCM(.CLKIN		(rgmii_2_rxc_ibuf),
                       .CLKFB		(rx_rgmii_clk_int[2]),  // feedback from BUFGMUX
                       .DSSEN		(1'b0),
                       .PSINCDEC	(1'b0),
                       .PSEN		(1'b0),
                       .PSCLK		(1'b0),
                       .RST		(reset),
                       .CLK0		(rgmii_2_clk0),
                       .CLK90		(),
                       .CLK180	(),
                       .CLK270	(),
                       .CLK2X		(),
                       .CLK2X180	(),
                       .CLKDV		(),
                       .CLKFX		(),
                       .CLKFX180	(),
                       .PSDONE	(),
                       .STATUS	(),
                       .LOCKED	()
                       );

   BUFGMUX BUFGMUX_RGMII_2_RXCLK (.O	(rx_rgmii_clk_int[2]),
                                  .I0	(rgmii_2_clk0),
                                  .I1	(),
                                  .S	(1'b0)
                                  );
											 
	 DCM RGMII_3_RX_DCM(.CLKIN		(rgmii_3_rxc_ibuf),
                       .CLKFB		(rx_rgmii_clk_int[3]),  // feedback from BUFGMUX
                       .DSSEN		(1'b0),
                       .PSINCDEC	(1'b0),
                       .PSEN		(1'b0),
                       .PSCLK		(1'b0),
                       .RST		(reset),
                       .CLK0		(rgmii_3_clk0),
                       .CLK90		(),
                       .CLK180	(),
                       .CLK270	(),
                       .CLK2X		(),
                       .CLK2X180	(),
                       .CLKDV		(),
                       .CLKFX		(),
                       .CLKFX180	(),
                       .PSDONE	(),
                       .STATUS	(),
                       .LOCKED	()
                       );

   BUFGMUX BUFGMUX_RGMII_3_RXCLK (.O	(rx_rgmii_clk_int[3]),
                                  .I0	(rgmii_3_clk0),
                                  .I1	(),
                                  .S	(1'b0)
                                  );
   //----------------------------------------------------
   // Now we instantiate the various RGMII buffers for the
   // data and control signals and the outgoing TXC for
   // each channel.
   //----------------------------------------------------
   
   wire [7:0]   gmii_txd_int [3:0];
	wire [3:0]   gmii_tx_en_int;
	wire [3:0]   gmii_tx_er_int;
   wire [7:0]   gmii_rxd_reg [3:0];
	wire [3:0]   gmii_rx_dv_reg;
	wire [3:0]   gmii_rx_er_reg;
	
   rgmii_io rgmii_0_io (.rgmii_txd             (rgmii_0_txd),
                        .rgmii_tx_ctl          (rgmii_0_tx_ctl),
                        .rgmii_txc             (rgmii_0_txc),
                        .rgmii_rxd             (rgmii_0_rxd),
                        .rgmii_rx_ctl          (rgmii_0_rx_ctl),
                        .gmii_txd_int          (gmii_txd_int[0]),
                        .gmii_tx_en_int        (gmii_tx_en_int[0]),
                        .gmii_tx_er_int        (gmii_tx_er_int[0]),
                        .gmii_col_int          (),
                        .gmii_crs_int          (),
                        .gmii_rxd_reg          (gmii_rxd_reg[0]),
                        .gmii_rx_dv_reg        (gmii_rx_dv_reg[0]),
                        .gmii_rx_er_reg        (gmii_rx_er_reg[0]),
                        .eth_link_status       (),
                        .eth_clock_speed       (),
                        .eth_duplex_status     (),
                        .tx_rgmii_clk_int      (tx_rgmii_clk_int),
                        .tx_rgmii_clk90_int    (tx_rgmii_clk90_int),
                        .rx_rgmii_clk_int      (rx_rgmii_clk_int[0]),
                        .reset                 (nf2_reset)
                        );


   rgmii_io rgmii_1_io (.rgmii_txd             (rgmii_1_txd),
                        .rgmii_tx_ctl          (rgmii_1_tx_ctl),
                        .rgmii_txc             (rgmii_1_txc),
                        .rgmii_rxd             (rgmii_1_rxd),
                        .rgmii_rx_ctl          (rgmii_1_rx_ctl),
                        .gmii_txd_int          (gmii_txd_int[1]),
                        .gmii_tx_en_int        (gmii_tx_en_int[1]),
                        .gmii_tx_er_int        (gmii_tx_er_int[1]),
                        .gmii_col_int          (),
                        .gmii_crs_int          (),
                        .gmii_rxd_reg          (gmii_rxd_reg[1]),
                        .gmii_rx_dv_reg        (gmii_rx_dv_reg[1]),
                        .gmii_rx_er_reg        (gmii_rx_er_reg[1]),
                        .eth_link_status       (),
                        .eth_clock_speed       (),
                        .eth_duplex_status     (),
                        .tx_rgmii_clk_int      (tx_rgmii_clk_int),
                        .tx_rgmii_clk90_int    (tx_rgmii_clk90_int),
                        .rx_rgmii_clk_int      (rx_rgmii_clk_int[1]),
                        .reset                 (nf2_reset)
                        );
								
   rgmii_io rgmii_2_io (.rgmii_txd             (rgmii_2_txd),
                        .rgmii_tx_ctl          (rgmii_2_tx_ctl),
                        .rgmii_txc             (rgmii_2_txc),
                        .rgmii_rxd             (rgmii_2_rxd),
                        .rgmii_rx_ctl          (rgmii_2_rx_ctl),
                        .gmii_txd_int          (gmii_txd_int[2]),
                        .gmii_tx_en_int        (gmii_tx_en_int[2]),
                        .gmii_tx_er_int        (gmii_tx_er_int[2]),
                        .gmii_col_int          (),
                        .gmii_crs_int          (),
                        .gmii_rxd_reg          (gmii_rxd_reg[2]),
                        .gmii_rx_dv_reg        (gmii_rx_dv_reg[2]),
                        .gmii_rx_er_reg        (gmii_rx_er_reg[2]),
                        .eth_link_status       (),
                        .eth_clock_speed       (),
                        .eth_duplex_status     (),
                        .tx_rgmii_clk_int      (tx_rgmii_clk_int),
                        .tx_rgmii_clk90_int    (tx_rgmii_clk90_int),
                        .rx_rgmii_clk_int      (rx_rgmii_clk_int[2]),
                        .reset                 (nf2_reset)
                        );
								
   rgmii_io rgmii_3_io (.rgmii_txd             (rgmii_3_txd),
                        .rgmii_tx_ctl          (rgmii_3_tx_ctl),
                        .rgmii_txc             (rgmii_3_txc),
                        .rgmii_rxd             (rgmii_3_rxd),
                        .rgmii_rx_ctl          (rgmii_3_rx_ctl),
                        .gmii_txd_int          (gmii_txd_int[3]),
                        .gmii_tx_en_int        (gmii_tx_en_int[3]),
                        .gmii_tx_er_int        (gmii_tx_er_int[3]),
                        .gmii_col_int          (),
                        .gmii_crs_int          (),
                        .gmii_rxd_reg          (gmii_rxd_reg[3]),
                        .gmii_rx_dv_reg        (gmii_rx_dv_reg[3]),
                        .gmii_rx_er_reg        (gmii_rx_er_reg[3]),
                        .eth_link_status       (),
                        .eth_clock_speed       (),
                        .eth_duplex_status     (),
                        .tx_rgmii_clk_int      (tx_rgmii_clk_int),
                        .tx_rgmii_clk90_int    (tx_rgmii_clk90_int),
                        .rx_rgmii_clk_int      (rx_rgmii_clk_int[3]),
                        .reset                 (nf2_reset)
                        );
   // synthesis attribute keep_hierarchy of rgmii_0_io is false;							
//---------------------------------------------
//
// CPCI interface
//
//---------------------------------------------

reg rd_wr_L_lock;
	always@(posedge cpci_clk_int) begin
		if(cpci_req)
			cpci_rd_rdy <=1;
		else 
		cpci_rd_rdy <=0;
		if(~cpci_rd_wr_L)
			cpci_wr_rdy <=1;
		else 
			cpci_wr_rdy <=0;
		if(~cpci_rd_wr_L & ~rd_wr_L_lock) begin // Interlocking ( forces to read after a write...this is a workaround because we are not receiving cpci_req)
			cpci_reg_rd_wr_L <=0;
			rd_wr_L_lock <= 1;
			end
		else
			cpci_reg_rd_wr_L <=1;
		if(cpci_rd_wr_L)
			rd_wr_L_lock <=0;
end								
/////////////////////////////////////////

mux4to1 ncm_mem_mux(		.out	(ncm_mem_data),
								.i0	(ncm_frame_data[0]),
								.i1	(ncm_frame_data[1]),
								.i2	(ncm_frame_data[2]),
								.i3	(ncm_frame_data[3]),
                        .s0	(cpci_addr[19]),
                        .s1	(cpci_addr[20]));

/////////////////////////////////////////


   // --- core clock logic.

// Hay problemas con los contadores en la MAC cuando se utiliza este clock
// Los contadores empiezan a contar durante la inicializacion y quedan con un valor inicial
// Pasa algo raro en la estabilizacion de las seniales, probablemente con el reset
// Verificar este comportamiento. En todo caso, no se ve mayor ventaja en utilizar un DCM extra.

//   IBUFG inst_core_clk_ibuf  (.I(core_clk),  .O(core_clk_ibuf));

//   DCM CORE_DCM_CLK (
//                     .CLKIN(core_clk_ibuf),
//                     .CLKFB(core_clk_int),  // feedback from BUFGMUX
//                     .DSSEN(1'b0),
//                     .PSINCDEC(1'b0),
//                     .PSEN(1'b0),
//                     .PSCLK(1'b0),
//                     .RST(nf2_reset), // & ~disable_reset),
//                     .CLK0(core_clk0),
//                     .CLK90(),
//                     .CLK180(),
//                     .CLK270(),
//                     .CLK2X(),
//                     .CLK2X180(),
//                     .CLKDV(),
//                     .CLKFX(),
//                     .CLKFX180(),
//                     .PSDONE(),
//                     .STATUS(),
//                     .LOCKED(core_locked)
//                     );
//
//   BUFGMUX BUFGMUX_CORE_CLK (
//                             .O(core_clk_int),
//                             .I1(cpci_clk_ibuf),  // not used.
//                             .I0(core_clk0),
//                             .S(1'b0)
//                             );

   // --- end of core clock logic.

		open_prescaler clk_ncm( 	.clk 		(gtx_clk_ibufg),
											.rst 		(nf2_reset),
											.en  		(1'b1),
											.pulse 	(ncp_clk),
											.divider (11'b00000000010)
										);

wire[3:0] run_NCM;
wire[3:0] userbit1;
wire[3:0] userbit2;
//wire[3:0] gen_traffic_mode;
wire[23:0] mac_low [3:0];
wire[10:0] pclock_divider;

/////////////////////////////////////////

assign run_NCM [0] = control_reg[0];
assign userbit1[0] = control_reg[1];
assign userbit2[0] = control_reg[2];
//assign gen_traffic_mode[0] = control_reg[3];
assign mac_low[0] = 0;

assign run_NCM [1] = control_reg[4];
assign userbit1[1] = control_reg[5];
assign userbit2[1] = control_reg[6];
//assign gen_traffic_mode[1] = control_reg[7];
assign mac_low[1] = 1;

assign run_NCM [2] = control_reg[8];
assign userbit1[2] = control_reg[9];
assign userbit2[2] = control_reg[10];
//assign gen_traffic_mode[2] = control_reg[11];
assign mac_low[2] = 2;

assign run_NCM [3] = control_reg[12];
assign userbit1[3] = control_reg[13];
assign userbit2[3] = control_reg[14];
//assign gen_traffic_mode[3] = control_reg[15];
assign mac_low[3] = 3;

assign pclock_divider = control_reg[26:16];

//signal pclock_divider           : std_logic_vector(10 downto 0) := "00001100100";      -- 125Mhz/100 = 1.25MHz => 800[ns] period 

genvar i;

generate
	for (i=0; i <4 ; i=i+1) begin: MC_NCP0
ncm_main ncm_main_inst (
				//.clk_ncm							(core_clk_int[i]								),
				.clk_ncm							(ncp_clk											),
				.pclock_divider				(pclock_divider								),
				.clk_ext_bus 					(cpci_clk_int									),
				.rst	                  	(nf2_reset										),
				.control_reg_hit				(control_reg_hit								),
		  
				.txgmiimiiclk           	(tx_rgmii_clk_int								),
				.rxgmiimiiclk         		(rx_rgmii_clk_int[i]							),
		  
				.NCM_wen							(~cpci_reg_rd_wr_L & ncm_frame_hit [i]	),
				.address_in						(cpci_reg_addr									),
				.data_in							(cpci_reg_wr_data								),
				.data_out						(ncm_frame_data [i]							),
		  
				.emacphytxd           		(gmii_txd_int [i]								),
				.emacphytxen          		(gmii_tx_en_int [i]							),
				.emacphytxer          		(gmii_tx_er_int [i] 							),
				.phyemacrxd           		(gmii_rxd_reg [i]								),
				.phyemacrxdv          		(gmii_rx_dv_reg [i]							),
				.phyemacrxer          		(gmii_rx_er_reg [i]							),
		  
				.mac_high						(24'hABCDEF										),
				.mac_low							(mac_low[i]										),
		  
				.run_NCM                   (run_NCM	[i]									),
				.ncm_user1						(userbit1[i]									),
				.ncm_user2						(userbit2[i]									)
				//.gen_traffic_mode    		(gen_traffic_mode[i]							)
				);
	end
endgenerate		  

endmodule // nf2_top